// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard';
import { u1F356 } from 'react-icons-kit/noto_emoji_regular/u1F356';
import { truck } from 'react-icons-kit/fa/truck';
import { rub } from 'react-icons-kit/fa/rub';
import { tags } from 'react-icons-kit/fa/tags';
import { ic_people } from 'react-icons-kit/md/ic_people';
import { u1F35B } from 'react-icons-kit/noto_emoji_regular/u1F35B';
// views
import DashboardPage from '../views/Dashboard';
import PaymentView from '../views/PaymentView';
import OrderView from '../views/OrderView';
import ProductView from '../views/ProductView';
import Categories from '../views/CategoryView';
import CustomerView from '../views/CustomerView';
import DishView from '../views/DishView';

const DashboardRoutes = [
  {
    path: '/dashboard',
    sidebarName: 'Dashboard',
    navbarName: 'Material Dashboard',
    icon: ic_dashboard,
    component: DashboardPage
  },
  { divider: true },
  {
    path: '/payments',
    sidebarName: 'Денежка',
    navbarName: 'Money',
    icon: rub,
    component: PaymentView
  },
  {
    path: '/orders',
    sidebarName: 'Заказы',
    navbarName: 'Orders',
    icon: truck,
    component: OrderView
  },
  { divider: true },
  {
    path: '/dishes',
    sidebarName: 'Блюда',
    navbarName: 'Dishes',
    icon: u1F35B,
    component: DishView
  },
  {
    path: '/products',
    sidebarName: 'Продукты',
    navbarName: 'Products',
    icon: u1F356,
    component: ProductView
  },
  { divider: true },
  {
    path: '/customers',
    sidebarName: 'Клиенты',
    navbarName: 'Customers',
    icon: ic_people,
    component: CustomerView
  },
  { divider: true },
  {
    path: '/categories',
    sidebarName: 'Категории',
    navbarName: 'Categories',
    icon: tags,
    component: Categories
  },
  { redirect: true, path: '/', to: '/dashboard', navbarName: 'Redirect' }
];

export default DashboardRoutes;
