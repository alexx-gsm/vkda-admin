import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
// Layouts
import Dashboard from '../layouts/Dashboard';
import Login from '../layouts/Login';
// Private Route (only for authorized users)
import PrivateRoute from './PrivateRoute';

const indexRoutes = [{ path: '/', component: Dashboard }];

class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          {indexRoutes.map((prop, key) => {
            return (
              <PrivateRoute
                path={prop.path}
                component={prop.component}
                key={key}
              />
            );
          })}
        </Switch>
      </Router>
    );
  }
}

export default Routes;
