import { combineReducers } from 'redux';
import authReducer, { moduleName as authModule } from './modules/auth';
import filterReducer, { moduleName as filterModule } from './modules/filter';
import sidebarReducer, { moduleName as sidebarModule } from './modules/sidebar';
import productReducer, { moduleName as productModule } from './modules/product';
import recipeReducer, { moduleName as recipeModule } from './modules/recipe';
import paymentReducer, { moduleName as paymentModule } from './modules/payment';
import dishReducer, { moduleName as dishModule } from './modules/dish';
import orderReducer, { moduleName as orderModule } from './modules/order';
import categoryReducer, {
  moduleName as categoryModule
} from './modules/category';
import customerReducer, {
  moduleName as customerModule
} from './modules/customer';

export default combineReducers({
  [authModule]: authReducer,
  [filterModule]: filterReducer,
  [sidebarModule]: sidebarReducer,
  [productModule]: productReducer,
  [recipeModule]: recipeReducer,
  [paymentModule]: paymentReducer,
  [categoryModule]: categoryReducer,
  [customerModule]: customerReducer,
  [dishModule]: dishReducer,
  [orderModule]: orderReducer
});
