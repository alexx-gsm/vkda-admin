import { createStore, applyMiddleware, compose } from 'redux'
import reducer from './reducer'
import thunk from 'redux-thunk'
import getPaymentsMiddleware from './middlewares/getPaymentsMiddleware'

const store = createStore(
  reducer,
  // applyMiddleware(thunk, getPaymentsMiddleware),
  compose(
    applyMiddleware(thunk, getPaymentsMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
)

export default store
