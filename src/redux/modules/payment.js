import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'
// --- MODULE NAME ---
export const moduleName = 'payments'

// --- ACTIONS ---
export const PAYMENTS_LOADING = `${appName}/${moduleName}/LOADING`
export const PAYMENTS_ADD = `${appName}/${moduleName}/ADD`
export const PAYMENTS_SAVE = `${appName}/${moduleName}/SAVE`
export const PAYMENTS_DELETE = `${appName}/${moduleName}/DELETE`
export const PAYMENTS_GET_ALL = `${appName}/${moduleName}/GET_ALL`
export const PAYMENTS_GET_ALL_SUCCESS = `${appName}/${moduleName}/GET_ALL_SUCCESS`
export const PAYMENTS_GET_ONE = `${appName}/${moduleName}/GET_ONE`
export const PAYMENTS_CLEAR = `${appName}/${moduleName}/CLEAR`
export const PAYMENTS_GET_ERROR = `${appName}/${moduleName}/GET_ERROR`
export const PAYMENTS_STORE = `${appName}/${moduleName}/STORE`
export const PAYMENTS_SET_SELECTED_DAYS = `${appName}/${moduleName}/SET_SELECTED_DAYS`
export const PAYMENTS_SET_SELECTED_DAY = `${appName}/${moduleName}/SET_SELECTED_DAY`

export const PAYMENTS_MONTH_SUMMARY = `${appName}/${moduleName}/MONTH_SUMMARY`
export const PAYMENTS_YEAR_SUMMARY = `${appName}/${moduleName}/YEAR_SUMMARY`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  payments: [],
  payment: {},
  monthPayments: [],
  yearPayments: [],
  selectedDays: [],
  selectedDay: 0,
  week: null,
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, week, error } = action

  switch (type) {
    case PAYMENTS_LOADING:
      return state.set('loading', true)
    case PAYMENTS_GET_ALL:
      return state
        .set('loading', false)
        .set('payments', payload)
        .set('week', week)
        .set('error', {})
    case PAYMENTS_GET_ALL_SUCCESS:
      return state
        .set('loading', false)
        .set('payments', payload)
        .set('week', week)
        .set('error', {})
    case PAYMENTS_GET_ONE:
      return state
        .set('loading', false)
        .set('payment', payload)
        .set('error', {})
    case PAYMENTS_ADD:
      return state
        .set('loading', false)
        .set('payments', [...state.payments, payload])
        .set('error', {})
    case PAYMENTS_STORE:
      return state
        .set('loading', false)
        .set('payment', payload)
        .set('error', {})
    case PAYMENTS_GET_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    case PAYMENTS_CLEAR:
      return state
        .set('loading', false)
        .set('payment', {})
        .set('error', {})
    case PAYMENTS_SET_SELECTED_DAYS:
      return state
        .set('loading', false)
        .set('selectedDays', payload)
        .set('week', week)
        .set('error', {})
    case PAYMENTS_SET_SELECTED_DAY:
      return state
        .set('loading', false)
        .set('selectedDay', payload)
        .set('week', week ? week : state.week)
        .set('error', {})
    case PAYMENTS_MONTH_SUMMARY:
      return state
        .set('loading', false)
        .set('monthPayments', payload)
        .set('error', {})
    case PAYMENTS_YEAR_SUMMARY:
      return state
        .set('loading', false)
        .set('yearPayments', payload)
        .set('error', {})
    default:
      return state
  }
}

// --- AC ---
// get all products
export const getPayments = (week) => (dispatch) => {
  dispatch({
    type: PAYMENTS_LOADING,
  })
  axios
    .post(`${HOST}/api/payments/week/${week}`)
    .then((res) => {
      dispatch({
        type: PAYMENTS_GET_ALL,
        payload: res.data,
        week,
        calculateWeekStatic: true,
      })
    })
    .catch((error) =>
      dispatch({
        type: PAYMENTS_GET_ERROR,
        error,
      })
    )
}

// get payment by ID
export const getPaymentById = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/payments/${id}`)
    .then((res) => {
      dispatch({
        type: PAYMENTS_GET_ONE,
        payload: res.data,
      })
    })
    .catch((error) =>
      dispatch({
        type: PAYMENTS_GET_ERROR,
        error,
      })
    )
}

// add payment
export const addPayment = (payment, history) => (dispatch) => {
  axios
    .post(`${HOST}/api/payments`, payment)
    .then((res) => {
      dispatch({
        type: PAYMENTS_ADD,
        payload: res.data,
      })

      history.push('/payments')
    })
    .catch((error) =>
      dispatch({
        type: PAYMENTS_GET_ERROR,
        error,
      })
    )
}

// save payments
export const savePayment = (payment, history) => (dispatch) => {
  axios
    .put(`${HOST}/api/payments`, payment)
    .then((res) => {
      dispatch({
        type: PAYMENTS_SAVE,
        payload: res.data,
      })
      history.push('/payments')
    })
    .catch((error) =>
      dispatch({
        type: PAYMENTS_GET_ERROR,
        error,
      })
    )
}

// Payment delete
export const deletePayment = (id, history) => (dispatch) => {
  console.log('AC delete id:', id)
  axios
    .delete(`${HOST}/api/payments/${id}`)
    .then((res) => history.push('/payments'))
    .catch((error) =>
      dispatch({
        type: PAYMENTS_GET_ERROR,
        error,
      })
    )
}

// store payment
export const storePayment = (payment) => ({
  type: PAYMENTS_STORE,
  payload: payment,
})

// clear payment
export const clearPayment = () => ({
  type: PAYMENTS_CLEAR,
})

// set selected days
export const setSelectedDays = (selectedDays, week) => ({
  type: PAYMENTS_SET_SELECTED_DAYS,
  payload: selectedDays,
  week,
  getPayments: true,
})

// set selected day
export const setSelectedDay = (selectedDay, week) => ({
  type: PAYMENTS_SET_SELECTED_DAY,
  payload: selectedDay,
  week,
})

// get all year payments
export const getYearPayments = (year) => (dispatch) => {
  axios
    .post(`${HOST}/api/payments/year`, { year })
    .then((res) => {
      dispatch({
        type: PAYMENTS_YEAR_SUMMARY,
        payload: res.data,
      })
    })
    .catch((error) =>
      dispatch({
        type: PAYMENTS_GET_ERROR,
        error,
      })
    )
}

// get month payments
export const getMonthPayments = ({ year, month }) => (dispatch) => {
  axios
    .post(`${HOST}/api/payments/month`, { year, month })
    .then((res) => {
      dispatch({
        type: PAYMENTS_MONTH_SUMMARY,
        payload: res.data,
      })
    })
    .catch((error) =>
      dispatch({
        type: PAYMENTS_GET_ERROR,
        error,
      })
    )
}

export default reducer
