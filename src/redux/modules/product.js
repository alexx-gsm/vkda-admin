import axios from 'axios';
import { appName } from '../../config';
import { Record } from 'immutable';

// --- MODULE NAME ---
export const moduleName = 'productStore';

// --- ACTIONS ---
export const PRODUCTS_LOADING = `${appName}/${moduleName}/LOADING`;
export const PRODUCTS_ADD = `${appName}/${moduleName}/ADD`;
export const PRODUCTS_SAVE = `${appName}/${moduleName}/SAVE`;
export const PRODUCTS_DELETE = `${appName}/${moduleName}/DELETE`;
export const PRODUCTS_GET_ALL = `${appName}/${moduleName}/GET_ALL`;
export const PRODUCTS_GET_ONE = `${appName}/${moduleName}/GET_ONE`;
export const PRODUCTS_CLEAR = `${appName}/${moduleName}/CLEAR`;
export const PRODUCTS_STORE = `${appName}/${moduleName}/STORE`;
export const PRODUCTS_STORE_PRICE = `${appName}/${moduleName}/STORE_PRICE`;
export const PRODUCTS_STORE_UNUSED_PRODUCTS = `${appName}/${moduleName}/STORE_UNUSED_PRODUCTS`;
export const PRODUCTS_GET_ERROR = `${appName}/${moduleName}/GET_ERROR`;

// --- INITIAL STATE ---
const emptyProduct = {
  title: '',
  category: '',
  isComplex: false,
  value: 0,
  amount: 1,
  price: 0,
  waste: 0,
  content: [],
  comment: ''
};

const ReducerRecord = Record({
  products: [],
  unusedProducts: [],
  product: emptyProduct,
  isProductLoaded: false,
  categories: [
    { _id: '1', title: 'Мясо' },
    { _id: '2', title: 'Овощи' },
    { _id: '3', title: 'Консервы' },
    { _id: '5', title: 'Крупа' },
    { _id: '6', title: 'Выпечка' },
    { _id: '7', title: 'Напитки' },
    { _id: 'Зелень', title: 'Зелень' },
    { _id: '8', title: 'Разное' }
  ],
  error: {},
  loading: false
});

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action;

  switch (type) {
    case PRODUCTS_LOADING:
      return state.set('loading', true);
    case PRODUCTS_GET_ALL:
      return state
        .set('loading', false)
        .set('products', payload)
        .set('error', {});
    case PRODUCTS_GET_ONE:
      return state
        .set('loading', false)
        .set('product', payload)
        .set('isProductLoaded', true)
        .set('error', {});
    case PRODUCTS_ADD:
      return state
        .set('loading', false)
        .set('products', [...state.products, payload])
        .set('error', {});
    case PRODUCTS_DELETE:
      return state
        .set('loading', false)
        .set('products', [
          ...state.products.filter(item => item._id !== payload._id)
        ])
        .set('error', {});
    case PRODUCTS_CLEAR:
      return state
        .set('loading', false)
        .set('product', emptyProduct)
        .set('unusedProducts', [])
        .set('isProductLoaded', false)
        .set('error', {});
    case PRODUCTS_STORE:
      return state
        .set('loading', false)
        .set('product', payload)
        .set('isProductLoaded', false)
        .set('error', {});
    case PRODUCTS_STORE_PRICE:
      return state
        .set('loading', false)
        .set('product', { ...state.product, price: payload })
        .set('error', {});
    case PRODUCTS_STORE_UNUSED_PRODUCTS:
      return state
        .set('loading', false)
        .set('unusedProducts', payload)
        .set('error', {});
    case PRODUCTS_GET_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response.data)
        .set('isProductLoaded', false);
    default:
      return state;
  }
};

// --- AC ---
// get all products
export const getProducts = () => dispatch => {
  dispatch({
    type: PRODUCTS_LOADING
  });
  axios
    .post('/api/products/all')
    .then(res => {
      dispatch({
        type: PRODUCTS_GET_ALL,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: PRODUCTS_GET_ERROR,
        error
      })
    );
};

// get product by ID
export const getProductById = id => dispatch => {
  axios
    .post(`/api/products/${id}`)
    .then(res => {
      dispatch({
        type: PRODUCTS_GET_ONE,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: PRODUCTS_GET_ERROR,
        error
      })
    );
};

// add product
export const addProduct = (product, history) => dispatch => {
  axios
    .post('/api/products', product)
    .then(res => {
      dispatch({
        type: PRODUCTS_ADD,
        payload: res.data
      });

      history.push('/products');
    })
    .catch(error =>
      dispatch({
        type: PRODUCTS_GET_ERROR,
        error
      })
    );
};

// save product
export const saveProduct = (product, history) => dispatch => {
  axios
    .put('/api/products', product)
    .then(res => {
      dispatch({
        type: PRODUCTS_SAVE,
        payload: res.data
      });
      history.push('/products');
    })
    .catch(error =>
      dispatch({
        type: PRODUCTS_GET_ERROR,
        error
      })
    );
};

// delete product
export const deleteProduct = (id, history) => dispatch => {
  console.log('AC delete id:', id);
  axios
    .delete(`/api/products/${id}`)
    .then(res => {
      dispatch({
        type: PRODUCTS_DELETE,
        payload: res.data
      });
      history.push('/products');
    })
    .catch(error => {
      dispatch({
        type: PRODUCTS_GET_ERROR,
        error
      });
    });
};

// store product
export const storeProduct = product => ({
  type: PRODUCTS_STORE,
  payload: product
});

// store price
export const storePrice = price => ({
  type: PRODUCTS_STORE_PRICE,
  payload: price
});

// store unused products
export const storeUnusedProducts = unusedProducts => ({
  type: PRODUCTS_STORE_UNUSED_PRODUCTS,
  payload: unusedProducts
});

// clear product
export const clearProduct = () => ({
  type: PRODUCTS_CLEAR
});

export default reducer;
