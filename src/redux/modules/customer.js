import axios from 'axios';
import { appName } from '../../config';
import { Record } from 'immutable';

// --- MODULE NAME ---
export const moduleName = 'customerStore';

// --- ACTIONS ---
export const CUSTOMER_LOADING = `${appName}/${moduleName}/CUSTOMER_LOADING`;
export const CUSTOMER_GET_ALL = `${appName}/${moduleName}/CUSTOMER_GET_ALL`;
export const CUSTOMER_GET_ONE = `${appName}/${moduleName}/CUSTOMER_GET_ONE`;
export const CUSTOMER_ADD = `${appName}/${moduleName}/CUSTOMER_ADD`;
export const CUSTOMER_SAVE = `${appName}/${moduleName}/CUSTOMER_SAVE`;
export const CUSTOMER_DELETE = `${appName}/${moduleName}/CUSTOMER_DELETE`;
export const CUSTOMER_STORE = `${appName}/${moduleName}/CUSTOMER_STORE`;
export const CUSTOMER_CLEAR = `${appName}/${moduleName}/CUSTOMER_CLEAR`;
export const CUSTOMER_GET_ERROR = `${appName}/${moduleName}/CUSTOMER_GET_ERROR`;
export const CUSTOMER_SET_TAB_INDEX = `${appName}/${moduleName}/CUSTOMER_SET_TAB_INDEX`;

// --- INITIAL STATE ---
const emptyItem = {};

const ReducerRecord = Record({
  customers: [],
  customer: emptyItem,
  error: {},
  loading: false
});

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action;

  switch (type) {
    case CUSTOMER_LOADING:
      return state.set('loading', true);
    case CUSTOMER_GET_ALL:
      return state
        .set('loading', false)
        .set('customers', payload)
        .set('customer', emptyItem)
        .set('error', {});
    case CUSTOMER_GET_ONE:
      return state
        .set('loading', false)
        .set('customer', payload)
        .set('error', {});
    case CUSTOMER_ADD:
      return state
        .set('loading', false)
        .set('customers', [...state.customers, payload])
        .set('error', {});
    case CUSTOMER_SAVE:
      return state
        .set('loading', false)
        .set('customer', {})
        .set('error', {});
    case CUSTOMER_STORE:
      return state
        .set('loading', false)
        .set('customer', payload)
        .set('error', {});
    case CUSTOMER_DELETE:
      return state
        .set('loading', false)
        .set('customers', [
          ...state.customers.filter(item => item._id !== payload._id)
        ])
        .set('error', {});
    case CUSTOMER_CLEAR:
      return state
        .set('loading', false)
        .set('customer', emptyItem)
        .set('error', {});
    case CUSTOMER_GET_ERROR:
      return state.set('loading', false).set('error', error.response.data);
    default:
      return state;
  }
};

// --- AC ---
// get all customers
export const getCustomers = () => dispatch => {
  dispatch({
    type: CUSTOMER_LOADING
  });
  axios
    .post('/api/customers/all')
    .then(res => {
      dispatch({
        type: CUSTOMER_GET_ALL,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: CUSTOMER_GET_ERROR,
        error
      })
    );
};
// get customer by ID
export const getCustomerById = id => dispatch => {
  axios
    .post(`/api/customers/${id}`)
    .then(res => {
      dispatch({
        type: CUSTOMER_GET_ONE,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: CUSTOMER_GET_ERROR,
        error
      })
    );
};

// save customer
export const saveCustomer = (customer, history) => dispatch => {
  axios
    .post('/api/customers', customer)
    .then(res => {
      dispatch({
        type: CUSTOMER_SAVE,
        payload: res.data
      });

      history.push('/customers');
    })
    .catch(error =>
      dispatch({
        type: CUSTOMER_GET_ERROR,
        error
      })
    );
};

// // save category
// export const saveCategory = (category, history) => dispatch => {
//   axios
//     .put('/api/categories', category)
//     .then(res => {
//       dispatch({
//         type: CATEGORY_STORE,
//         payload: res.data
//       });

//       history.push('/categories');
//     })
//     .catch(error =>
//       dispatch({
//         type: CATEGORY_GET_ERROR,
//         error
//       })
//     );
// };

// // delete category
// export const deleteCategory = (id, history) => dispatch => {
//   if (id === '') {
//     history.push('/categories');
//     return;
//   }
//   axios
//     .delete(`/api/categories/${id}`)
//     .then(res => {
//       dispatch({
//         type: CATEGORY_DELETE,
//         payload: res.data
//       });
//       history.push('/categories');
//     })
//     .catch(error => {
//       dispatch({
//         type: CATEGORY_GET_ERROR,
//         error
//       });
//     });
// };

// store customer
export const storeCustomer = customer => ({
  type: CUSTOMER_STORE,
  payload: customer
});

// // clear customer
// export const clearCustomer = () => ({
//   type: CUSTOMER_CLEAR
// });

export default reducer;
