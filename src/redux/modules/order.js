import React from 'react';

import axios from 'axios';
import { appName } from '../../config';
import { Record } from 'immutable';
// icons
import { Icon } from 'react-icons-kit';
import { ic_fiber_new } from 'react-icons-kit/md/ic_fiber_new';
import { ic_done_all } from 'react-icons-kit/md/ic_done_all';
import { ic_settings } from 'react-icons-kit/md/ic_settings';
import { ic_shopping_basket } from 'react-icons-kit/md/ic_shopping_basket';
import { ic_local_shipping } from 'react-icons-kit/md/ic_local_shipping';
import { ic_priority_high } from 'react-icons-kit/md/ic_priority_high';
import moment from 'moment';

// --- MODULE NAME ---
export const moduleName = 'orderStore';

// --- ACTIONS ---
export const ORDER_LOADING = `${appName}/${moduleName}/LOADING`;
export const ORDER_ADD = `${appName}/${moduleName}/ADD`;
export const ORDER_SAVE = `${appName}/${moduleName}/SAVE`;
export const ORDER_DELETE = `${appName}/${moduleName}/DELETE`;
export const ORDER_GET_ALL = `${appName}/${moduleName}/GET_ALL`;
export const ORDER_GET_ALL_IN_DELIVERY_DAY = `${appName}/${moduleName}/GET_ALL_IN_DELIVERY_DAY`;
export const ORDER_GET_ONE = `${appName}/${moduleName}/GET_ONE`;
export const ORDER_CLEAR = `${appName}/${moduleName}/CLEAR`;
export const ORDER_STORE = `${appName}/${moduleName}/STORE`;
export const ORDER_SET_FILTER = `${appName}/${moduleName}/SET_FILTER`;
export const ORDER_STORE_SELECTED_DAY = `${appName}/${moduleName}/STORE_SELECTED_DAY`;
export const ORDER_GET_ERROR = `${appName}/${moduleName}/GET_ERROR`;

// --- INITIAL STATE ---
const emptyOrder = {
  dishes: []
};

const ReducerRecord = Record({
  orders: [],
  filteredOrders: [],
  order: emptyOrder,
  error: {},
  loading: false,
  selectedDay: moment().toDate(),
  filter: 'none',
  statuses: [
    {
      title: 'draft',
      alias: 'черновик',
      icon: <Icon icon={ic_priority_high} />,
      color: 'grey',
      sorting: 10
    },

    {
      title: 'new',
      alias: 'новый',
      icon: <Icon icon={ic_fiber_new} />,
      color: 'red',
      sorting: 20
    },
    {
      title: 'onProcess',
      alias: 'сборка',
      icon: <Icon icon={ic_settings} />,
      color: 'indigo',
      sorting: 30
    },
    {
      title: 'isReady',
      alias: 'готов',
      icon: <Icon icon={ic_shopping_basket} />,
      color: 'teal',
      sorting: 40
    },
    {
      title: 'onDelivery',
      alias: 'доставка',
      icon: <Icon icon={ic_local_shipping} />,
      color: 'orange',
      sorting: 50
    },
    {
      title: 'done',
      alias: 'выполнен',
      icon: <Icon icon={ic_done_all} />,
      color: 'green',
      sorting: 60
    }
  ]
});

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action;

  switch (type) {
    case ORDER_LOADING:
      return state.set('loading', true);
    case ORDER_GET_ALL:
      return state
        .set('loading', false)
        .set('orders', payload)
        .set('error', {});
    case ORDER_GET_ALL_IN_DELIVERY_DAY:
      return state
        .set('loading', false)
        .set('orders', payload)
        .set('error', {});
    case ORDER_GET_ONE:
      return state
        .set('loading', false)
        .set('order', payload)
        .set('error', {});
    case ORDER_ADD:
      return state
        .set('loading', false)
        .set('orders', [...state.orders, payload])
        .set('error', {});
    case ORDER_DELETE:
      return state
        .set('loading', false)
        .set('orders', [
          ...state.orders.filter(item => item._id !== payload._id)
        ])
        .set('error', {});
    case ORDER_SET_FILTER:
      return state
        .set('loading', false)
        .set('filter', payload)
        .set('error', {});
    case ORDER_CLEAR:
      return state
        .set('loading', false)
        .set('order', emptyOrder)
        .set('error', {});
    case ORDER_STORE:
      return state
        .set('loading', false)
        .set('order', payload)
        .set('error', {});
    case ORDER_STORE_SELECTED_DAY:
      return state
        .set('loading', false)
        .set('selectedDay', payload)
        .set('error', {});
    case ORDER_GET_ERROR:
      return state.set('loading', false).set('error', error);
    default:
      return state;
  }
};

// --- AC ---
// get all orders
export const getOrders = () => dispatch => {
  dispatch({
    type: ORDER_LOADING
  });
  axios
    .post('/api/orders/all')
    .then(res => {
      dispatch({
        type: ORDER_GET_ALL,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: ORDER_GET_ERROR,
        error
      })
    );
};

// get all orders ininterval
export const getOrdersInInterval = (day, dayNext) => dispatch => {
  dispatch({
    type: ORDER_LOADING
  });
  axios
    .post('/api/orders/interval', { day, dayNext })
    .then(res => {
      dispatch({
        type: ORDER_GET_ALL_IN_DELIVERY_DAY,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: ORDER_GET_ERROR,
        error
      })
    );
};

// get order by ID
export const getOrderById = id => dispatch => {
  axios
    .post(`/api/orders/${id}`)
    .then(res => {
      dispatch({
        type: ORDER_GET_ONE,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: ORDER_GET_ERROR,
        error
      })
    );
};

// add order
export const addOrder = (order, history) => dispatch => {
  axios
    .post('/api/orders', order)
    .then(res => {
      dispatch({
        type: ORDER_ADD,
        payload: res.data
      });

      history.push('/orders');
    })
    .catch(error =>
      dispatch({
        type: ORDER_GET_ERROR,
        error
      })
    );
};

// save order
export const saveOrder = (order, history) => dispatch => {
  // return;
  axios
    .post('/api/orders', order)
    .then(res => {
      dispatch({
        type: ORDER_SAVE,
        payload: res.data
      });
      history.push('/orders');
    })
    .catch(error =>
      dispatch({
        type: ORDER_GET_ERROR,
        error
      })
    );
};

// delete order
export const deleteOrder = (id, history) => dispatch => {
  axios
    .delete(`/api/orders/${id}`)
    .then(res => {
      dispatch({
        type: ORDER_DELETE,
        payload: res.data
      });
      history.push('/orders');
    })
    .catch(error => {
      dispatch({
        type: ORDER_GET_ERROR,
        error
      });
    });
};

// set order filter
export const setOrderFilter = filter => ({
  type: ORDER_SET_FILTER,
  payload: filter
});

// store order
export const storeOrder = order => ({
  type: ORDER_STORE,
  payload: order
});

// set selected day
export const storeSelectedDay = day => ({
  type: ORDER_STORE_SELECTED_DAY,
  payload: day
});

// clear order
export const clearOrder = () => ({
  type: ORDER_CLEAR
});

// fire order form error
export const setError = error => ({
  type: ORDER_GET_ERROR,
  error
});

export default reducer;
