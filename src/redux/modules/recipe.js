import axios from 'axios';
import { appName } from '../../config';
import { Record } from 'immutable';

// --- MODULE NAME ---
export const moduleName = 'recipes';

// --- ACTIONS ---
export const RECIPE_LOADING = `${appName}/${moduleName}/RECIPE_LOADING`;
export const RECIPE_GET_ALL = `${appName}/${moduleName}/RECIPE_GET_ALL`;
export const RECIPE_GET_ONE = `${appName}/${moduleName}/RECIPE_GET_ONE`;
export const RECIPE_ADD = `${appName}/${moduleName}/RECIPE_ADD`;
export const RECIPE_SAVE = `${appName}/${moduleName}/RECIPE_SAVE`;
export const RECIPE_DELETE = `${appName}/${moduleName}/RECIPE_DELETE`;
export const RECIPE_STORE = `${appName}/${moduleName}/RECIPE_STORE`;
export const RECIPE_CLEAR = `${appName}/${moduleName}/RECIPE_CLEAR`;
export const RECIPE_GET_ERROR = `${appName}/${moduleName}/RECIPE_GET_ERROR`;

// --- INITIAL STATE ---
const emptyRecipe = {
  title: '',
  category: '',
  content: [],
  comment: '',
  cost: 0
};

const ReducerRecord = Record({
  recipes: [],
  recipe: emptyRecipe,
  categories: [
    { _id: '1', title: 'Первое' },
    { _id: '2', title: 'Второе' },
    { _id: '5', title: 'Салат' },
    { _id: '3', title: 'Гарнир' },
    { _id: '6', title: 'Выпечка' },
    { _id: '7', title: 'Напитки' },
    { _id: '8', title: 'Разное' }
  ],
  error: {},
  isRecipeLoaded: true,
  loading: false
});

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action;

  switch (type) {
    case RECIPE_LOADING:
      return state.set('loading', true);
    case RECIPE_GET_ALL:
      return state
        .set('loading', false)
        .set('recipes', payload)
        .set('error', {});
    case RECIPE_GET_ONE:
      return state
        .set('loading', false)
        .set('recipe', payload)
        .set('isRecipeLoaded', true)
        .set('error', {});
    case RECIPE_ADD:
      return state
        .set('loading', false)
        .set('recipes', [...state.recipes, payload])
        .set('error', {});
    case RECIPE_STORE:
      return state
        .set('loading', false)
        .set('recipe', payload)
        .set('isRecipeLoaded', false)
        .set('error', {});
    case RECIPE_DELETE:
      return state
        .set('loading', false)
        .set('recipes', [
          ...state.recipes.filter(item => item._id !== payload._id)
        ])
        .set('error', {});
    case RECIPE_CLEAR:
      return state
        .set('loading', false)
        .set('recipe', emptyRecipe)
        .set('error', {});
    case RECIPE_GET_ERROR:
      return state.set('loading', false).set('error', error.response.data);
    default:
      return state;
  }
};

// --- AC ---
// get all recipes
export const getRecipes = () => dispatch => {
  dispatch({
    type: RECIPE_LOADING
  });
  axios
    .post('/api/recipes/all')
    .then(res => {
      dispatch({
        type: RECIPE_GET_ALL,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: RECIPE_GET_ERROR,
        error
      })
    );
};
// get recipe by ID
export const getRecipeById = id => dispatch => {
  console.log('get recipe by ID');
  axios
    .post(`/api/recipes/${id}`)
    .then(res => {
      dispatch({
        type: RECIPE_GET_ONE,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: RECIPE_GET_ERROR,
        error
      })
    );
};

// add recipe
export const addRecipe = (recipe, history) => dispatch => {
  console.log('add recipe:', recipe);
  axios
    .post('/api/recipes', recipe)
    .then(res => {
      dispatch({
        type: RECIPE_ADD,
        payload: res.data
      });

      history.push('/recipes');
    })
    .catch(error =>
      dispatch({
        type: RECIPE_GET_ERROR,
        error
      })
    );
};

// save recipe
export const saveRecipe = (recipe, history) => dispatch => {
  console.log('===save recipe:', recipe);
};

// delete recipe
export const deleteRecipe = (id, history) => dispatch => {
  console.log('AC delete recipe, id', id, history);
  if (id === '') {
    history.push('/recipes');
    return;
  }
  axios
    .delete(`/api/recipes/${id}`)
    .then(res => {
      dispatch({
        type: RECIPE_DELETE,
        payload: res.data
      });
      history.push('/recipes');
    })
    .catch(error => {
      dispatch({
        type: RECIPE_GET_ERROR,
        error
      });
    });
};

// store recipe
export const storeRecipe = recipe => ({
  type: RECIPE_STORE,
  payload: recipe
});

// clear recipe
export const clearRecipe = () => ({
  type: RECIPE_CLEAR
});

export default reducer;
