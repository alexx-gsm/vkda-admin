import axios from 'axios';
import { appName } from '../../config';
import { Record } from 'immutable';

// --- MODULE NAME ---
export const moduleName = 'dishStore';

// --- ACTIONS ---
export const DISH_LOADING = `${appName}/${moduleName}/DISH_LOADING`;
export const DISH_GET_ALL = `${appName}/${moduleName}/DISH_GET_ALL`;
export const DISH_GET_ONE = `${appName}/${moduleName}/DISH_GET_ONE`;
export const DISH_ADD = `${appName}/${moduleName}/DISH_ADD`;
export const DISH_SAVE = `${appName}/${moduleName}/DISH_SAVE`;
export const DISH_DELETE = `${appName}/${moduleName}/DISH_DELETE`;
export const DISH_STORE = `${appName}/${moduleName}/DISH_STORE`;
export const DISH_CLEAR = `${appName}/${moduleName}/DISH_CLEAR`;
export const DISH_GET_ERROR = `${appName}/${moduleName}/DISH_GET_ERROR`;
export const DISH_SET_CATEGORY_TAB_INDEX = `${appName}/${moduleName}/DISH_SET_CATEGORY_TAB_INDEX`;

// --- INITIAL STATE ---
const emptyItem = {};

const ReducerRecord = Record({
  dishes: [],
  dish: emptyItem,
  tabIndex: 0,
  error: {},
  loading: false
});

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action;

  switch (type) {
    case DISH_LOADING:
      return state.set('loading', true);
    case DISH_GET_ALL:
      return (
        state
          .set('loading', false)
          .set('dishes', payload)
          // .set('dish', emptyItem)
          .set('error', {})
      );
    case DISH_GET_ONE:
      return state
        .set('loading', false)
        .set('dish', payload)
        .set('error', {});
    case DISH_ADD:
      return state
        .set('loading', false)
        .set('dishes', [...state.dishes, payload])
        .set('error', {});
    case DISH_SAVE:
      return state
        .set('loading', false)
        .set('dish', {})
        .set('error', {});
    case DISH_STORE:
      return state
        .set('loading', false)
        .set('dish', payload)
        .set('error', {});
    case DISH_DELETE:
      return state
        .set('loading', false)
        .set('dishes', [
          ...state.dishes.filter(item => item._id !== payload._id)
        ])
        .set('error', {});
    case DISH_CLEAR:
      return state
        .set('loading', false)
        .set('dish', emptyItem)
        .set('error', {});
    case DISH_SET_CATEGORY_TAB_INDEX:
      return state.set('tabIndex', payload);
    case DISH_GET_ERROR:
      return state.set('loading', false).set('error', error.response.data);
    default:
      return state;
  }
};

// --- AC ---
// get all dishes
export const getDishes = () => dispatch => {
  dispatch({
    type: DISH_LOADING
  });
  axios
    .post('/api/dishes/all')
    .then(res => {
      dispatch({
        type: DISH_GET_ALL,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: DISH_GET_ERROR,
        error
      })
    );
};
// get dish by ID
export const getDishById = id => dispatch => {
  axios
    .post(`/api/dishes/${id}`)
    .then(res => {
      dispatch({
        type: DISH_GET_ONE,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: DISH_GET_ERROR,
        error
      })
    );
};

// save dish
export const saveDish = (dish, history) => dispatch => {
  axios
    .post('/api/dishes', dish)
    .then(res => {
      dispatch({
        type: DISH_SAVE,
        payload: res.data
      });

      history.push('/dishes');
    })
    .catch(error =>
      dispatch({
        type: DISH_GET_ERROR,
        error
      })
    );
};

// set tab index
export const setTabIndex = index => ({
  type: DISH_SET_CATEGORY_TAB_INDEX,
  payload: index
});

// // save category
// export const saveCategory = (category, history) => dispatch => {
//   axios
//     .put('/api/categories', category)
//     .then(res => {
//       dispatch({
//         type: CATEGORY_STORE,
//         payload: res.data
//       });

//       history.push('/categories');
//     })
//     .catch(error =>
//       dispatch({
//         type: CATEGORY_GET_ERROR,
//         error
//       })
//     );
// };

// // delete category
// export const deleteCategory = (id, history) => dispatch => {
//   if (id === '') {
//     history.push('/categories');
//     return;
//   }
//   axios
//     .delete(`/api/categories/${id}`)
//     .then(res => {
//       dispatch({
//         type: CATEGORY_DELETE,
//         payload: res.data
//       });
//       history.push('/categories');
//     })
//     .catch(error => {
//       dispatch({
//         type: CATEGORY_GET_ERROR,
//         error
//       });
//     });
// };

// store dish
export const storeDish = dish => ({
  type: DISH_STORE,
  payload: dish
});

// clear dish
export const clearDish = () => ({
  type: DISH_CLEAR
});

export default reducer;
