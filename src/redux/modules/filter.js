import { appName } from "../../config";
import { Record } from "immutable";
import moment from "moment";

// --- MODULE NAME ---
export const moduleName = "filterStore";

// --- ACTIONS ---
export const FILTER_SET = `${appName}/${moduleName}/FILTER_SET`;

// --- INITIAL STATE ---
const minYear = 2018;
const maxYear = moment().year();

let arrYears = [];
for (let year = minYear; year <= maxYear; year++) {
  arrYears.push(year);
}

const ReducerRecord = Record({
  filters: {
    summuryYearMin: minYear,
    summuryYear: maxYear,
    summuryYearMax: maxYear,
    arrYears
  }
});

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action;

  switch (type) {
    case FILTER_SET:
      return state.set("filters", { ...state.filters, ...payload });
    default:
      return state;
  }
};

// --- AC ---
// set filters
export const setFilter = selectedYear => ({
  type: FILTER_SET,
  payload: {
    summuryYear: selectedYear
  }
});

export default reducer;
