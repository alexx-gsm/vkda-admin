import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'

// --- MODULE NAME ---
export const moduleName = 'categoryStore'

// --- ACTIONS ---
export const CATEGORY_LOADING = `${appName}/${moduleName}/CATEGORY_LOADING`
export const CATEGORY_GET_ALL = `${appName}/${moduleName}/CATEGORY_GET_ALL`
export const CATEGORY_GET_ONE = `${appName}/${moduleName}/CATEGORY_GET_ONE`
export const CATEGORY_ADD = `${appName}/${moduleName}/CATEGORY_ADD`
export const CATEGORY_SAVE = `${appName}/${moduleName}/CATEGORY_SAVE`
export const CATEGORY_DELETE = `${appName}/${moduleName}/CATEGORY_DELETE`
export const CATEGORY_STORE = `${appName}/${moduleName}/CATEGORY_STORE`
export const CATEGORY_CLEAR = `${appName}/${moduleName}/CATEGORY_CLEAR`
export const CATEGORY_GET_ERROR = `${appName}/${moduleName}/CATEGORY_GET_ERROR`
export const CATEGORY_SET_TAB_INDEX = `${appName}/${moduleName}/CATEGORY_SET_TAB_INDEX`

// --- INITIAL STATE ---
const emptyItem = {
  title: '',
  isRoot: false,
  rootId: null,
}

const ReducerRecord = Record({
  categories: [],
  category: emptyItem,
  tabIndex: 0,
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case CATEGORY_LOADING:
      return state.set('loading', true)
    case CATEGORY_GET_ALL:
      return state
        .set('loading', false)
        .set('categories', payload)
        .set('error', {})
    case CATEGORY_GET_ONE:
      return state
        .set('loading', false)
        .set('category', payload)
        .set('error', {})
    case CATEGORY_ADD:
      return state
        .set('loading', false)
        .set('categories', [...state.categories, payload])
        .set('error', {})
    case CATEGORY_SAVE:
      return state
        .set('loading', false)
        .set('category', {})
        .set('error', {})
    case CATEGORY_STORE:
      return state
        .set('loading', false)
        .set('category', payload)
        .set('error', {})
    case CATEGORY_DELETE:
      return state
        .set('loading', false)
        .set('categories', [
          ...state.categories.filter((item) => item._id !== payload._id),
        ])
        .set('error', {})
    case CATEGORY_SET_TAB_INDEX:
      return state.set('tabIndex', payload)
    case CATEGORY_CLEAR:
      return state
        .set('loading', false)
        .set('category', emptyItem)
        .set('error', {})
    case CATEGORY_GET_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    default:
      return state
  }
}

// --- AC ---
// get all categories
export const getCategories = () => (dispatch) => {
  dispatch({
    type: CATEGORY_LOADING,
  })
  axios
    .post(`${HOST}/api/categories/all`)
    .then((res) => {
      dispatch({
        type: CATEGORY_GET_ALL,
        payload: res.data,
      })
    })
    .catch((error) =>
      dispatch({
        type: CATEGORY_GET_ERROR,
        error,
      })
    )
}
// get category by ID
export const getCategoryById = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/categories/${id}`)
    .then((res) => {
      dispatch({
        type: CATEGORY_GET_ONE,
        payload: res.data,
      })
    })
    .catch((error) =>
      dispatch({
        type: CATEGORY_GET_ERROR,
        error,
      })
    )
}

// add category
export const addCategory = (category, history) => (dispatch) => {
  axios
    .post(`${HOST}/api/categories`, category)
    .then((res) => {
      dispatch({
        type: CATEGORY_ADD,
        payload: res.data,
      })

      history.push('/categories')
    })
    .catch((error) =>
      dispatch({
        type: CATEGORY_GET_ERROR,
        error,
      })
    )
}

// save category
export const saveCategory = (category, history) => (dispatch) => {
  axios
    .put(`${HOST}/api/categories`, category)
    .then((res) => {
      dispatch({
        type: CATEGORY_STORE,
        payload: res.data,
      })

      history.push('/categories')
    })
    .catch((error) =>
      dispatch({
        type: CATEGORY_GET_ERROR,
        error,
      })
    )
}

// delete category
export const deleteCategory = (id, history) => (dispatch) => {
  if (id === '') {
    history.push('/categories')
    return
  }
  axios
    .delete(`${HOST}/api/categories/${id}`)
    .then((res) => {
      dispatch({
        type: CATEGORY_DELETE,
        payload: res.data,
      })
      history.push('/categories')
    })
    .catch((error) => {
      dispatch({
        type: CATEGORY_GET_ERROR,
        error,
      })
    })
}

// set tab index
export const setTabIndex = (index) => ({
  type: CATEGORY_SET_TAB_INDEX,
  payload: index,
})

// store category
export const storeCategory = (category) => ({
  type: CATEGORY_STORE,
  payload: category,
})

// clear category
export const clearCategory = () => ({
  type: CATEGORY_CLEAR,
})

export default reducer
