import axios from 'axios'
import jwt_decode from 'jwt-decode'
import { Record } from 'immutable'
// useful tools
import { setAuthToken } from '../../helpers/authHelpers'
import isEmpty from '../../helpers/is-empty'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'auth'

// --- ACTIONS ---
export const AUTH_LOADING = `${appName}/${moduleName}/LOADING`
export const AUTH_SET_USER = `${appName}/${moduleName}/SET_USER`
export const AUTH_GET_ERROR = `${appName}/${moduleName}/GET_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  isAuthenticated: false,
  user: {},
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case AUTH_LOADING:
      return state.set('loading', true)
    case AUTH_SET_USER:
      return state
        .set('loading', false)
        .set('isAuthenticated', !isEmpty(payload))
        .set('user', payload)
        .set('error', {})
    case AUTH_GET_ERROR:
      return state.set('loading', false).set('error', error.response.data)
    default:
      return state
  }
}

// --- AC ---
// Register user
export const registerUser = (userData, history) => (dispatch) => {
  axios
    .post(`${HOST}/api/users/register`, userData)
    .then(() => history.push('/login'))
    .catch((error) =>
      dispatch({
        type: AUTH_GET_ERROR,
        error,
      })
    )
}

// Login - Get User Token
export const loginUser = (userData, history) => (dispatch) => {
  axios
    .post(`${HOST}/api/users/login`, userData)
    .then((res) => {
      const { token } = res.data

      localStorage.setItem('jwtToken', token)
      setAuthToken(token)

      dispatch({
        type: AUTH_SET_USER,
        payload: jwt_decode(token),
      })

      history.push('/dashboard')
    })
    .catch((error) =>
      dispatch({
        type: AUTH_GET_ERROR,
        error,
      })
    )
}

// Logout
export const logoutUser = (history) => (dispatch) => {
  localStorage.removeItem('jwtToken')
  setAuthToken(false)
  dispatch({
    type: AUTH_SET_USER,
    payload: {},
  })
  history.push('/login')
}

export default reducer
