import axios from 'axios'
import { PAYMENTS_GET_ALL, PAYMENTS_GET_ALL_SUCCESS } from '../modules/payment'
import { HOST } from '../../config'

export default () => (next) => (action) => {
  const { getPayments, week, type, ...rest } = action
  if (!getPayments) return next(action)

  next({ ...rest, type })

  return axios
    .post(`${HOST}/api/payments/week/${action.week}`)
    .then((result) => {
      return next({
        type: PAYMENTS_GET_ALL_SUCCESS,
        payload: result.data,
        week,
      })
    })
    .catch((error) =>
      next({ ...rest, error, type: `${PAYMENTS_GET_ALL}_FAILURE` })
    )
}
