export default theme => {
  return {
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper
    },
    toolbar: theme.mixins.toolbar,
    listItem: {
      '&.active$listItemRoot': {
        background: theme.palette.primary.main
      },
      '&.active $listItemIconRoot, &.active $listItemTextRoot': {
        color: theme.palette.primary.contrastText
      }
    },
    // listItemRoot: {
    //   '&.active': {
    //     color: theme.palette.primary.contrastText,
    //     background: theme.palette.primary.main
    //   }
    // },
    listItemRoot: {},
    listItemIconRoot: {},
    listItemTextRoot: {}
  };
};
