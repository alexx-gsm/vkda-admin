import deepOrange from '@material-ui/core/colors/deepOrange';

export default theme => ({
  margin: {
    margin: theme.spacing.unit
  },
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridTools: {
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },
  gridBtnBack: {
    [theme.breakpoints.up('sm')]: {
      padding: '6px 20px'
    }
  },
  card: {
    maxWidth: '560px',
    margin: '40px 0',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  cardHeader: {
    position: 'relative',
    margin: '0',
    top: '-20px',
    padding: '5px 10px',
    margin: '0 20px',
    background: `linear-gradient(60deg, ${deepOrange[500]}, ${
      deepOrange[600]
    })`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoHeaderTitle: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  // CARD BODY
  cardForm: {
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  wrapTitle: {
    marginBottom: '30px'
  },
  inputTitle: {
    fontSize: '46px',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  labelPrimary: {
    '&$cssFocused': {
      color: deepOrange[700]
    }
  },
  cssFocused: {},
  underlinePrimary: {
    '&:after': {
      borderBottomColor: deepOrange[500]
    }
  },
  paperDishInfo: {
    padding: '0 20px 20px',
    marginTop: '20px',
    backgroundColor: deepOrange[50]
  },
  iconHeaderTitleWrap: {
    position: 'absolute',
    top: '-3px',
    left: '-30px'
  },
  iconNewCustomer: {
    color: 'white'
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  btnPrimary: {
    color: theme.palette.common.white,
    backgroundColor: deepOrange[600],
    '&:hover': {
      backgroundColor: deepOrange[700]
    }
  }
});
