import { compose, withHandlers, lifecycle } from 'recompose';
import { connect } from 'react-redux';
// AC
import {
  getDishes,
  getDishById,
  storeDish,
  saveDish,
  setTabIndex
} from '../../../redux/modules/dish';
import { getCategories } from '../../../redux/modules/category';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';
// useful tools
import isEmpty from '../../../helpers/is-empty';
import DishEdit from './DishEdit';

export default compose(
  connect(
    ({ dishStore, categoryStore }) => {
      const { dishes, dish, loading, tabIndex } = dishStore;
      const { categories } = categoryStore;
      const dishCategory = categories.find(c => c.title === 'Блюда');
      return {
        dishes,
        dish,
        loading,
        tabIndex,
        categories: !isEmpty(dishCategory)
          ? categories.filter(c => c.rootId === dishCategory._id)
          : []
      };
    },
    { getDishes, getDishById, storeDish, saveDish, setTabIndex, getCategories }
  ),
  withHandlers({
    onChange: ({ dish, storeDish }) => name => event => {
      storeDish({
        ...dish,
        [name]: event.target.value
      });
    },
    onSave: ({ dish, saveDish, categories, tabIndex, history }) => () => {
      if (isEmpty(dish.category)) {
        saveDish(
          {
            ...dish,
            category: categories[tabIndex]._id
          },
          history
        );
      }
      saveDish(dish, history);
    }
  }),
  lifecycle({
    componentDidMount() {
      this.props.getDishes();
      this.props.getCategories();
      const { id } = this.props.match.params;
      if (!isEmpty(id)) {
        this.props.getDishById(id);
      }
    }
  }),
  withStyles(styles, { withTheme: true })
)(DishEdit);
