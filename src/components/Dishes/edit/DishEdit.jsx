import React from 'react';
import { Link } from 'react-router-dom';
// material-ui components
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import Divider from '@material-ui/core/Divider';
// @material-ui/icons
import ArrowBack from '@material-ui/icons/ArrowBack';
import FiberNew from '@material-ui/icons/FiberNew';
// useful tools
import isEmpty from '../../../helpers/is-empty';
import classNames from 'classnames';
// icons
import { Icon } from 'react-icons-kit';
import { rub } from 'react-icons-kit/fa/rub';
import { balanceScale } from 'react-icons-kit/fa/balanceScale';
import { trashO } from 'react-icons-kit/fa/trashO';

const DishEdit = ({
  dish,
  tabIndex,
  onChange,
  onSave,
  onDelete,
  error,
  categories,
  classes
}) => {
  const defaultCategory = !isEmpty(categories) ? categories[tabIndex]._id : '';

  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridBtnBack}>
          <Button
            mini
            variant="fab"
            to={'/dishes'}
            component={Link}
            aria-label="Back"
          >
            <ArrowBack />
          </Button>
        </Grid>
      </Grid>
      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant="display1" className={classes.typoHeaderTitle}>
              {isEmpty(dish._id) && (
                <Typography
                  variant="caption"
                  className={classes.iconHeaderTitleWrap}
                >
                  <FiberNew classes={{ root: classes.iconNewCustomer }} />
                </Typography>
              )}
              Блюдо
            </Typography>
          </Grid>
        </Paper>
        <form className={classes.cardForm} noValidate autoComplete="off">
          <TextField
            fullWidth
            id="title"
            label="Название"
            value={isEmpty(dish.title) ? '' : dish.title}
            onChange={onChange('title')}
            margin="dense"
            className={classes.wrapTitle}
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputTitle,
              classes: { underline: classes.underlinePrimary }
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />
          <Grid container className={classes.gridCategorySelect}>
            <Grid item xs={12} sm={5}>
              <TextField
                select
                fullWidth
                error={Boolean(error.category)}
                id="type-select"
                label="Категория"
                // className={classes.select}
                value={isEmpty(dish.category) ? defaultCategory : dish.category}
                onChange={onChange('category')}
                InputLabelProps={{
                  shrink: true,
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu
                  }
                }}
                InputProps={{
                  classes: { underline: classes.underlinePrimary }
                }}
                helperText={error.category ? error.category : ''}
                margin="dense"
              >
                {categories.map(option => (
                  <MenuItem key={option._id} value={option._id}>
                    {option.title}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
          </Grid>
          <Paper className={classes.paperDishInfo}>
            <Grid container spacing={16}>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  id="value"
                  label="Цена"
                  value={isEmpty(dish.price) ? 0 : dish.price}
                  onChange={onChange('price')}
                  type="number"
                  inputProps={{
                    min: 0,
                    step: 1
                  }}
                  InputLabelProps={{
                    FormLabelClasses: {
                      root: classes.labelPrimary,
                      focused: classes.cssFocused
                    }
                  }}
                  InputProps={{
                    classes: { underline: classes.underlinePrimary },
                    endAdornment: (
                      <InputAdornment position="end">
                        <Icon icon={rub} />
                      </InputAdornment>
                    )
                  }}
                  error={error.value}
                  helperText={error.value ? error.value : null}
                  margin="normal"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  id="amount"
                  label="Вес, г"
                  value={isEmpty(dish.weight) ? 0 : dish.weight}
                  onChange={onChange('weight')}
                  type="number"
                  min={1}
                  step={1}
                  InputLabelProps={{
                    FormLabelClasses: {
                      root: classes.labelPrimary,
                      focused: classes.cssFocused
                    }
                  }}
                  InputProps={{
                    classes: { underline: classes.underlinePrimary },
                    endAdornment: (
                      <InputAdornment position="end">
                        <Icon size={18} icon={balanceScale} />
                      </InputAdornment>
                    )
                  }}
                  margin="normal"
                />
              </Grid>
            </Grid>
          </Paper>

          <TextField
            fullWidth
            id="multiline-flexible"
            label="Комментарий"
            multiline
            rowsMax="5"
            value={isEmpty(dish.comment) ? '' : dish.comment}
            onChange={onChange('comment')}
            className={classes.textField}
            margin="normal"
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              classes: { underline: classes.underlinePrimary }
            }}
          />
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Button
              mini
              onClick={onDelete}
              variant="fab"
              aria-label="Delete"
              color="secondary"
              className={classes.margin}
            >
              <Icon size={18} icon={trashO} />
            </Button>
          </Grid>
          <Grid item>
            <Button
              to={'/dishes'}
              component={Link}
              aria-label="Back"
              variant="contained"
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label="Save"
              variant="contained"
              className={classNames(classes.margin, classes.btnPrimary)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

DishEdit.defaultProps = {
  dish: {},
  error: {},
  categories: []
};

export default DishEdit;
