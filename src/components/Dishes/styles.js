import deepOrange from '@material-ui/core/colors/deepOrange';

export default theme => ({
  padding: { padding: '20px' },
  paddingH: { padding: '0 20px' },

  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridTools: {
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },
  gridBtnBack: {
    [theme.breakpoints.up('sm')]: {
      padding: '6px 20px'
    }
  },
  buttonAdd: {
    color: 'white',
    backgroundColor: deepOrange[500],
    '&:hover': { backgroundColor: deepOrange[600] }
  },
  card: {
    maxWidth: '760px',
    margin: '40px 0',
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  cardHeader: {
    position: 'relative',
    margin: '0',
    top: '-20px',
    padding: '5px 10px',
    background: `linear-gradient(60deg, ${deepOrange[500]}, ${
      deepOrange[600]
    })`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoHeaderTitle: {
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  wrapper: {
    display: 'inline-block',
    margin: `0 ${theme.spacing.unit}`,
    position: 'relative'
  },
  buttonReload: {},
  fabProgress: {
    position: 'absolute',
    top: 4,
    left: 4,
    zIndex: 1
  },
  tabsIndicator: { backgroundColor: deepOrange[500] },
  tabRoot: {
    '&:hover': {
      color: deepOrange[500],
      opacity: 1
    },
    '&$tabSelected': {
      color: deepOrange[500],
      fontWeight: theme.typography.fontWeightMedium
    },
    '&:focus': {
      color: deepOrange[500]
    }
  },
  tabSelected: {},
  viewContainer: {
    padding: '20px'
  },
  viewItem: {
    marginBottom: '5px'
  },
  gridType: {
    alignItems: 'center',
    flexDirection: 'row',
    [theme.breakpoints.down('xs')]: {
      alignItems: 'flex-start',
      marginLeft: 0,
      flexDirection: 'column'
    }
  },
  typoType: {
    marginLeft: '10px',
    paddingLeft: '10px',
    borderLeft: '1px solid',
    [theme.breakpoints.down('xs')]: {
      marginLeft: 0
    }
  },
  paperTable: {
    borderRadius: 0,
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  tableRow: {
    cursor: 'pointer',
    '& td': { background: deepOrange[100] },
    '&:nth-child(odd) td': {
      background: deepOrange[50]
    },
    '&:hover td': { background: deepOrange[200] }
  },
  cellData: {
    width: '100%'
  },
  headerCellInfo: {
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    }
  },
  cellInfo: {
    whiteSpace: 'nowrap',
    background: deepOrange[50],
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    }
  }
});
