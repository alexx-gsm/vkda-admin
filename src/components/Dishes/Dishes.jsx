import React from 'react';
import { Link } from 'react-router-dom';
import SwipeableViews from 'react-swipeable-views';
// material-ui components
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core';
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add';
import Autorenew from '@material-ui/icons/Autorenew';
// useful tools
import isEmpty from '../../helpers/is-empty';

const Dishes = ({
  dishes,
  categories,
  loading,
  onClick,
  onReload,
  tabIndex,
  onTabChange,
  onChangeIndex,
  formatter,
  classes
}) => {
  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridTools}>
          <Button
            mini
            to={'/dishes/edit'}
            component={Link}
            variant="fab"
            aria-label="Add"
            className={classes.buttonAdd}
          >
            <AddIcon />
          </Button>
          <div className={classes.wrapper}>
            <IconButton
              onClick={onReload}
              className={classes.button}
              aria-label="Reload"
            >
              {loading ? '' : <Autorenew />}
            </IconButton>
            {loading && (
              <CircularProgress
                size={40}
                color="secondary"
                className={classes.fabProgress}
              />
            )}
          </div>
        </Grid>
      </Grid>

      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid container className={classes.gridHeader}>
            <Typography variant="display1" className={classes.typoHeaderTitle}>
              Блюда
            </Typography>
          </Grid>
        </Paper>
        {!isEmpty(categories) && (
          <AppBar position="static" color="default">
            <Tabs
              value={tabIndex}
              onChange={onTabChange}
              scrollable
              scrollButtons="on"
              classes={{
                root: classes.tabsRoot,
                indicator: classes.tabsIndicator
              }}
              fullWidth
            >
              {categories.map(c => (
                <Tab
                  key={c._id}
                  label={c.title}
                  classes={{
                    root: classes.tabRoot,
                    selected: classes.tabSelected
                  }}
                />
              ))}
            </Tabs>
          </AppBar>
        )}
        <Paper className={classes.paperTable}>
          <Grid container className={classes.gridTable}>
            <Table>
              <TableHead className={classes.tHead}>
                <TableRow>
                  <TableCell
                    className={classes.headCellData}
                    padding="checkbox"
                  >
                    <Typography variant="caption">Назваиние | вес</Typography>
                  </TableCell>

                  <TableCell
                    numeric
                    padding="checkbox"
                    className={classes.headerCellInfo}
                  >
                    Цена
                  </TableCell>
                </TableRow>
              </TableHead>
            </Table>
          </Grid>
          <SwipeableViews
            axis={'x'}
            index={tabIndex}
            onChangeIndex={onChangeIndex}
          >
            {categories.map((category, index) => {
              const categoriedDishes = dishes.filter(
                d => d.category === category._id
              );
              if (isEmpty(categoriedDishes)) {
                return (
                  <Typography key={index} className={classes.paddingH}>
                    {'---'}
                  </Typography>
                );
              } else {
                return (
                  <Table key={index}>
                    <TableBody>
                      {categoriedDishes.map(dish => (
                        <TableRow
                          key={dish._id}
                          className={classes.tableRow}
                          onClick={onClick(dish._id)}
                        >
                          <TableCell
                            className={classes.cellData}
                            padding="checkbox"
                          >
                            <Grid container className={classes.gridInfo}>
                              <Grid item container className={classes.gridType}>
                                <Typography variant="title">
                                  {dish.title}
                                </Typography>

                                <Typography
                                  className={classes.typoType}
                                  variant="caption"
                                >
                                  {dish.weight} г.
                                </Typography>
                              </Grid>
                            </Grid>
                          </TableCell>
                          <TableCell
                            className={classes.cellData}
                            padding="checkbox"
                          >
                            <Grid container className={classes.gridInfo}>
                              <Grid item container className={classes.gridType}>
                                <Typography variant="title">
                                  {formatter.format(dish.price)}
                                </Typography>
                              </Grid>
                            </Grid>
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                );
              }
            })}
          </SwipeableViews>
        </Paper>
      </Paper>
    </div>
  );
};

Dishes.defaultProps = {
  dishes: [],
  categories: []
};

export default Dishes;
