import { compose, withProps, withHandlers, lifecycle } from 'recompose';
import { connect } from 'react-redux';
// AC
import { getDishes, clearDish, setTabIndex } from '../../redux/modules/dish';
import { getCategories } from '../../redux/modules/category';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// useful tools
import isEmpty from '../../helpers/is-empty';
// styles
import styles from './styles';
import Dishes from './Dishes';

export default compose(
  connect(
    ({ dishStore, categoryStore }) => {
      const { dishes, loading, tabIndex } = dishStore;
      const { categories } = categoryStore;
      const dishCategory = categories.find(c => c.title === 'Блюда');
      return {
        dishes,
        loading,
        tabIndex,
        categories: !isEmpty(dishCategory)
          ? categories.filter(c => c.rootId === dishCategory._id)
          : []
      };
    },
    { getDishes, clearDish, setTabIndex, getCategories }
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0
    })
  }),
  withHandlers({
    onClick: ({ history }) => id => () => history.push(`/dishes/edit/${id}`),
    onReload: ({ getDishes }) => () => getDishes(),
    onTabChange: ({ setTabIndex }) => (event, value) => setTabIndex(value),
    onChangeIndex: ({ setTabIndex }) => index => setTabIndex(index)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearDish();
      this.props.getDishes();
      this.props.getCategories();
    }
  }),
  withStyles(styles, { withTheme: true })
)(Dishes);
