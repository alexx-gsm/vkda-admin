import blue from '@material-ui/core/colors/blue';
import grey from '@material-ui/core/colors/grey';
import red from '@material-ui/core/colors/red';
import indigo from '@material-ui/core/colors/indigo';
import teal from '@material-ui/core/colors/teal';
import orange from '@material-ui/core/colors/orange';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';

export default theme => ({
  padding: { padding: '20px' },
  paddingH: { padding: '0 20px' },
  fullWidth: { width: '100%' },
  underlineWhite: {
    '&:before': {
      borderBottomColor: theme.palette.common.white + '!important'
    },
    '&:after': {
      borderBottomColor: theme.palette.common.white
    }
  },

  // card header
  card: {
    maxWidth: '760px',
    margin: '40px 0',
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  cardHeader: {
    position: 'relative',
    margin: '0',
    top: '-20px',
    padding: '5px 10px',
    background: `linear-gradient(60deg, ${blue[500]}, ${blue[600]})`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridHeader: {
    justifyContent: 'space-between'
  },
  typoCardHeaderNumber: {
    color: theme.palette.common.white
  },
  cellIcon: {
    minWidth: '16px',
    '& svg': {
      width: '32px',
      height: '32px'
    }
  },
  gridOrderInfo: {
    paddingBottom: '5px'
  },
  customerName: {
    color: grey[800],
    marginBottom: '8px',
    [theme.breakpoints.down('xs')]: {
      lineHeight: '1',
      fontSize: '1.75rem'
    }
  },
  personIcon: {
    height: '16px',
    color: grey[700],
    marginRight: '3px',
    paddingLeft: '5px',
    borderLeft: `1px solid ${grey[700]}`
  },
  phoneIcon: {
    height: '16px',
    color: grey[700],
    marginRight: '3px',
    paddingLeft: '5px',
    borderLeft: `1px solid ${grey[700]}`
  },
  typoPhone: { marginRight: '5px' },
  typoPerson: { marginRight: '5px' },
  mapIcon: {
    height: '16px',
    color: grey[700],
    marginRight: '3px',
    paddingLeft: '5px',
    borderLeft: `1px solid ${grey[700]}`
  },
  tableRow: {
    '&.grey td': {
      background: grey[50]
    },
    '&.grey td:first-child': {
      color: grey[800]
    },
    '&.red td': {
      background: red[50]
    },
    '&.red td:first-child': {
      color: red[800]
    },
    '&.teal td': {
      background: teal[50]
    },
    '&.teal td:first-child': {
      color: teal[800]
    },
    '&.orange td': {
      background: orange[50]
    },
    '&.orange td:first-child': {
      color: orange[800]
    },
    '&.indigo td': {
      background: indigo[50]
    },
    '&.indigo td:first-child': {
      color: indigo[800]
    },
    '&.green td': {
      background: green[50]
    },
    '&.green td:first-child': {
      color: green[800]
    }
  },
  cellCustomer: {
    borderLeft: '1px solid #e0e0e0',
    padding: '5px 12px'
  },
  gridMapInfo: {
    display: 'inline-flex',
    width: 'auto'
  },
  inputDeliveryDay: {
    fontSize: '40px',
    fontWeight: 'bold',
    color: theme.palette.common.white,
    padding: 0,
    width: '50px',
    textAlign: 'center',
    cursor: 'pointer'
  },
  typoDeliveryDay: {
    color: theme.palette.common.white
  },
  daypickerContainer: {
    left: 'inherit',
    right: 0
  },
  headerCardButton: {
    '&:hover': {
      borderRadius: 0,
      background: 'transparent'
    }
  },
  headerCardIcons: {
    color: theme.palette.common.white,
    opacity: 0.7,
    transition: 'easy 0.3s',
    width: '48px',
    height: '48px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '&.is-active': {
      color: theme.palette.common.white,
      opacity: 1,
      transition: 'easy 0.3s',
      borderBottom: '1px solid white'
      // borderRadius: '5px'
    },
    '&:hover': {
      transition: 'easy 0.3s',
      borderBottom: '1px solid white'
      // borderRadius: '5px'
    },
    '& svg': {
      width: '32px',
      height: '32px'
    }
  },
  headerCardMobileButton: {
    width: 'auto',
    height: '33px',
    color: 'black'
  },
  headerCardMobileIcons: {
    marginRight: '10px'
  },
  selectFilterForm: {
    '& $headerCardMobileIcons': {
      color: 'white'
    }
  },
  buttonAll: {
    color: theme.palette.common.white,
    opacity: 0.7,
    width: '48px',
    height: '48px',
    lineHeight: '48px',
    '&.is-active': {
      color: theme.palette.common.white,
      opacity: 1,
      transition: 'easy 0.3s',
      borderBottom: '1px solid white'
      // borderRadius: '5px'
    },
    '&:hover': {
      transition: 'easy 0.3s',
      borderBottom: '1px solid white'
    },
    [theme.breakpoints.down('xs')]: {
      color: 'black',
      width: 'inherit',
      minWidth: '150px',
      textAlign: 'left',
      height: 'auto',
      lineHeight: '33px',
      textTransform: 'capitalize',
      '&.is-active': {
        borderBottom: 'none'
      }
    }
  },
  selectIconWhite: {
    color: theme.palette.common.white
  },
  overlayWrapper: {
    position: 'relative',
    color: '#333',

    [theme.breakpoints.down('xs')]: {
      position: 'fixed',
      top: '60px',
      left: 0,
      right: 0,
      bottom: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  },
  overlay: {
    position: 'absolute',
    zIndex: 1,
    background: theme.palette.common.white,
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.15)',
    right: 0,
    left: 'auto',
    paddingTop: '20px',
    [theme.breakpoints.down('xs')]: {
      position: 'static'
    }
  },
  badge: {
    width: '18px',
    height: '18px',
    top: '-5px',
    right: '-10px',
    border: `2px solid ${grey[200]}`,
    background: orange[900]
  },

  gridOrderIconStatistics: {
    padding: '8px 5px 5px',
    marginBottom: '5px',
    boxShadow: `1px 1px 5px ${grey[500]}`,
    borderRadius: '3px',
    '&.new': {
      background: red[100],
      border: `1px solid ${red[200]}`
    },
    '&.onProcess': {
      background: indigo[100],
      border: `1px solid ${indigo[200]}`
    },
    '&.isReady': {
      background: teal[100],
      border: `1px solid ${teal[200]}`
    }
  },
  gridOrderIcon: {
    marginRight: '8px'
  }
});
