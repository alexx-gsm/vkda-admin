import React from 'react';
// moment utils
import MomentLocaleUtils, {
  formatDate,
  parseDate
} from 'react-day-picker/moment';
import 'moment/locale/ru';
import moment from 'moment';
// useful tools
import isEmpty from '../../../helpers/is-empty';
// Material UI
import { Typography } from '@material-ui/core';
// Day Picker
import DayPickerInput from 'react-day-picker/DayPickerInput';

function DayPickerOverlay({
  classes,
  classNames,
  selectedDay,
  children,
  ...props
}) {
  return (
    <div className={classes.overlayWrapper} {...props}>
      <div className={classes.overlay}>
        <Typography variant="caption" align="center">
          Выбери день
        </Typography>
        {children}
      </div>
    </div>
  );
}

class Btn extends React.Component {
  render() {
    return (
      <button className="text-select-day" {...this.props}>
        {this.props.value}
      </button>
    );
  }
}

const DayPicker = ({ value, format, classes, onDayClick }) => {
  return (
    <div className="SelectedWeek">
      <DayPickerInput
        component={Btn}
        value={moment(value).toDate()}
        formatDate={formatDate}
        parseDate={parseDate}
        format={format}
        dayPickerProps={{
          showWeekNumbers: true,
          showOutsideDays: true,
          firstDayOfWeek: 1,
          todayButton: 'Сегодня',
          locale: 'ru',
          localeUtils: MomentLocaleUtils,
          onDayClick: onDayClick,
          onTodayButtonClick: onDayClick
        }}
        overlayComponent={props => DayPickerOverlay({ ...props, classes })}
        keepFocus={false}
      />
    </div>
  );
};

export default DayPicker;
