import React from 'react';
// moment utils
import MomentLocaleUtils, {
  formatDate,
  parseDate
} from 'react-day-picker/moment';
import 'moment/locale/ru';
// useful tools
import isEmpty from '../../../helpers/is-empty';
// Material UI
import { Typography, TextField } from '@material-ui/core';
// Day Picker
import DayPickerInput from 'react-day-picker/DayPickerInput';
import moment from 'moment';

const DayPickerOverlay = ({
  classes,
  classNames,
  selectedDay,
  children,
  ...props
}) => {
  return (
    <div className={classes.overlayWrapper} {...props}>
      <div className={classes.overlay}>
        <Typography variant="caption" align="center">
          Выбери день
        </Typography>
        {children}
      </div>
    </div>
  );
};

const withDayInput = ({ ...props }) => {
  return class extends React.Component {
    render() {
      return (
        <TextField
          id="delivery"
          margin={this.props.margin ? this.props.margin : 'normal'}
          fullWidth
          {...this.props}
          {...props}
        >
          {this.props.value}
        </TextField>
      );
    }
  };
};

const DayPicker = ({
  value,
  format,
  classes,
  onDayClick,
  InputLabelProps,
  InputProps,
  label,
  margin
}) => {
  return (
    <DayPickerInput
      component={withDayInput({
        InputLabelProps,
        InputProps,
        label,
        margin
      })}
      formatDate={formatDate}
      value={moment(value).toDate()}
      parseDate={parseDate}
      format={format}
      dayPickerProps={{
        showWeekNumbers: true,
        showOutsideDays: true,
        firstDayOfWeek: 1,
        todayButton: 'Сегодня',
        locale: 'ru',
        localeUtils: MomentLocaleUtils,
        onDayClick: onDayClick,
        onTodayButtonClick: onDayClick
      }}
      overlayComponent={props => DayPickerOverlay({ ...props, classes })}
    />
  );
};

export default DayPicker;
