import {
  compose,
  lifecycle,
  withState,
  withProps,
  withHandlers
} from 'recompose';
import { connect } from 'react-redux';
// AC
import {
  getOrdersInInterval,
  getOrderById,
  storeOrder,
  saveOrder,
  deleteOrder,
  setError
} from '../../../redux/modules/order';
import { getCustomers } from '../../../redux/modules/customer';
import { getDishes } from '../../../redux/modules/dish';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';
// useful tools
import isEmpty from '../../../helpers/is-empty';
import moment from 'moment';
// Components
import withDialog from '../../Dialog';
import OrderEdit from './OrderEdit';

const calcTotal = (dishes, orderedDishes) => {
  return orderedDishes.reduce((acc, { dish, amount }) => {
    const _dish = dishes.find(d => d._id === dish);
    return isEmpty(_dish) ? acc : acc + +_dish.price * +amount;
  }, 0);
};

export default compose(
  connect(
    ({ orderStore, customerStore, dishStore }) => {
      const {
        filteredOrders,
        selectedDay,
        order,
        statuses,
        error,
        loading
      } = orderStore;
      const orderedDishes = !isEmpty(order) ? order.dishes : [];
      const { customers } = customerStore;
      const { dishes } = dishStore;
      return {
        filteredOrders,
        selectedDay,
        order,
        orderedDishes,
        statuses,
        error,
        loading,
        customers,
        dishes
      };
    },
    {
      getOrdersInInterval,
      getOrderById,
      storeOrder,
      saveOrder,
      getCustomers,
      getDishes,
      setError,
      onConfirm: deleteOrder
    }
  ),
  withState('isVisible', 'setVisible', false),
  withState('deletingItemId', 'setDeletingItemId', ''),
  withProps({
    modalTitle: 'Удаление документа',
    modalText: 'Вы уверены, что хотите удалить этот документ?'
  }),
  withState('deliveryDate', 'setDeliveryDate', ''),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0
    })
  }),
  withHandlers({
    onChange: ({ order, storeOrder }) => name => event => {
      storeOrder({
        ...order,
        [name]: event.target.value
      });
    },
    onDayChange: ({
      order,
      storeOrder,
      getOrdersInInterval,
      setDeliveryDate
    }) => date => {
      // setDeliveryDate(date);
      const day = moment(date).startOf('date');
      const nextDay = moment(day).add(1, 'd');

      getOrdersInInterval(day, nextDay);
      storeOrder({
        ...order,
        deliveryDate: date
      });
    },
    onSave: ({
      filteredOrders,
      order,
      customers,
      history,
      saveOrder,
      setError
    }) => () => {
      if (isEmpty(order._id)) {
        const customerID = !isEmpty(order.customer) ? order.customer : null;
        if (!customerID) {
          setError({ customer: 'Укажите клиента' });
          return;
        }

        if (isEmpty(order.dishes)) {
          setError({ dishes: 'Укажите блюда' });
          return;
        }
        const customer = customers.find(c => c._id === customerID);
        const number = `${customer.acronym}-${moment().format(
          'YY-DDDD'
        )}-${filteredOrders.filter(o => o.customer === customerID).length + 1}`;

        saveOrder(
          {
            ...order,
            number
          },
          history
        );
      } else {
        saveOrder(order, history);
      }
    },

    onDelete: ({ deletingItemId, setDeletingItemId, setVisible }) => () => {
      setDeletingItemId(deletingItemId);
      setVisible(true);
    },

    onAddDish: ({ order, storeOrder }) => () => {
      // check if empty dish row is already in order (w/ id=0)
      if (!isEmpty(order.dishes.find(d => d.dish === '0'))) return;
      storeOrder({
        ...order,
        dishes: [
          ...order.dishes,
          {
            dish: '0',
            amount: 0,
            is_ready: false
          }
        ]
      });
    },
    onSelectDish: ({ order, dishes, storeOrder }) => index => event => {
      const selectedDishID = event.target.value;

      if (selectedDishID === '0') return;

      const selectedDish = dishes.find(d => d._id === selectedDishID);

      const orderedDishes = [
        ...order.dishes.slice(0, index),
        {
          dish: selectedDishID,
          amount: 1,
          is_ready: false
        },
        ...order.dishes.slice(index + 1)
      ];

      storeOrder({
        ...order,
        dishes: orderedDishes,
        total: calcTotal(dishes, orderedDishes)
      });
    },
    onChangeAmount: ({ order, dishes, storeOrder }) => (
      index,
      delta
    ) => event => {
      const newAmount =
        delta === 1 || delta === -1
          ? +order.dishes[index].amount + delta
          : event.target.value;

      if (+newAmount < 1) {
        return;
      }

      const orderedDishes = [
        ...order.dishes.slice(0, index),
        {
          ...order.dishes.slice(index, index + 1)[0],
          amount: newAmount
        },
        ...order.dishes.slice(index + 1)
      ];

      storeOrder({
        ...order,
        dishes: orderedDishes,
        total: calcTotal(dishes, orderedDishes)
      });
    },
    onDeleteDish: ({ order, dishes, storeOrder }) => index => () => {
      const orderedDishes = order.dishes.filter((d, i) => index !== i);

      storeOrder({
        ...order,
        dishes: orderedDishes,
        total: calcTotal(dishes, orderedDishes)
      });
    }
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params;
      if (!isEmpty(id)) {
        this.props.getOrderById(id);
        this.props.setDeletingItemId(id);
      } else {
        this.props.storeOrder({
          ...this.props.order,
          deliveryDate: this.props.selectedDay || moment().toDate()
        });
      }
      this.props.getCustomers();
      this.props.getDishes();
    }
  }),
  withStyles(styles, { withTheme: true }),
  withDialog
)(OrderEdit);
