import blue from '@material-ui/core/colors/blue';
import grey from '@material-ui/core/colors/grey';
import red from '@material-ui/core/colors/red';
import deepOrange from '@material-ui/core/colors/deepOrange';

export default theme => ({
  padding: { padding: '20px' },
  paddingH: { padding: '10px 20px' },
  margin: {
    margin: theme.spacing.unit
  },
  textCenter: {
    textAlign: 'center'
  },
  w100: { width: '100%' },
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridTools: {
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },
  gridBtnBack: {
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },

  buttonPrimary: {
    color: 'white',
    backgroundColor: blue[500],
    '&:hover': { backgroundColor: blue[700] }
  },
  card: {
    maxWidth: '800px',
    margin: '40px 0',

    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0',
      marginBottom: 0
    }
  },
  // CARD HEADER
  cardHeader: {
    position: 'relative',
    margin: '0 20px',
    top: '-20px',
    padding: '5px 10px',
    background: `linear-gradient(60deg, ${blue[500]}, ${blue[700]})`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px 15px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0',
      borderRadius: 0
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end'
  },
  typoHeaderTitle: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase',
    textAlign: 'right',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.5rem'
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.25rem'
    }
  },
  iconNewItem: {
    color: theme.palette.common.white
  },
  // CARD BODY
  cardForm: {
    padding: '0 20px 20px',
    '& .DayPickerInput': { width: '100%' },
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  wrapTitle: {
    marginBottom: '0'
  },
  inputTitle: {
    fontSize: '46px',
    color: '#666',
    paddingBottom: 0,
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },

  labelPrimary: {
    '&$cssFocused': {
      color: blue[700]
    }
  },
  cssFocused: {},
  underlinePrimary: {
    '&:before': {
      borderBottomColor: blue[500] + '!important'
    },
    '&:after': {
      borderBottomColor: blue[500]
    }
  },
  underlineWhite: {
    '&:before': {
      borderBottomColor: theme.palette.common.white + '!important'
    },
    '&:after': {
      borderBottomColor: theme.palette.common.white
    }
  },
  selectIconWhite: {
    color: theme.palette.common.white
  },
  gridType: {
    alignItems: 'baseline',
    flexDirection: 'row',
    '& $typoSelectInputTitle': {
      marginRight: '15px'
    }
  },
  selectInput: {
    paddingBottom: '5px',
    '& $typoSelectInputTitle': {
      fontSize: '46px',
      lineHeight: 1,
      paddingRight: '10px',
      whiteSpace: 'normal'
    },
    '& $typoSelectInputSubtitle': {
      fontSize: '20px',
      lineHeight: 1
    }
  },
  typoSelectInputTitle: {},
  typoSelectInputSubtitle: {
    // marginLeft: '10px',
    paddingLeft: '10px',
    borderLeft: '1px solid',
    [theme.breakpoints.down('xs')]: {
      // paddingLeft: '5px',
      // paddingBottom: '5px'
      // borderLeft: 'none',
      // marginLeft: 0
    }
  },
  // formControl: {
  //   margin: theme.spacing.unit
  // },
  daypickerContainer: {
    width: '100%'
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  btnPrimary: {
    color: theme.palette.common.white,
    backgroundColor: blue[600],
    '&:hover': {
      backgroundColor: blue[700]
    }
  },
  // ORDER TABLE
  tableWrap: {
    margin: '0 -8px'
  },
  table: {
    marginTop: '40px'
  },
  headRow: {
    height: '28px',
    minHeight: '28px',
    '& th': {
      backgroundColor: blue[500],
      color: theme.palette.common.white,
      padding: '5px',
      textTransform: 'uppercase'
    }
  },
  tableRow: {
    height: '28px',
    '&:first-child td': { paddingTop: '10px' },
    '&:nth-child(even) td': { backgroundColor: blue[50] },
    '&:last-child td': { backgroundColor: 'white' },
    '& td': {
      borderBottom: 'none',
      padding: '0',
      fontSize: '1rem',
      '&:not(:first-child)': { padding: '0 5px' },
      '&:last-child': { padding: '5px' },
      '&.icon-remove': {
        paddingLeft: '5px',
        paddingTop: '5px'
      },
      '&.icon-add': {
        paddingTop: '10px'
      }
    },
    '& td input': {
      width: '50px',
      textAlign: 'center',
      padding: 0
    }
  },
  removeIcon: {
    color: grey[400],
    transition: 'color ease 0.3s',
    cursor: 'pointer',
    '&:hover': {
      color: red[800],
      transition: 'color ease 0.3s'
    }
  },
  addIcon: {
    fontSize: '35px',
    color: blue[600],
    transition: 'color ease 0.3s',
    cursor: 'pointer',
    '&:hover': {
      color: blue[700],
      transition: 'color ease 0.3s'
    }
  },
  cellInput: {
    height: '32px'
  },
  paperTotal: {
    boxShadow: 'none',
    borderRadius: 0,
    borderTop: `4px solid ${grey[800]}`,
    marginTop: '-20px'
  },
  gridTotal: {
    paddingTop: 0
  },
  typoTotal: {
    marginLeft: '40px',
    lineHeight: 1.2,
    color: grey[800]
  },
  // MOBILE CARDS
  gridCards: {
    padding: '15px'
  },
  gridCard: {
    marginBottom: '15px'
  },
  paperCardRoot: {
    borderColor: deepOrange[500]
  },
  paperCard: {
    border: `1px solid ${deepOrange[100]}`,
    borderRadius: '0 0 4px 4px',
    '& $selectItem': {
      background: deepOrange[500],
      padding: '5px 10px',
      boxSizing: 'border-box'
    },
    '& $gridCard': {
      paddingTop: '15px'
    }
  },
  inputSelect: {
    color: theme.palette.common.white,
    fontWeight: 'bold',
    textTransform: 'uppercase'
  },
  selectItem: {},
  inputText: {
    color: 'white'
  },
  gridCardInfo: {
    padding: '13px 10px 10px'
  },
  gridTotalValue: {
    padding: '4px 10px 0',
    borderTop: `1px solid ${deepOrange[100]}`,
    background: grey[50]
  },
  typoRowTotal: {
    paddingLeft: '15px',
    minWidth: '100px',
    textAlign: 'right'
  },
  typoCardTitle: {
    paddingRight: '10px'
  },
  typoCardAmount: {
    padding: '0 20px',
    margin: '0 5px',
    borderBottom: '1px solid'
  },
  buttonCardPrimary: {
    color: 'white',
    backgroundColor: deepOrange[500],
    '&:hover': { backgroundColor: deepOrange[700] }
  },
  paperCardTotal: {
    boxShadow: 'none',
    borderRadius: 0,
    borderTop: `4px solid ${grey[800]}`,
    margin: '40px 10px 10px'
  },
  inputStatus: {
    paddingTop: 0,
    paddingBottom: 0
  },
  gridStatusSelect: {
    paddingTop: 0,
    paddingBottom: 0,
    height: '32px',
    position: 'relative'
  },
  gridStatusIcon: {
    position: 'absolute',
    bottom: 0,
    padding: 0,
    '& svg': {
      width: '32px',
      height: '32px'
    }
  },
  gridStatusTitle: {
    marginLeft: '40px',
    lineHeight: 1
  },
  // DAY PICKER STYLES
  overlayWrapper: {
    position: 'relative',
    color: '#333',

    [theme.breakpoints.down('xs')]: {
      position: 'fixed',
      top: '60px',
      left: 0,
      right: 0,
      bottom: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  },
  overlay: {
    position: 'absolute',
    zIndex: 1,
    background: theme.palette.common.white,
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.15)',
    // right: 0,
    // left: 'auto',
    paddingTop: '20px',
    [theme.breakpoints.down('xs')]: {
      position: 'static'
    }
  }
});
