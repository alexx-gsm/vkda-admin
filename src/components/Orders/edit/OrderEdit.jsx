import React from 'react';
import { Link } from 'react-router-dom';
// Components
// import InputDayPicker from '../../InputDayPicker';
import DayPicker from './DayPicker';
// useful utils
import isEmpty from '../../../helpers/is-empty';
import classNames from 'classnames';
// material-ui components
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core';
// @material-ui/icons
import ArrowBack from '@material-ui/icons/ArrowBack';
import FiberNew from '@material-ui/icons/FiberNew';
import AddCircle from '@material-ui/icons/AddCircle';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import LeftIcon from '@material-ui/icons/ChevronLeft';
import RightIcon from '@material-ui/icons/ChevronRight';
// icons
import { Icon } from 'react-icons-kit';
import { trashO } from 'react-icons-kit/fa/trashO';

function OrderEdit({
  order,
  loading,
  orderedDishes,
  statuses,
  error,
  customers,
  dishes,
  onChange,
  onDayChange,
  onSave,
  onDelete,
  onAddDish,
  onSelectDish,
  onChangeAmount,
  onDeleteDish,
  formatter,
  classes
}) {
  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridBtnBack}>
          <Button
            mini
            variant="fab"
            to={'/orders'}
            component={Link}
            aria-label="Back"
            className={classes.buttonPrimary}
          >
            <ArrowBack />
          </Button>
        </Grid>
      </Grid>

      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid
            container
            className={classes.gridCardHeader}
            alignItems="center"
            justify="space-between"
          >
            <Grid item xs={8} sm={6}>
              {!isEmpty(order._id) && (
                <TextField
                  fullWidth
                  id="number"
                  value={isEmpty(order.number) ? '' : order.number}
                  onChange={onChange('number')}
                  margin="none"
                  InputProps={{
                    classes: {
                      input: classes.inputText,
                      underline: classes.underlineWhite
                    }
                  }}
                />
              )}
            </Grid>
            <Grid item container xs={4} sm={6} justify="flex-end">
              <Grid item>
                {isEmpty(order._id) && (
                  <FiberNew classes={{ root: classes.iconNewItem }} />
                )}
              </Grid>
              <Grid item>
                <Typography
                  variant="display1"
                  className={classes.typoHeaderTitle}
                >
                  Заказ
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Paper>

        <form className={classes.cardForm} noValidate autoComplete="off">
          <TextField
            select
            fullWidth
            id="customer-select"
            label="Клиент"
            value={isEmpty(order.customer) ? '' : order.customer}
            onChange={onChange('customer')}
            margin="normal"
            className={classes.wrapTitle}
            InputLabelProps={{
              // shrink: true,
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputTitle,
              classes: { underline: classes.underlinePrimary }
            }}
            SelectProps={{
              classes: {
                select: classes.selectInput
              }
            }}
            error={Boolean(error.customer)}
            helperText={error.customer ? error.customer : null}
          >
            {customers.map(option => (
              <MenuItem key={option._id} value={option._id}>
                <Grid
                  container
                  className={classes.gridType}
                  direction="row"
                  wrap="wrap"
                >
                  <Typography
                    className={classes.typoSelectInputTitle}
                    variant="title"
                  >
                    {option.name}
                  </Typography>
                  <Typography
                    className={classes.typoSelectInputSubtitle}
                    variant="caption"
                  >
                    {option.person}
                  </Typography>
                </Grid>
              </MenuItem>
            ))}
          </TextField>

          <Grid container spacing={16}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                select
                id="number"
                label="Статус"
                value={isEmpty(order.status) ? 'draft' : order.status}
                onChange={onChange('status')}
                className={classes.select}
                margin="normal"
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    input: classes.inputStatus,
                    underline: classes.underlinePrimary
                  }
                }}
              >
                {statuses.map(option => (
                  <MenuItem
                    key={option.title}
                    value={option.title}
                    disabled={
                      option.title !== 'draft' && option.title !== 'new'
                    }
                  >
                    <Grid
                      container
                      className={classes.gridStatusSelect}
                      direction="row"
                      alignItems="center"
                      wrap="nowrap"
                    >
                      <Grid
                        container
                        item
                        direction="row"
                        alignItems="center"
                        className={classes.gridStatusIcon}
                      >
                        {option.icon}
                      </Grid>
                      <Grid item className={classes.gridStatusTitle}>
                        {option.alias}
                      </Grid>
                    </Grid>
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <DayPicker
                classes={classes}
                format="D MMMM YYYY"
                value={order.deliveryDate}
                onDayClick={onDayChange}
                margin="normal"
                label="Дата доставки"
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    underline: classes.underlinePrimary
                  }
                }}
              />
            </Grid>
          </Grid>

          <Hidden xsDown>
            <div className={classes.tableWrap}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow className={classes.headRow}>
                    <TableCell />
                    <TableCell numeric style={{ paddingRight: '37px' }}>
                      Блюда
                    </TableCell>
                    <TableCell className={classes.textCenter}>Цена</TableCell>
                    <TableCell className={classes.textCenter}>Кол-во</TableCell>
                    <TableCell numeric>Итог</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {!isEmpty(dishes) &&
                    orderedDishes.map(({ dish, amount }, index) => {
                      const _dish = dishes.find(d => d._id === dish);
                      const price = isEmpty(_dish) ? '0' : _dish.price;

                      return (
                        <TableRow key={index} className={classes.tableRow}>
                          <TableCell
                            className={classNames(
                              classes.removeCell,
                              'icon-remove'
                            )}
                          >
                            <RemoveCircleOutline
                              className={classes.removeIcon}
                              onClick={onDeleteDish(index)}
                            />
                          </TableCell>
                          <TableCell numeric className={classes.w100}>
                            <TextField
                              select
                              fullWidth
                              error={Boolean(error.dishes)}
                              className={classes.selectItem}
                              value={
                                !isEmpty(dishes) && dish !== '0' ? dish : '0'
                              }
                              onChange={onSelectDish(index)}
                              InputProps={{
                                classes: { underline: classes.underlinePrimary }
                              }}
                              SelectProps={{
                                classes: {
                                  select: classes.selectInput
                                },
                                MenuProps: {
                                  className: classes.menu
                                }
                              }}
                              helperText={
                                !isEmpty(error.dishes) ? error.dishes : ''
                              }
                              FormHelperTextProps={{
                                className: classes.selectError
                              }}
                              margin="none"
                            >
                              {dishes.map(option => (
                                <MenuItem key={option._id} value={option._id}>
                                  {option.title}
                                </MenuItem>
                              ))}
                            </TextField>
                          </TableCell>
                          <TableCell className={classes.textCenter}>
                            <Typography variant="caption">
                              {formatter.format(price)}
                            </Typography>
                          </TableCell>
                          <TableCell className={classes.textCenter}>
                            <Grid
                              container
                              direction="row"
                              alignItems="center"
                              justify="center"
                              wrap="nowrap"
                            >
                              <Grid item>
                                <IconButton
                                  aria-label="Dec"
                                  className={classes.button}
                                  onClick={onChangeAmount(index, -1)}
                                >
                                  <LeftIcon />
                                </IconButton>
                              </Grid>
                              <Grid item>
                                <TextField
                                  fullWidth
                                  id="amount"
                                  value={amount}
                                  onChange={onChangeAmount(index)}
                                  type="number"
                                  step={1}
                                  InputProps={{
                                    classes: {
                                      underline: classes.underlinePrimary,
                                      input: classes.cellInput
                                    }
                                  }}
                                />
                              </Grid>
                              <Grid item>
                                <IconButton
                                  aria-label="Inc"
                                  className={classes.button}
                                  onClick={onChangeAmount(index, 1)}
                                >
                                  <RightIcon />
                                </IconButton>
                              </Grid>
                            </Grid>
                          </TableCell>
                          <TableCell numeric>
                            <Typography variant="title">
                              {formatter.format(+price * +amount)}
                            </Typography>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  <TableRow className={classes.tableRow}>
                    <TableCell
                      className={classNames(classes.addCell, 'icon-add')}
                    >
                      <AddCircle
                        className={classes.addIcon}
                        onClick={onAddDish}
                      />
                    </TableCell>
                    <TableCell />
                  </TableRow>
                </TableBody>
              </Table>
              <Grid
                container
                justify="flex-end"
                alignItems="baseline"
                style={{ marginBottom: '40px' }}
              >
                <Grid item className={classes.gridTotal}>
                  <Paper className={classes.paperTotal}>
                    <Grid container justify="flex-end" alignItems="baseline">
                      <Grid item>
                        <Typography variant="display1">ИТОГО:</Typography>
                      </Grid>
                      <Grid item>
                        <Typography
                          variant="display3"
                          className={classes.typoTotal}
                        >
                          {order.total
                            ? formatter.format(order.total)
                            : formatter.format(0)}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
            </div>
          </Hidden>

          <Hidden smUp>
            <div className={classes.tableWrap}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow className={classes.headRow}>
                    <TableCell
                      className={classes.textCenter}
                      padding="checkbox"
                    >
                      Блюда
                    </TableCell>
                  </TableRow>
                </TableHead>
              </Table>
              <Grid container direction="column" className={classes.gridCards}>
                {!isEmpty(dishes) &&
                  orderedDishes.map(({ dish, amount }, index) => {
                    const _dish = dishes.find(d => d._id === dish);
                    const price = isEmpty(_dish) ? '0' : _dish.price;
                    const weight = isEmpty(_dish) ? '' : _dish.weight + 'г.';

                    return (
                      <Grid item key={index} className={classes.gridCard}>
                        <Paper className={classes.paperCard}>
                          <TextField
                            select
                            fullWidth
                            error={Boolean(error.dishes)}
                            className={classes.selectItem}
                            value={
                              !isEmpty(dishes) && dish !== '0' ? dish : '0'
                            }
                            onChange={onSelectDish(index)}
                            InputProps={{
                              classes: {
                                underline: classes.underlineWhite
                              }
                            }}
                            SelectProps={{
                              classes: {
                                icon: classes.selectIconWhite,
                                select: classes.inputSelect
                              },
                              MenuProps: {
                                className: classes.menu
                              },
                              IconComponent: KeyboardArrowDown
                            }}
                            helperText={
                              !isEmpty(error.dishes) ? error.dishes : ''
                            }
                            FormHelperTextProps={{
                              className: classes.selectError
                            }}
                            margin="none"
                          >
                            {dishes.map(option => (
                              <MenuItem key={option._id} value={option._id}>
                                {option.title}
                              </MenuItem>
                            ))}
                          </TextField>
                          <Grid
                            container
                            direction="row"
                            wrap="nowrap"
                            justify="space-between"
                            alignItems="center"
                            className={classes.gridCardInfo}
                          >
                            <Grid
                              container
                              item
                              direction="row"
                              className={classNames(classes.gridType)}
                            >
                              <Grid item>
                                <Typography
                                  className={classes.typoCardTitle}
                                  variant="title"
                                >
                                  {formatter.format(price)}
                                </Typography>
                              </Grid>
                              <Grid item>
                                <Typography
                                  className={classes.typoSelectInputSubtitle}
                                  variant="caption"
                                >
                                  {weight}
                                </Typography>
                              </Grid>
                            </Grid>
                            <Grid
                              container
                              item
                              direction="row"
                              justify="flex-end"
                              alignItems="center"
                              wrap="nowrap"
                            >
                              <Button
                                variant="fab"
                                mini
                                aria-label="Remove"
                                className={classes.buttonCardSecondary}
                                onClick={onChangeAmount(index, -1)}
                              >
                                <RemoveIcon />
                              </Button>

                              <Typography
                                className={classes.typoCardAmount}
                                variant="title"
                              >
                                {amount}
                              </Typography>

                              <Button
                                variant="fab"
                                mini
                                aria-label="Add"
                                className={classes.buttonCardPrimary}
                                onClick={onChangeAmount(index, 1)}
                              >
                                <AddIcon />
                              </Button>
                            </Grid>
                          </Grid>

                          <Grid
                            container
                            direction="row"
                            wrap="nowrap"
                            justify="space-between"
                            alignItems="center"
                            className={classes.gridTotalValue}
                          >
                            <Grid item>
                              <RemoveCircleOutline
                                className={classes.removeIcon}
                                onClick={onDeleteDish(index)}
                              />
                            </Grid>
                            <Grid
                              container
                              item
                              justify="flex-end"
                              alignItems="center"
                            >
                              <Grid item>
                                <Typography variant="caption">ИТОГ:</Typography>
                              </Grid>
                              <Grid item>
                                <Typography
                                  variant="display1"
                                  className={classes.typoRowTotal}
                                >
                                  {formatter.format(+price * +amount)}
                                </Typography>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Paper>
                      </Grid>
                    );
                  })}
              </Grid>
              <Grid container justify="center">
                <Button
                  variant="fab"
                  aria-label="Add"
                  className={classes.buttonPrimary}
                  onClick={onAddDish}
                >
                  <AddIcon />
                </Button>
              </Grid>
              <Paper className={classes.paperCardTotal}>
                <Grid container justify="flex-end" alignItems="baseline">
                  <Grid item>
                    <Typography variant="display1">ИТОГО:</Typography>
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="display3"
                      className={classes.typoTotal}
                    >
                      {order.total
                        ? formatter.format(order.total)
                        : formatter.format(0)}
                    </Typography>
                  </Grid>
                </Grid>
              </Paper>
            </div>
          </Hidden>
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Button
              mini
              onClick={onDelete}
              variant="fab"
              aria-label="Delete"
              color="secondary"
              className={classes.margin}
            >
              <Icon size={18} icon={trashO} />
            </Button>
          </Grid>
          <Grid item>
            <Button
              to={'/orders'}
              component={Link}
              aria-label="Back"
              variant="contained"
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label="Save"
              variant="contained"
              className={classNames(classes.margin, classes.btnPrimary)}
              disabled={isEmpty(order.dishes)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}

OrderEdit.defaultProps = {
  order: {},
  orderedDishes: [],
  customers: [],
  dishes: []
};

export default OrderEdit;
