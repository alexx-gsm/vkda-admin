import blue from '@material-ui/core/colors/blue';
import grey from '@material-ui/core/colors/grey';
import red from '@material-ui/core/colors/red';
import teal from '@material-ui/core/colors/teal';

export default theme => ({
  // padding: { padding: '20px' },
  // paddingH: { padding: '10px 20px' },
  margin: {
    margin: theme.spacing.unit
  },
  textCenter: {
    textAlign: 'center'
  },
  // w100: { width: '100%' },
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  // gridTools: {
  //   [theme.breakpoints.up('sm')]: {
  //     padding: '0 20px'
  //   }
  // },
  gridBtnBack: {
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },

  buttonPrimary: {
    color: 'white',
    backgroundColor: blue[500],
    '&:hover': { backgroundColor: blue[700] }
  },
  // CARD HEADER
  cardHeader: {
    position: 'relative',
    margin: '0 20px',
    top: '-20px',
    padding: '5px 10px',
    background: `linear-gradient(60deg, ${blue[500]}, ${blue[700]})`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px 15px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0',
      borderRadius: 0
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end'
  },
  typoHeaderTitle: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase',
    textAlign: 'right',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.5rem'
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.25rem'
    }
  },
  // CARD BODY
  cardForm: {
    padding: '0 20px 20px',
    '& .DayPickerInput': { width: '100%' },
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  // card header
  card: {
    maxWidth: '760px',
    margin: '40px 0',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  btnPrimary: {
    color: theme.palette.common.white,
    backgroundColor: blue[600],
    '&:hover': {
      backgroundColor: blue[700]
    }
  },
  typoCardHeaderNumber: {
    color: theme.palette.common.white
  },
  typoHeaderTitle: {
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  inputTitle: {
    fontSize: '46px',
    color: '#666',
    paddingBottom: 0,
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  // ORDER TABLE
  tableRow: {
    cursor: 'pointer',
    transition: 'ease-in-out 0.3s',
    '& td': { background: red[50], transition: 'ease-in-out 0.3s' },
    '&.is-ready': {
      '& td': { background: teal[50], transition: 'ease-in-out 0.3s' }
    }
  },
  tableWrap: {
    margin: '0 -8px'
  },
  table: {
    marginTop: '40px',
    marginBottom: '40px'
  },
  headRow: {
    height: '28px',
    minHeight: '28px',
    '& th': {
      backgroundColor: blue[500],
      color: theme.palette.common.white,
      textTransform: 'uppercase'
    }
  },
  cellDishProcess: {
    width: '48px',
    padding: 0,
    '&:last-child': {
      padding: 0
    }
  }
});
