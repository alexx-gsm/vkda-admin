import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
// material-ui components
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  IconButton
} from '@material-ui/core';
// @material-ui/icons
import ArrowBack from '@material-ui/icons/ArrowBack';
// useful tools
import isEmpty from '../../../helpers/is-empty';
import moment from 'moment';
// react-icon-kits
import { Icon } from 'react-icons-kit';
import { ic_check_box_outline_blank } from 'react-icons-kit/md/ic_check_box_outline_blank';
import { ic_check_box } from 'react-icons-kit/md/ic_check_box';

const OrderProcessing = ({
  isReady,
  order,
  orderedDishes,
  dishes,
  customers,
  onClickTool,
  onSave,
  onSetReady,
  classes,
  history
}) => {
  const customer =
    !isEmpty(order) && !isEmpty(customers)
      ? customers.find(c => c._id === order.customer)
      : '';

  const getDishTitle = dishId => dishes.find(d => d._id === dishId).title;

  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridBtnBack}>
          <Button
            mini
            variant="fab"
            to={'/orders'}
            component={Link}
            aria-label="Back"
            className={classes.buttonPrimary}
          >
            <ArrowBack />
          </Button>
        </Grid>
      </Grid>

      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid
            container
            className={classes.gridCardHeader}
            alignItems="center"
            justify="space-between"
          >
            <Grid item xs={8} sm={6}>
              <Typography
                variant="title"
                className={classes.typoCardHeaderNumber}
              >
                {isEmpty(order.number) ? '' : order.number}
              </Typography>
            </Grid>
            <Grid item container xs={4} sm={6} justify="flex-end">
              <Grid item />
              <Grid item>
                <Typography
                  variant="display1"
                  className={classes.typoHeaderTitle}
                >
                  Заказ
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
        <form className={classes.cardForm} noValidate autoComplete="off">
          <TextField
            disabled
            fullWidth
            id="customer-select"
            label="Клиент"
            value={isEmpty(customer) ? '' : customer.name}
            margin="normal"
            className={classes.wrapTitle}
            InputLabelProps={{
              shrink: true,
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputTitle,
              classes: { underline: classes.underlinePrimary }
            }}
            SelectProps={{
              classes: {
                select: classes.selectInput
              }
            }}
          />

          <Grid container spacing={16}>
            <Grid item xs={12} sm={6}>
              <TextField
                disabled
                fullWidth
                id="number"
                label="Статус"
                value={isEmpty(order.status) ? 'draft' : order.status}
                className={classes.select}
                margin="normal"
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    input: classes.inputStatus,
                    underline: classes.underlinePrimary
                  }
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                disabled
                fullWidth
                id="number"
                label="Дата доставки"
                value={moment(order.deliveryDate).format('D MMMM YYYY')}
                className={classes.select}
                margin="normal"
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    input: classes.inputStatus,
                    underline: classes.underlinePrimary
                  }
                }}
              />
            </Grid>
          </Grid>

          <div className={classes.tableWrap}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow className={classes.headRow}>
                  <TableCell numeric padding="checkbox">
                    Блюда
                  </TableCell>
                  <TableCell
                    className={classes.textCenter}
                    style={{ whiteSpace: 'nowrap' }}
                    padding="checkbox"
                  >
                    Кол-во
                  </TableCell>
                  <TableCell numeric />
                </TableRow>
              </TableHead>
              <TableBody>
                {!isEmpty(dishes) &&
                  !isEmpty(orderedDishes) &&
                  orderedDishes.map(({ dish, amount, is_ready }, index) => {
                    return (
                      <TableRow
                        key={index}
                        className={classNames(
                          classes.tableRow,
                          is_ready ? 'is-ready' : ''
                        )}
                        onClick={() => onClickTool(index)}
                      >
                        <TableCell
                          numeric
                          className={classes.w100}
                          padding="checkbox"
                        >
                          <Typography variant="title">
                            {getDishTitle(dish)}
                          </Typography>
                        </TableCell>
                        <TableCell padding="checkbox">
                          <Typography
                            variant="title"
                            className={classes.textCenter}
                          >
                            {amount}
                          </Typography>
                        </TableCell>
                        <TableCell className={classes.cellDishProcess}>
                          <IconButton>
                            {is_ready ? (
                              <div
                                style={{
                                  width: 32,
                                  height: 32,
                                  color: '#1B5E20'
                                }}
                              >
                                <Icon size={'100%'} icon={ic_check_box} />
                              </div>
                            ) : (
                              <div
                                style={{
                                  width: 32,
                                  height: 32,
                                  color: '#BF360C'
                                }}
                              >
                                <Icon
                                  size={'100%'}
                                  icon={ic_check_box_outline_blank}
                                />
                              </div>
                            )}
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </div>
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Button
              onClick={() => history.push(`/orders/edit/${order._id}`)}
              aria-label="setReady"
              variant="contained"
              className={classNames(classes.margin, classes.btnPrimary)}
            >
              Изменить
            </Button>
          </Grid>
          <Grid item>
            <Button
              to={'/orders'}
              component={Link}
              aria-label="Back"
              variant="contained"
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label="Save"
              variant="contained"
              className={classNames(classes.margin, classes.btnPrimary)}
              disabled={isEmpty(order.dishes)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

OrderProcessing.defaultProps = {
  order: {}
};

export default OrderProcessing;
