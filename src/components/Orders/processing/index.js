import {
  compose,
  lifecycle,
  withState,
  withProps,
  withHandlers
} from 'recompose';
import { connect } from 'react-redux';
// AC
import {
  getOrderById,
  storeOrder,
  saveOrder,
  deleteOrder,
  setError
} from '../../../redux/modules/order';
import { getCustomers } from '../../../redux/modules/customer';
import { getDishes } from '../../../redux/modules/dish';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';
// Main Component
import OrderProcessing from './OrderProcessing';
// useful tools
import isEmpty from '../../../helpers/is-empty';

const checkReady = dishes => dishes.every(dish => dish.is_ready);

export default compose(
  connect(
    ({ orderStore, customerStore, dishStore }) => {
      const { order } = orderStore;
      const { customers } = customerStore;
      const { dishes } = dishStore;
      return {
        order,
        orderedDishes: !isEmpty(order) ? order.dishes : [],
        dishes,
        customers
      };
    },
    {
      getOrderById,
      storeOrder,
      saveOrder,
      getCustomers,
      getDishes
    }
  ),
  withState('isReady', 'setIsReady', false),
  withHandlers({
    onClickTool: ({
      order,
      orderedDishes,
      storeOrder,
      setIsReady
    }) => index => {
      const editedDish = orderedDishes.slice(index, index + 1)[0];
      const $orderedDishes = [
        ...orderedDishes.slice(0, index),
        {
          ...editedDish,
          is_ready: !editedDish.is_ready
        },
        ...orderedDishes.slice(index + 1)
      ];

      storeOrder({
        ...order,
        dishes: $orderedDishes
      });

      setIsReady(checkReady($orderedDishes));
    },
    onSave: ({ order, saveOrder, history }) => () => {
      const status = order.dishes.every(item => item.is_ready)
        ? 'isReady'
        : order.dishes.some(item => item.is_ready)
          ? 'onProcess'
          : 'new';
      saveOrder(
        {
          ...order,
          status
        },
        history
      );
    }
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params;
      this.props.getOrderById(id);
      this.props.getCustomers();
      this.props.getDishes();
    }
  }),

  withStyles(styles, { withTheme: true })
)(OrderProcessing);
