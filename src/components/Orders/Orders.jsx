import React from 'react';

// useful tools
import isEmpty from '../../helpers/is-empty';
import classNames from 'classnames';
// DayPicker
import DayPicker from './DayPicker/DayPicker.jsx';
// material-ui components
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';

import IconButton from '@material-ui/core/IconButton';

import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter,
  Hidden,
  FormControl,
  Input
} from '@material-ui/core';

import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
// icons
import { Icon } from 'react-icons-kit';
import { ic_face } from 'react-icons-kit/md/ic_face';
import { phone } from 'react-icons-kit/fa/phone';
import { mapMarker } from 'react-icons-kit/fa/mapMarker';
import { u1F345 } from 'react-icons-kit/noto_emoji_regular/u1F345';
import { u1F35C } from 'react-icons-kit/noto_emoji_regular/u1F35C';
import { u1F35A } from 'react-icons-kit/noto_emoji_regular/u1F35A';
import { u1F355 } from 'react-icons-kit/noto_emoji_regular/u1F355';
import { u1F37C } from 'react-icons-kit/noto_emoji_regular/u1F37C';
import { u1F375 } from 'react-icons-kit/noto_emoji_regular/u1F375';
import { u1F36D } from 'react-icons-kit/noto_emoji_regular/u1F36D';
import { u1F356 } from 'react-icons-kit/noto_emoji_regular/u1F356';

// moment
import moment from 'moment';
// common component
import ToolPanel from '../ToolPanel/ToolPanel.jsx';

const statisticIcons = {
  Салаты: <Icon size={'100%'} icon={u1F345} />,
  Первое: <Icon size={'100%'} icon={u1F35C} />,
  Второе: <Icon size={'100%'} icon={u1F356} />,
  Гарниры: <Icon size={'100%'} icon={u1F35A} />,
  Выпечка: <Icon size={'100%'} icon={u1F355} />,
  Соусы: <Icon size={'100%'} icon={u1F37C} />,
  Напитки: <Icon size={'100%'} icon={u1F375} />,
  Разное: <Icon size={'100%'} icon={u1F36D} />
};

const Orders = ({
  selectedDay,
  orders,
  customers,
  statuses,
  onDayChange,
  getStatusIcon,
  getCustomer,
  getStatus,
  getOrderStatistics,
  filter,
  setFilter,
  loading,
  onClick,
  onChange,
  onReload,
  classes,
  formatter
}) => {
  const getOrderStatisticsBody = order => {
    const statistics = getOrderStatistics(order);
    return Object.keys(statistics).map(s => {
      return (
        <Grid key={s} item className={classes.gridOrderIcon}>
          <Typography variant="caption">
            <Badge
              classes={{ badge: classes.badge }}
              badgeContent={statistics[s]}
              color="secondary"
            >
              <div style={{ width: 32, height: 32 }}>{statisticIcons[s]}</div>
            </Badge>
          </Typography>
        </Grid>
      );
    });
  };

  return (
    <div>
      <ToolPanel
        title="Заказы"
        addButtonLink="/orders/edit"
        loading={loading}
        onReload={onReload}
        color="blue"
        width="760px"
      />
      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid
            container
            justify="space-between"
            className={classes.gridHeader}
          >
            <Hidden xsDown>
              <Grid item className={classes.gridDays}>
                <IconButton
                  className={classes.button}
                  aria-label="filter-orders"
                  onClick={() => setFilter('none')}
                >
                  <Typography
                    variant="title"
                    className={classNames(
                      classes.buttonAll,
                      filter === 'none' ? 'is-active' : null
                    )}
                  >
                    Все
                  </Typography>
                </IconButton>
                {!isEmpty(statuses) &&
                  statuses.map(status => (
                    <IconButton
                      key={status.title}
                      className={classes.headerCardButton}
                      aria-label="filter-orders"
                      onClick={() => setFilter(status.title)}
                    >
                      <div
                        className={classNames(
                          classes.headerCardIcons,
                          filter === status.title ? 'is-active' : null
                        )}
                      >
                        {status.icon}
                      </div>
                    </IconButton>
                  ))}
              </Grid>
            </Hidden>
            <Hidden smUp>
              <TextField
                select
                id="filter-select"
                className={classes.selectFilterForm}
                value={filter}
                onChange={onChange}
                SelectProps={{
                  classes: {
                    icon: classes.selectIconWhite
                    // icon: classes.colorWhite
                  },
                  IconComponent: KeyboardArrowDown
                }}
                InputProps={{
                  classes: {
                    underline: classes.underlineWhite,
                    input: classes.selectHeaderText
                  }
                }}
                margin="none"
              >
                <MenuItem key={0} value={'none'}>
                  <Typography
                    variant="title"
                    className={classNames(
                      classes.buttonAll,
                      filter === 'none' ? 'is-active' : null
                    )}
                  >
                    Все
                  </Typography>
                </MenuItem>
                {!isEmpty(statuses) &&
                  statuses.map((status, i) => (
                    <MenuItem key={i + 1} value={status.title}>
                      <IconButton
                        key={status.title}
                        className={classes.headerCardMobileButton}
                        aria-label="filter-orders"
                        onClick={() => setFilter(status.title)}
                      >
                        <Grid
                          container
                          direction="row"
                          wrap="nowrap"
                          alignItems="center"
                        >
                          <div
                            className={classNames(
                              classes.headerCardMobileIcons,
                              filter === status.title ? 'is-active' : null
                            )}
                          >
                            {status.icon}
                          </div>
                          <Typography
                            variant="title"
                            className={classNames(
                              classes.buttonAll,
                              filter === status.title ? 'is-active' : null
                            )}
                          >
                            {status.alias}
                          </Typography>
                        </Grid>
                      </IconButton>
                    </MenuItem>
                  ))}
              </TextField>
            </Hidden>
            <Grid item>
              <FormControl margin="none" className="align-right">
                <DayPicker
                  value={selectedDay}
                  format="D"
                  onDayClick={onDayChange}
                  classes={classes}
                />
                <Typography className={classes.typoDeliveryDay}>
                  {moment(selectedDay)
                    .locale('ru')
                    .format('MMMM')}
                </Typography>
              </FormControl>
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paperTable}>
          <Grid container className={classes.gridTable}>
            <Table>
              <TableHead className={classes.tHead}>
                <TableRow>
                  <TableCell className={classes.cellIcon} padding="none" />
                  <TableCell className={classes.fullWidth} padding="checkbox">
                    <Typography variant="caption">Организация | ФИО</Typography>
                  </TableCell>

                  <TableCell
                    numeric
                    padding="checkbox"
                    className={classes.headerCellInfo}
                  >
                    Сумма
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {!isEmpty(orders) &&
                  !isEmpty(customers) &&
                  !isEmpty(statuses) &&
                  orders.map(order => {
                    const customer = getCustomer(order.customer);
                    const status = getStatus(order.status);

                    return (
                      <TableRow
                        key={order._id}
                        className={classNames(classes.tableRow, status.color)}
                        onClick={onClick(order)}
                      >
                        <TableCell
                          className={classes.cellIcon}
                          padding="checkbox"
                        >
                          <div className={classes.statusIcon}>
                            {getStatusIcon(order.status)}
                          </div>
                        </TableCell>
                        <TableCell
                          className={classes.cellCustomer}
                          padding="checkbox"
                        >
                          <Grid container className={classes.gridOrderInfo}>
                            <Grid item>
                              <Typography
                                variant="display1"
                                className={classes.customerName}
                              >
                                {customer.name}
                              </Typography>
                            </Grid>
                            {(filter === 'none' || filter === 'onDelivery') && (
                              <Grid item container className={classes.gridType}>
                                <div className={classes.personIcon}>
                                  <Icon icon={ic_face} />
                                </div>
                                <Typography
                                  variant="caption"
                                  className={classes.typoPerson}
                                >
                                  {customer.person}
                                </Typography>
                                <Grid
                                  container
                                  direction="row"
                                  wrap="nowrap"
                                  className={classes.gridMapInfo}
                                >
                                  <div className={classes.phoneIcon}>
                                    <Icon icon={phone} />
                                  </div>
                                  <Typography
                                    variant="caption"
                                    className={classes.typoPhone}
                                  >
                                    {customer.phone}
                                  </Typography>
                                </Grid>
                                <Grid
                                  container
                                  direction="row"
                                  wrap="nowrap"
                                  className={classes.gridMapInfo}
                                >
                                  <div className={classes.mapIcon}>
                                    <Icon icon={mapMarker} />
                                  </div>
                                  {isEmpty(customer.map) ? (
                                    <Typography variant="caption">
                                      {customer.address}
                                    </Typography>
                                  ) : (
                                    <a href={customer.map} target="_blank">
                                      {customer.address}
                                    </a>
                                  )}
                                </Grid>
                              </Grid>
                            )}
                          </Grid>
                          {(filter === 'new' ||
                            filter === 'onProcess' ||
                            filter === 'isReady') && (
                            <Grid
                              container
                              direction="row"
                              className={classNames(
                                classes.gridOrderIconStatistics,
                                filter
                              )}
                            >
                              {getOrderStatisticsBody(order)}
                            </Grid>
                          )}
                        </TableCell>
                        <TableCell
                          padding="checkbox"
                          className={classes.cellInfo}
                          numeric
                        >
                          <Typography
                            variant="title"
                            className={classes.typoTotal}
                          >
                            {order.total
                              ? formatter.format(order.total)
                              : formatter.format(0)}
                          </Typography>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
              <TableFooter>
                <TableRow />
              </TableFooter>
            </Table>
          </Grid>
        </Paper>
      </Paper>
    </div>
  );
};

Orders.defaultProps = {
  orders: []
};

export default Orders;
