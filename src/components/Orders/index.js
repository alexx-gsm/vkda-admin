import {
  compose,
  withState,
  withProps,
  withHandlers,
  lifecycle
} from 'recompose';
import { connect } from 'react-redux';
// AC
import {
  getOrders,
  getOrdersInInterval,
  storeOrder,
  storeSelectedDay,
  setOrderFilter,
  clearOrder
} from '../../redux/modules/order';
import { getCustomers } from '../../redux/modules/customer';
import { getDishes } from '../../redux/modules/dish';
import { getCategories } from '../../redux/modules/category';
// useful tools
import isEmpty from '../../helpers/is-empty';
import moment from 'moment';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';
import Orders from './Orders';

export default compose(
  connect(
    ({ orderStore, customerStore, dishStore, categoryStore }) => {
      const { selectedDay, orders, filter, statuses, loading } = orderStore;
      const { customers } = customerStore;
      const { dishes } = dishStore;
      const { categories } = categoryStore;
      const filteredOrders =
        filter === 'none'
          ? orders.sort((a, b) => {
              const status1 = statuses.find(s => s.title === a.status).sorting;
              const status2 = statuses.find(s => s.title === b.status).sorting;
              return Math.sign(status1 - status2);
            })
          : orders.filter(o => o.status === filter);

      return {
        $selectedDay: selectedDay,
        orders: filteredOrders,
        filter,
        statuses,
        customers,
        dishes,
        categories,
        loading
      };
    },
    {
      getOrders,
      getOrdersInInterval,
      storeOrder,
      storeSelectedDay,
      setOrderFilter,
      clearOrder,
      getCustomers,
      getDishes,
      getCategories
    }
  ),
  withState('selectedDay', 'setSelectedDay', ({ $selectedDay }) =>
    moment($selectedDay).toDate()
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0
    })
  }),
  withHandlers({
    onClick: ({ filter, history }) => ({ _id, status }) => event => {
      if (event.target.tagName !== 'A') {
        switch (filter) {
          case 'none':
            switch (status) {
              case 'draft':
                history.push(`/orders/edit/${_id}`);
                break;
              case 'new':
              case 'onProcess':
              case 'isReady':
                history.push(`/orders/processing/${_id}`);
                break;

              default:
                break;
            }
            break;
          case 'draft':
            history.push(`/orders/edit/${_id}`);
            break;
          case 'new':
          case 'onProcess':
          case 'isReady':
            history.push(`/orders/processing/${_id}`);
            break;

          default:
            break;
        }
        // history.push(`/orders/edit/${id}`);
      }
    },
    onReload: ({ selectedDay, getOrdersInInterval }) => () => {
      const day = moment(selectedDay).startOf('date');
      const nextDay = moment(day).add(1, 'd');

      getOrdersInInterval(day, nextDay);
    },
    getStatusIcon: ({ statuses }) => status =>
      statuses.find(s => s.title === status).icon,
    getCustomer: ({ customers }) => id => customers.find(c => c._id === id),
    getStatus: ({ statuses }) => id => statuses.find(s => s.title === id),
    setFilter: ({ setOrderFilter }) => filter => setOrderFilter(filter),
    onChange: ({ setOrderFilter }) => event => {
      setOrderFilter(event.target.value);
    },
    onDayChange: ({
      storeSelectedDay,
      getOrdersInInterval,
      setSelectedDay
    }) => date => {
      const day = moment(date).startOf('date');
      const nextDay = moment(day).add(1, 'd');

      getOrdersInInterval(day, nextDay);
      storeSelectedDay(date);
      setSelectedDay(date);
    },
    getOrderStatistics: ({ dishes, categories }) => order => {
      return order.dishes.reduce((acc, item) => {
        const $dish = dishes.find(d => d._id === item.dish);
        const $category = categories.find(c => c._id === $dish.category);

        return {
          ...acc,
          [$category.title]: acc[$category.title]
            ? +acc[$category.title] + +item.amount
            : +item.amount
        };
      }, {});
    }
  }),
  lifecycle({
    componentDidMount() {
      const day = moment(this.props.selectedDay).startOf('date');
      const nextDay = moment(day).add(1, 'd');

      this.props.getOrdersInInterval(day, nextDay);
      this.props.getCustomers();
      this.props.getDishes();
      this.props.getCategories();
      this.props.clearOrder();
    }
  }),
  withStyles(styles, { withTheme: true })
)(Orders);
