import React from 'react';
import { compose, withProps } from 'recompose';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate
} from 'react-day-picker/moment';
import 'moment/locale/ru';
// useful tools
import isEmpty from '../../helpers/is-empty';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';
import './style.css';

const withDayInput = ({ ...props }) => {
  return class extends React.Component {
    render() {
      return (
        <TextField
          id="delivery"
          margin={this.props.margin ? this.props.margin : 'normal'}
          fullWidth
          className={this.props.styles}
          {...this.props}
          {...props}
        >
          {props.value}
        </TextField>
      );
    }
  };
};

class InputDayPicker extends React.Component {
  render() {
    const {
      deliveryDate,
      format,
      value,
      label,
      onDayClick,
      margin,
      InputLabelProps,
      InputProps,
      styles
    } = this.props;
    console.log('---value', value);
    return (
      <DayPickerInput
        component={withDayInput({
          value,
          InputLabelProps,
          InputProps,
          label,
          margin,
          styles
        })}
        onDayChange={onDayClick}
        value={
          isEmpty(value)
            ? formatDate(Date.now(), format, 'ru')
            : moment(value).format(format)
        }
        formatDate={formatDate}
        parseDate={parseDate}
        format={format}
        dayPickerProps={{
          showWeekNumbers: true,
          showOutsideDays: true,
          firstDayOfWeek: 1,
          todayButton: 'Сегодня',
          locale: 'ru',
          localeUtils: MomentLocaleUtils,
          onTodayButtonClick: onDayClick
        }}
        keepFocus={false}
      />
    );
  }
}

export default InputDayPicker;
