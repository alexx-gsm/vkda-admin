import { compose, withHandlers } from 'recompose';

import InputDayPicker from './InputDayPicker';

export default compose(
  withHandlers({
    onDayClick: ({ onDayChange }) => date => onDayChange(date)
  })
)(InputDayPicker);
