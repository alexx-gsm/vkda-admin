import React from 'react';
import { Link } from 'react-router-dom';
// material-ui components
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core';
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add';
import Autorenew from '@material-ui/icons/Autorenew';

const Customers = ({ customers, onClick, onReload, loading, classes }) => {
  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridTools}>
          <Button
            mini
            to={'/customers/edit'}
            component={Link}
            variant="fab"
            aria-label="Add"
            className={classes.buttonAdd}
          >
            <AddIcon />
          </Button>
          <div className={classes.wrapper}>
            <IconButton
              onClick={onReload}
              className={classes.button}
              aria-label="Reload"
            >
              {loading ? '' : <Autorenew />}
            </IconButton>
            {loading && (
              <CircularProgress
                size={40}
                color="secondary"
                className={classes.fabProgress}
              />
            )}
          </div>
        </Grid>
      </Grid>

      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid container className={classes.gridHeader}>
            <Typography variant="display1" className={classes.typoHeaderTitle}>
              Клиенты
            </Typography>
          </Grid>
        </Paper>

        <Paper className={classes.paperTable}>
          <Grid container className={classes.gridTable}>
            <Table>
              <TableHead className={classes.tHead}>
                <TableRow>
                  <TableCell
                    className={classes.headCellData}
                    padding="checkbox"
                  >
                    <Typography variant="caption">Организация | ФИО</Typography>
                  </TableCell>

                  <TableCell
                    numeric
                    padding="checkbox"
                    className={classes.headerCellInfo}
                  >
                    Телефон
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {customers.map(customer => {
                  return (
                    <TableRow
                      key={customer._id}
                      className={classes.tableRow}
                      onClick={onClick(customer._id)}
                    >
                      <TableCell
                        className={classes.cellData}
                        padding="checkbox"
                      >
                        <Grid container className={classes.gridInfo}>
                          <Grid item container className={classes.gridType}>
                            <Typography variant="title">
                              {customer.name}
                            </Typography>
                            <Typography
                              className={classes.typoType}
                              variant="caption"
                            >
                              {customer.person}
                            </Typography>
                          </Grid>
                        </Grid>
                      </TableCell>
                      <TableCell
                        padding="checkbox"
                        className={classes.cellInfo}
                      >
                        <Typography variant="title" align="right">
                          {customer.phone}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
              <TableFooter>
                <TableRow />
              </TableFooter>
            </Table>
          </Grid>
        </Paper>
      </Paper>
    </div>
  );
};

Customers.defaultProps = {
  customers: []
};

export default Customers;
