import { connect } from 'react-redux';
// AC
import {
  getCustomerById,
  storeCustomer,
  saveCustomer
} from '../../../redux/modules/customer';
import { compose, lifecycle, withHandlers } from 'recompose';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';
// core components
import CustomerEdit from './CustomerEdit';
import isEmpty from '../../../helpers/is-empty';

export default compose(
  connect(
    ({ customerStore }) => {
      const { customer, error, loading } = customerStore;
      return { customer, error, loading };
    },
    { getCustomerById, storeCustomer, saveCustomer }
  ),
  withHandlers({
    onChange: ({ customer, storeCustomer }) => name => event => {
      storeCustomer({
        ...customer,
        [name]: event.target.value
      });
    },
    onSave: ({ customer, history, saveCustomer }) => () =>
      saveCustomer(customer, history)
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params;
      if (!isEmpty(id)) {
        this.props.getCustomerById(id);
      }
    }
  }),
  withStyles(styles, { withTheme: true })
)(CustomerEdit);
