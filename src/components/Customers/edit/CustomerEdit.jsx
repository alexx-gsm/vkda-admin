import React from 'react';
import { Link } from 'react-router-dom';
// useful utils
import isEmpty from '../../../helpers/is-empty';
import classNames from 'classnames';
// material-ui components
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import InputAdornment from '@material-ui/core/InputAdornment';
// @material-ui/icons
import ArrowBack from '@material-ui/icons/ArrowBack';
import FiberNew from '@material-ui/icons/FiberNew';
// icons
import { Icon } from 'react-icons-kit';
import { trashO } from 'react-icons-kit/fa/trashO';
import { mapMarker } from 'react-icons-kit/fa/mapMarker';
import { envelopeO } from 'react-icons-kit/fa/envelopeO';
import { mobile } from 'react-icons-kit/fa/mobile';
import { mapO } from 'react-icons-kit/fa/mapO';

const CustomerEdit = ({
  customer,
  classes,
  error,
  onChange,
  onSave,
  onDelete
}) => {
  const {
    _id,
    name,
    person,
    acronym,
    phone,
    email,
    address,
    map,
    note
  } = customer;
  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridBtnBack}>
          <Button
            mini
            variant="fab"
            to={'/customers'}
            component={Link}
            aria-label="Back"
            className={classes.buttonPrimary}
          >
            <ArrowBack />
          </Button>
        </Grid>
      </Grid>

      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant="display1" className={classes.typoHeaderTitle}>
              {isEmpty(_id) && (
                <Typography
                  variant="caption"
                  className={classes.iconHeaderTitleWrap}
                >
                  <FiberNew classes={{ root: classes.iconNewCustomer }} />
                </Typography>
              )}
              Клиент
            </Typography>
          </Grid>
        </Paper>

        <form className={classes.cardForm} noValidate autoComplete="off">
          <TextField
            fullWidth
            id="name"
            label="Организация"
            type="name"
            value={isEmpty(name) ? '' : name}
            onChange={onChange('name')}
            className={classes.select}
            margin="none"
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputTitle,
              classes: { underline: classes.underlinePrimary }
            }}
          />

          <Grid container spacing={16}>
            <Grid item xs={12} sm={10}>
              <TextField
                fullWidth
                id="person"
                label="ФИО"
                value={isEmpty(person) ? '' : person}
                onChange={onChange('person')}
                margin="normal"
                className={classes.wrapTitle}
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: { underline: classes.underlinePrimary }
                }}
                error={Boolean(error.person)}
                helperText={error.person ? error.person : null}
              />
            </Grid>
            <Grid item xs={12} sm={2}>
              <TextField
                fullWidth
                id="acronym"
                label="Акроним"
                value={isEmpty(acronym) ? '' : acronym}
                onChange={onChange('acronym')}
                className={classes.select}
                margin="normal"
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: { underline: classes.underlinePrimary }
                }}
              />
            </Grid>
          </Grid>

          <Paper className={classes.paperCustomerInfo}>
            <TextField
              fullWidth
              id="phone"
              label="Телефон"
              type="phone"
              value={isEmpty(phone) ? '' : phone}
              onChange={onChange('phone')}
              className={classes.select}
              margin="normal"
              InputLabelProps={{
                FormLabelClasses: {
                  root: classes.labelPrimary,
                  focused: classes.cssFocused
                }
              }}
              InputProps={{
                classes: { underline: classes.underlinePrimary },
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon icon={mobile} />
                  </InputAdornment>
                )
              }}
              error={Boolean(error.phone)}
              helperText={error.phone ? error.phone : null}
            />
            <TextField
              fullWidth
              id="email"
              label="Почта"
              type="email"
              value={isEmpty(email) ? '' : email}
              onChange={onChange('email')}
              className={classes.select}
              margin="normal"
              InputLabelProps={{
                FormLabelClasses: {
                  root: classes.labelPrimary,
                  focused: classes.cssFocused
                }
              }}
              InputProps={{
                classes: { underline: classes.underlinePrimary },
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon icon={envelopeO} />
                  </InputAdornment>
                )
              }}
            />
            <TextField
              fullWidth
              id="address"
              label="Адрес"
              type="address"
              value={isEmpty(address) ? '' : address}
              onChange={onChange('address')}
              className={classes.select}
              margin="normal"
              InputLabelProps={{
                FormLabelClasses: {
                  root: classes.labelPrimary,
                  focused: classes.cssFocused
                }
              }}
              InputProps={{
                classes: { underline: classes.underlinePrimary },
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon icon={mapMarker} />
                  </InputAdornment>
                )
              }}
              error={Boolean(error.address)}
              helperText={error.address ? error.address : null}
            />
            <TextField
              fullWidth
              id="map"
              label="Карта"
              type="map"
              value={isEmpty(map) ? '' : map}
              onChange={onChange('map')}
              className={classes.select}
              margin="normal"
              InputLabelProps={{
                FormLabelClasses: {
                  root: classes.labelPrimary,
                  focused: classes.cssFocused
                }
              }}
              InputProps={{
                classes: { underline: classes.underlinePrimary },
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon icon={mapO} />
                  </InputAdornment>
                )
              }}
              error={Boolean(error.map)}
              helperText={error.map ? error.map : null}
            />
          </Paper>

          <TextField
            fullWidth
            id="note"
            label="Заметки"
            multiline
            rowsMax="5"
            value={note}
            onChange={onChange('note')}
            margin="normal"
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              classes: { underline: classes.underlinePrimary }
            }}
          />
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Button
              mini
              onClick={onDelete}
              variant="fab"
              aria-label="Delete"
              color="secondary"
              className={classes.margin}
            >
              <Icon size={18} icon={trashO} />
            </Button>
          </Grid>
          <Grid item>
            <Button
              to={'/customers'}
              component={Link}
              aria-label="Back"
              variant="contained"
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label="Save"
              variant="contained"
              className={classNames(classes.margin, classes.btnPrimary)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

CustomerEdit.defaultProps = {
  customer: {},
  error: {}
};

export default CustomerEdit;
