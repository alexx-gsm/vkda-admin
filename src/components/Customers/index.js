import { compose, withHandlers, lifecycle } from 'recompose';
import { connect } from 'react-redux';
// AC
import { getCustomers } from '../../redux/modules/customer';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';
import Customers from './Customers';

export default compose(
  connect(
    ({ customerStore }) => {
      const { customers, loading } = customerStore;
      return {
        customers,
        loading
      };
    },
    { getCustomers }
  ),
  withHandlers({
    onClick: ({ history }) => id => () => history.push(`/customers/edit/${id}`),
    onReload: ({ getCustomers }) => () => getCustomers()
  }),
  lifecycle({
    componentDidMount() {
      this.props.getCustomers();
    }
  }),
  withStyles(styles, { withTheme: true })
)(Customers);
