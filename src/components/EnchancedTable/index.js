import { withStyles } from '@material-ui/core/styles';
import EnchancedTable from './EnchancedTable';
import styles from './styles';

export default withStyles(styles, { withTheme: true })(EnchancedTable);
