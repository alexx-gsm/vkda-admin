export default theme => ({
  root: {
    maxWidth: 980,
    marginTop: '25px',
    backgroundColor: theme.palette.background.paper,
    [theme.breakpoints.up('sm')]: {
      padding: '20px'
    },
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0
    }
  },
  table: {
    width: '100%',
    [theme.breakpoints.down('xs')]: {
      width: '100%'
    }
  },
  number: {
    textAlign: 'right'
  }
});
