import React from 'react';
// @material-ui components
import { withStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const EnchancedTable = ({
  classes,
  tableHead,
  tableBody,
  onRowClickHandler
}) => {
  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            {Object.keys(tableHead).map((key, i) => (
              <CustomTableCell key={i}>{tableHead[key]}</CustomTableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {tableBody.map((row, i) => (
            <TableRow key={i} onClick={() => onRowClickHandler(row._id)}>
              {Object.keys(tableHead).map((key, i) => (
                <CustomTableCell
                  key={i}
                  className={
                    typeof row[key] === 'number' ? classes.number : classes.text
                  }
                >
                  {typeof row[key] === 'number'
                    ? row[key].toFixed(2)
                    : row[key]}
                </CustomTableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default EnchancedTable;
