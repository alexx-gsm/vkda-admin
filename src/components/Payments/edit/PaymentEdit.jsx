import React from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
// DayPicker
import DayPickerInput from 'react-day-picker/DayPickerInput'
import 'react-day-picker/lib/style.css'
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment'
import 'moment/locale/ru'
// @material-ui components
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'
import {
  Typography,
  Hidden,
  Paper,
  Divider,
  Input,
  InputLabel,
  FormControl,
  InputAdornment,
} from '@material-ui/core'
// @material-ui/icons
import ArrowBack from '@material-ui/icons/ArrowBack'
import TransferIcon from '@material-ui/icons/TrendingFlat'
// useful utils
import isEmpty from '../../../helpers/is-empty'
import moment from 'moment'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
import { rub } from 'react-icons-kit/fa/rub'
import { creditCard } from 'react-icons-kit/fa/creditCard'
import { u1F426 } from 'react-icons-kit/noto_emoji_regular/u1F426'
import { u1F374 } from 'react-icons-kit/noto_emoji_regular/u1F374'
import { u1F4B0 } from 'react-icons-kit/noto_emoji_regular/u1F4B0'
import { u1F4B3 } from 'react-icons-kit/noto_emoji_regular/u1F4B3' // card

const PaymentEdit = ({
  classes,
  paymentStore,
  types,
  onChange,
  onSwitch,
  onDayChange,
  onDelete,
  onSave,
}) => {
  const {
    _id,
    title,
    date,
    cash,
    card,
    type,
    comment,
    variant = 'colibri',
    isTransfer = false,
    from = 'vkda',
    isFromCard = true,
    transferAmount = 0,
    to = 'colibri',
    isToCard = false,
  } = paymentStore.payment

  const { selectedDays, selectedDay } = paymentStore
  const isIncome =
    paymentStore.payment.isIncome === undefined
      ? false
      : paymentStore.payment.isIncome
  const { error = {} } = paymentStore

  const currentDate = date
    ? formatDate(date, 'LL', 'ru')
    : !isEmpty(selectedDays) && !isEmpty(selectedDay)
    ? moment(selectedDays[selectedDay]).format('LL')
    : formatDate(Date.now(), 'LL', 'ru')

  const dayPickerInput = () => (
    <DayPickerInput
      value={currentDate}
      formatDate={formatDate}
      parseDate={parseDate}
      format="LL"
      dayPickerProps={{
        showWeekNumbers: true,
        todayButton: 'Сегодня',
        locale: 'ru',
        localeUtils: MomentLocaleUtils,
      }}
      onDayChange={(date) => onDayChange(date)}
    />
  )

  const coloredUnderline = {
    underline: isIncome ? classes.underlinePrimary : classes.underlineSecondary,
  }

  const coloredLabel = {
    root: isIncome ? classes.labelPrimary : classes.labelSecondary,
    focused: classes.cssFocused,
  }

  const id = isEmpty(types)
    ? null
    : isIncome
    ? types.find((t) => t.title === 'Приход')._id
    : types.find((t) => t.title === 'Расход')._id

  const typeSelectOptions = id ? types.filter((t) => t.rootId === id) : []

  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item>
          <Button
            mini
            variant="fab"
            to={'/payments'}
            component={Link}
            aria-label="Add">
            <ArrowBack />
          </Button>
        </Grid>
        <Grid item>
          <Typography variant="display2">Денежка</Typography>
        </Grid>
      </Grid>

      <Hidden xsDown>
        <Divider />
      </Hidden>

      <Paper className={classes.cardHeader}>
        <Typography
          variant="display2"
          align="center"
          className={classNames(
            classes.cardTitle,
            isIncome ? classes.primaryTitle : classes.secondaryTitle
          )}>
          {isTransfer ? 'Новое' : isEmpty(_id) ? 'Новый' : 'Редактируем'}{' '}
          {isTransfer ? 'Перемещение' : isIncome ? 'Приход' : 'Расход'}
        </Typography>

        <form className={classes.cardForm} noValidate autoComplete="off">
          <Grid
            container
            spacing={16}
            alignItems="center"
            style={{ position: 'relative', zIndex: 2 }}>
            <Grid item xs={4} sm>
              {!isTransfer && (
                <Grid container alignItems="center">
                  <Grid item>
                    <Switch
                      checked={variant === 'vkda'}
                      onChange={onChange('variant')}
                      value={variant === 'vkda' ? 'colibri' : 'vkda'}
                      classes={{
                        switchBase: classes.colorSwitchBase,
                        checked: classes.colorChecked,
                        bar: classes.colorBar,
                      }}
                    />
                  </Grid>
                  <Grid item>
                    {variant === 'vkda' ? (
                      <div
                        style={{
                          color: '#00acc1',
                        }}>
                        <Icon size={32} icon={u1F374} />
                      </div>
                    ) : (
                      <div
                        style={{
                          color: '#f44336',
                        }}>
                        <Icon size={32} icon={u1F426} />
                      </div>
                    )}
                  </Grid>
                </Grid>
              )}
            </Grid>
            <Grid item xs={8} sm>
              <FormControl fullWidth>
                <InputLabel htmlFor="date" shrink={true}>
                  Дата
                </InputLabel>
                <Input
                  id="date"
                  inputComponent={dayPickerInput}
                  className={classes.inputDatePicker}
                />
              </FormControl>
            </Grid>
          </Grid>

          {!isTransfer && (
            <Grid container spacing={16} alignItems="center">
              <Grid item xs={4} sm>
                <FormGroup row className={classes.formGroupIncome}>
                  <FormControlLabel
                    style={{ marginLeft: 0 }}
                    control={
                      <Switch
                        checked={isIncome}
                        onChange={onSwitch('isIncome')}
                        classes={{
                          switchBase: classes.colorSwitchBase,
                          checked: classes.colorChecked,
                          bar: classes.colorBar,
                        }}
                      />
                    }
                    label={isIncome ? 'Приход' : 'Расход'}
                  />
                </FormGroup>
              </Grid>
              <Grid item xs={8} sm>
                <TextField
                  select
                  fullWidth
                  error={Boolean(error.type)}
                  id="type-select"
                  label="Тип"
                  className={classes.select}
                  value={isEmpty(type) ? '' : type}
                  onChange={onChange('type')}
                  InputLabelProps={{
                    shrink: true,
                    FormLabelClasses: coloredLabel,
                  }}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
                  InputProps={{
                    classes: coloredUnderline,
                  }}
                  helperText={error.type ? error.type : ''}
                  margin="dense">
                  {typeSelectOptions.map((option) => (
                    <MenuItem key={option._id} value={option._id}>
                      {option.title}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
            </Grid>
          )}

          {isTransfer && (
            <Grid
              container
              spacing={16}
              alignItems="center"
              justify="space-between"
              className={classes.GridTransfer}>
              <Grid item xs={4}>
                <Typography variant="caption" align="center">
                  Откуда
                </Typography>
                <Grid container alignItems="center" justify="center">
                  <Grid item>
                    <Switch
                      checked={from === 'vkda'}
                      onChange={onChange('from')}
                      value={from === 'vkda' ? 'colibri' : 'vkda'}
                      classes={{
                        switchBase: classes.colorSwitchBase,
                        checked: classes.colorChecked,
                        bar: classes.colorBar,
                      }}
                    />
                  </Grid>
                  <Grid item>
                    {from === 'vkda' ? (
                      <div
                        style={{
                          color: '#00acc1',
                        }}>
                        <Icon size={32} icon={u1F374} />
                      </div>
                    ) : (
                      <div
                        style={{
                          color: '#f44336',
                        }}>
                        <Icon size={32} icon={u1F426} />
                      </div>
                    )}
                  </Grid>
                </Grid>

                <Grid container alignItems="center" justify="center">
                  <Grid item>
                    <Switch
                      checked={isFromCard}
                      onChange={onSwitch('isFromCard')}
                      color="primary"
                    />
                  </Grid>
                  <Grid item>
                    {isFromCard ? (
                      <div
                        style={{
                          color: '#303f9f',
                        }}>
                        <Icon size={32} icon={u1F4B3} />
                      </div>
                    ) : (
                      <div
                        style={{
                          color: '#21790e',
                        }}>
                        <Icon size={32} icon={u1F4B0} />
                      </div>
                    )}
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={4}>
                <Paper square className={classes.GridTransferColumn}>
                  <Grid container direction="column" alignItems="center">
                    <Grid item xs={8}>
                      <TextField
                        id="transferAmount"
                        label="Сумма"
                        type="number"
                        value={isEmpty(transferAmount) ? '' : transferAmount}
                        onChange={onChange('transferAmount')}
                        className={classes.textField}
                        margin="dense"
                        inputProps={{
                          min: 0,
                          step: 1,
                        }}
                        InputLabelProps={{
                          FormLabelClasses: coloredLabel,
                        }}
                        InputProps={{
                          classes: coloredUnderline,
                          endAdornment: (
                            <InputAdornment position="end">
                              <Icon icon={rub} />
                            </InputAdornment>
                          ),
                        }}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={8}></Grid>
                  </Grid>
                </Paper>
              </Grid>

              <Grid item xs={4}>
                <Typography variant="caption" align="center">
                  Куда
                </Typography>

                <Grid container alignItems="center" justify="center">
                  <Grid item>
                    <Switch
                      checked={to === 'vkda'}
                      onChange={onChange('to')}
                      value={to === 'vkda' ? 'colibri' : 'vkda'}
                      classes={{
                        switchBase: classes.colorSwitchBase,
                        checked: classes.colorChecked,
                        bar: classes.colorBar,
                      }}
                    />
                  </Grid>
                  <Grid item>
                    {to === 'vkda' ? (
                      <div
                        style={{
                          color: '#00acc1',
                        }}>
                        <Icon size={32} icon={u1F374} />
                      </div>
                    ) : (
                      <div
                        style={{
                          color: '#f44336',
                        }}>
                        <Icon size={32} icon={u1F426} />
                      </div>
                    )}
                  </Grid>
                </Grid>

                <Grid container alignItems="center" justify="center">
                  <Grid item>
                    <Switch
                      checked={isToCard}
                      onChange={onSwitch('isToCard')}
                      color="primary"
                    />
                  </Grid>
                  <Grid item>
                    {isToCard ? (
                      <div
                        style={{
                          color: '#303f9f',
                        }}>
                        <Icon size={32} icon={u1F4B3} />
                      </div>
                    ) : (
                      <div
                        style={{
                          color: '#21790e',
                        }}>
                        <Icon size={32} icon={u1F4B0} />
                      </div>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          )}

          {!isTransfer && (
            <Paper className={classes.paperInputs} square>
              <Grid container spacing={16}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id="cash"
                    label="Наличка"
                    type="number"
                    value={isEmpty(cash) ? '' : cash}
                    onChange={onChange('cash')}
                    className={classes.textField}
                    margin="none"
                    inputProps={{
                      min: 0,
                      step: 1,
                    }}
                    InputLabelProps={{
                      FormLabelClasses: coloredLabel,
                    }}
                    InputProps={{
                      classes: coloredUnderline,
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon icon={rub} />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id="card"
                    label="Карта"
                    value={isEmpty(card) ? '' : card}
                    type="number"
                    onChange={onChange('card')}
                    className={classes.textField}
                    margin="none"
                    inputProps={{
                      min: 0,
                      step: 1,
                    }}
                    InputLabelProps={{
                      FormLabelClasses: coloredLabel,
                    }}
                    InputProps={{
                      classes: coloredUnderline,
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon icon={creditCard} />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
              </Grid>
            </Paper>
          )}
          <TextField
            fullWidth
            id="title"
            label="Название"
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin="dense"
            InputLabelProps={{
              FormLabelClasses: coloredLabel,
            }}
            InputProps={{
              classes: coloredUnderline,
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />
          <TextField
            fullWidth
            id="multiline-flexible"
            label="Комментарий"
            multiline
            rowsMax="5"
            value={!isEmpty(comment) ? comment : ''}
            onChange={onChange('comment')}
            className={classes.textField}
            margin="dense"
            InputLabelProps={{
              FormLabelClasses: coloredLabel,
            }}
            InputProps={{
              classes: coloredUnderline,
            }}
          />

          <Grid
            container
            alignItems="center"
            className={classes.GridSwitchTransfer}>
            <Grid item xs={4} sm>
              <FormGroup row className={classes.formGroupIncome}>
                <FormControlLabel
                  style={{ marginLeft: 0 }}
                  control={
                    <Switch
                      checked={isTransfer}
                      onChange={onSwitch('isTransfer')}
                      // classes={{
                      //   switchBase: classes.colorSwitchBase,
                      //   checked: classes.colorChecked,
                      //   bar: classes.colorBar,
                      // }}
                    />
                  }
                  label="Перемещение"
                />
              </FormGroup>
            </Grid>
          </Grid>
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Button
              mini
              onClick={onDelete(_id)}
              variant="fab"
              aria-label="Delete"
              color="secondary"
              className={classes.margin}>
              <Icon size={18} icon={trashO} />
            </Button>
          </Grid>
          <Grid item>
            <Button
              to={'/payments'}
              component={Link}
              aria-label="Add"
              variant="contained">
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label="Add"
              variant="contained"
              className={classNames(
                classes.margin,
                isIncome ? classes.btnPrimary : classes.btnSecondary
              )}>
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}

PaymentEdit.defaultProps = {
  paymentStore: {},
  types: {},
  error: {},
}

export default PaymentEdit
