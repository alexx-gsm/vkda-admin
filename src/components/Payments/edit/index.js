import { connect } from 'react-redux'
import {
  compose,
  lifecycle,
  withState,
  withHandlers,
  withProps,
} from 'recompose'
// useful tools
import isEmpty from '../../../helpers/is-empty'
import moment from 'moment'
// AC
import {
  getPaymentById,
  addPayment,
  savePayment,
  deletePayment,
  storePayment,
} from '../../../redux/modules/payment'
import { getCategories } from '../../../redux/modules/category'
// @material-ui
import { withStyles } from '@material-ui/core/styles'
// styles
import styles from './styles'
// core components
import withDialog from '../../Dialog'
import PaymentEdit from './PaymentEdit'

export default compose(
  connect(
    ({ payments, categoryStore }) => ({
      paymentStore: payments,
      types: categoryStore.categories,
    }),
    {
      getPaymentById,
      addPayment,
      savePayment,
      storePayment,
      onConfirm: deletePayment,
      getCategories,
    }
  ),
  withState('isVisible', 'setVisible', false),
  withState('deletingItemId', 'setDeletingProductId', ''),
  withProps({
    modalTitle: 'Удаление документа',
    modalText: 'Вы уверены, что хотите удалить этот документ?',
  }),
  withHandlers({
    onSwitch: ({ paymentStore, storePayment }) => (name) => (event) => {
      const { payment } = paymentStore
      storePayment({
        ...payment,
        [name]: event.target.checked,
      })
    },
    onChange: ({ paymentStore, storePayment }) => (name) => (event) => {
      const { payment } = paymentStore

      if (name === 'card' || name === 'cash') {
        storePayment({
          ...payment,
          [name]: +event.target.value,
        })
      } else {
        storePayment({
          ...payment,
          [name]: event.target.value,
        })
      }
    },
    onDayChange: ({ paymentStore, storePayment }) => (date) => {
      const { payment } = paymentStore

      storePayment({
        ...payment,
        date,
      })
    },
    onSave: ({ paymentStore, history, addPayment, savePayment }) => () => {
      const { payment } = paymentStore
      const { selectedDays, selectedDay } = paymentStore
      const date =
        payment.date !== undefined
          ? payment.date
          : !isEmpty(selectedDays) && !isEmpty(selectedDay)
          ? moment(selectedDays[selectedDay])
          : Date.now()
      const week = moment(date).isoWeek()
      const isIncome =
        payment.isIncome === undefined && isEmpty(payment.isIncome)
          ? false
          : payment.isIncome

      if (isEmpty(payment._id)) {
        addPayment({ ...payment, isIncome, date, week }, history)
      } else {
        savePayment({ ...payment, isIncome, date, week }, history)
      }
    },
    onDelete: ({ setDeletingProductId, setVisible }) => (id) => () => {
      setDeletingProductId(id)
      setVisible(true)
    },
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getPaymentById(id)
      }
      this.props.getCategories()
    },
  }),
  withDialog,
  withStyles(styles, { withTheme: true })
)(PaymentEdit)
