import cyan from '@material-ui/core/colors/cyan'
import grey from '@material-ui/core/colors/grey'
import red from '@material-ui/core/colors/red'

export default (theme) => ({
  padding: { padding: '20px' },
  margin: {
    margin: theme.spacing.unit,
  },
  labelPrimary: {
    '&$cssFocused': {
      color: cyan[700],
    },
  },
  labelSecondary: {
    '&$cssFocused': {
      color: red[500],
    },
  },
  cssFocused: {},
  underlinePrimary: {
    '&:after': {
      borderBottomColor: cyan[700],
    },
  },
  underlineSecondary: {
    '&:after': {
      borderBottomColor: red[500],
    },
  },
  btnPrimary: {
    color: theme.palette.common.white,
    backgroundColor: cyan[600],
    '&:hover': {
      backgroundColor: cyan[700],
    },
  },
  btnSecondary: {
    color: theme.palette.common.white,
    backgroundColor: red[500],
    '&:hover': {
      backgroundColor: red[700],
    },
  },

  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px',
    },
  },
  cardHeader: {
    marginTop: '20px',

    [theme.breakpoints.up('sm')]: {
      width: '580px',
    },
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      marginTop: '10px',
    },
  },
  cardTitle: {
    color: 'white',
    backgroundColor: grey[500],
    fontSize: '26px',
    textTransform: ' uppercase',
    padding: '5px 0',
    borderRadius: '4px 4px 0 0',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
    },
  },
  primaryTitle: {
    backgroundColor: cyan[600],
  },
  secondaryTitle: {
    backgroundColor: red[500],
  },
  wrapTitle: {
    marginBottom: '30px',
  },
  inputTitle: {
    fontSize: '46px',
    color: '#333',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28,
    },
  },
  colorSwitchBase: {
    color: red[500],
    '& + $colorBar': {
      backgroundColor: red[700],
    },
    '&$colorChecked': {
      color: cyan[500],
      '& + $colorBar': {
        backgroundColor: cyan[700],
      },
    },
  },
  colorBar: {},
  colorChecked: {},

  cardForm: {
    padding: theme.spacing.unit * 3,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px',
    },
  },
  inputDatePicker: {
    width: '100%',
    display: 'flex',
    '& .DayPickerInput': { flex: 1 },
    '& input': {
      fontSize: '1.2rem',
      padding: '5px 0',
      border: 'none',
      width: '100%',
      '&:focus': { outline: 'none' },
    },
  },
  GridSwitch: {
    marginBottom: theme.spacing.unit * 3,
  },
  paperInputs: {
    padding: '20px',
    backgroundColor: grey[200],
    border: '1px solid',
    borderColor: grey[300],
    marginTop: theme.spacing.unit * 3,
  },
  cardFooter: {
    padding: '10px',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  GridWrapperTransfer: {
    marginBottom: theme.spacing.unit * 3,
  },
  GridTransfer: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
  },
  GridTransferColumn: {
    backgroundColor: grey[200],
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 3,
  },
  GridSwitchTransfer: {
    marginTop: theme.spacing.unit * 3,
  },
})
