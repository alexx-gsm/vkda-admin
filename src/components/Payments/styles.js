import cyan from '@material-ui/core/colors/cyan'
import grey from '@material-ui/core/colors/grey'
import amber from '@material-ui/core/colors/amber'
import red from '@material-ui/core/colors/red'
import green from '@material-ui/core/colors/green'
import indigo from '@material-ui/core/colors/indigo'
import orange from '@material-ui/core/colors/orange'

export default (theme) => ({
  dflex: { display: 'flex' },
  padding: { padding: '15px' },
  paddingL: { paddingLeft: '15px' },
  wrapTools: {
    padding: '0 20px',
    [theme.breakpoints.down('xs')]: {
      padding: 0,
    },
  },
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    alignItems: 'center',
    maxWidth: '960px',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px',
      boxSizing: 'border-box',
    },
  },
  buttonAdd: {
    color: 'white',
    backgroundColor: cyan[700],
    '&:hover': { backgroundColor: cyan[800] },
  },
  divider: {
    padding: '0 20px',
    maxWidth: '960px',
  },
  content: {
    marginTop: '20px',
    [theme.breakpoints.up('sm')]: {
      width: '580px',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      marginTop: '10px',
    },
  },
  card: {
    maxWidth: '960px',
    margin: '40px 0 0',
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0',
    },
  },
  cardHeader: {
    position: 'relative',
    margin: '0',
    top: '-20px',
    padding: '5px 10px',
    background: `linear-gradient(60deg, ${cyan[800]}, ${cyan[700]})`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px',
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px',
    },
  },
  tHead: {
    '& th, & th span': {
      color: cyan[600],
    },
  },
  gridHeader: {
    justifyContent: 'space-between',
  },
  gridDays: {
    display: 'flex',
    alignItems: 'flex-start',
    '&:hover': {},
  },
  gridDay: {
    cursor: 'pointer',
    position: 'relative',
    padding: '3px 8px 0',
    borderRight: '1px solid rgba(255,255,255,.4)',
    minWidth: '70px',
    '&:last-child': {
      borderRight: 'none',
    },
    '&.active *, &:hover *': {
      textShadow: '1px 1px 2px rgba(0,0,0,.4)',
      color: theme.palette.common.white,
    },
    '&.active': {
      borderBottom: '1px solid white',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '3px 5px 0',
      minWidth: '65px',
    },
  },
  typoWeek: {
    fontSize: '16px',
    color: 'rgba(255,255,255,.6)',
    textTransform: 'uppercase',
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      fontSize: '14px',
    },
  },
  typoDay: {
    fontSize: '14px',
    color: 'rgba(255,255,255,.6)',
    textAlign: 'center',
  },
  gridTable: {
    [theme.breakpoints.down('xs')]: {
      padding: '0',
    },
  },
  // inner elements
  headerPicker: {
    '& input': {
      fontSize: '40px',
      fontWeight: 'bold',
      textAlign: 'center',
      width: '50px',
      background: 'transparent',
      color: theme.palette.common.white,
      outline: 'none',
      border: 'none',
      borderBottom: '1px solid white',
      cursor: 'pointer',
      '&:hover': {
        textShadow: '1px 1px 2px rgba(0,0,0,.4)',
      },
    },
  },
  typoHeaderPicker: {
    color: 'white',
  },
  typoCountPayments: {
    position: 'absolute',
    top: '-4px',
    right: 0,
    color: 'black',
    lineHeight: '1',
    padding: '3px 5px 1px',
    borderRadius: '50%',
    boxShadow: '1px 1px 2px rgba(0,0,0,.42)',
    background: amber[500],
  },
  overlayWrapper: {
    position: 'relative',

    [theme.breakpoints.down('xs')]: {
      position: 'fixed',
      top: '60px',
      left: 0,
      right: 0,
      bottom: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  overlay: {
    position: 'absolute',
    zIndex: 1,
    background: theme.palette.common.white,
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.15)',
    right: 0,
    left: 'auto',
    paddingTop: '20px',
    [theme.breakpoints.down('xs')]: {
      position: 'static',
    },
  },
  headCellData: {
    '&, & span': {
      width: '100px',
      [theme.breakpoints.down('xs')]: {
        width: '50px',
      },
    },
  },
  headCellInfo: {
    width: '100%',
  },
  cellData: { width: '100px', padding: '5px', background: grey[100] },
  gridData: {
    width: '100px',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'baseline',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      alignItems: 'center',
      width: '50px',
    },
  },
  gridValue: {
    display: 'flex',
    flexWrap: 'nowrap',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  cellStatic: {
    background: grey[100],
    textAlign: 'center',
  },
  cellTotalValue: {
    background: grey[100],
  },
  gridTotalValue: {
    display: 'flex',
    flexWrap: 'nowrap',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: '20px 15px',
    borderTop: '2px solid',
  },
  gridMiddleTotalValue: {
    display: 'flex',
    paddingRight: '15px',
    borderRight: '1px solid',
  },
  typoTableMonth: {
    padding: '0 2px',
  },
  gridType: {
    alignItems: 'center',
    flexDirection: 'row',
    [theme.breakpoints.down('xs')]: {
      alignItems: 'flex-start',
      marginLeft: 0,
      flexDirection: 'column',
    },
  },
  typoType: {
    marginLeft: '10px',
    paddingLeft: '10px',
    borderLeft: '1px solid',
    [theme.breakpoints.down('xs')]: {
      marginLeft: 0,
    },
  },
  typoRub: { paddingLeft: '5px' },
  colorWhite: {
    color: theme.palette.common.white,
  },
  selectHeaderText: {
    color: theme.palette.common.white,
  },
  underlineWhite: {
    '&:before, &:after': {
      borderBottomColor: theme.palette.common.white,
    },
  },
  rowTransfer: {
    background: indigo[50],
    '& $typoValue, & $typoRub': {
      color: indigo[900],
    },
  },
  rowOutcome: {
    background: red[50],
    '& $typoValue, & $typoRub': {
      color: red[900],
    },
  },
  rowIncome: {
    background: green[50],
    '& $typoValue, & $typoRub': {
      color: green[900],
    },
  },
  typoValue: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '18px',
    },
  },
  typoMiddleValue: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '0.75rem',
    },
  },
  typoTotalValue: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '20px',
    },
  },
  gridStatic: {
    marginTop: '20px',
    flexDirection: 'column',
    [theme.breakpoints.down('xs')]: {
      // alignItems: 'center'
    },
  },
  gridStaticInfo: {
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      alignItems: 'center',
    },
  },
  paperStatic: {
    marginTop: '5px',
    padding: '20px',
    color: theme.palette.common.white,
    [theme.breakpoints.down('xs')]: {
      padding: '10px',
      borderRadius: 0,
    },
    '&.indigo': {
      background: indigo[50],
    },
    '&.orange': {
      background: orange[50],
    },
    '&.cyan': {
      background: cyan[50],
    },
  },
  typoStaticTitle: {
    [theme.breakpoints.down('xs')]: {
      padding: '0 10px',
    },
  },
  wrapper: {
    display: 'inline-block',
    margin: theme.spacing.unit,
    position: 'relative',
  },
  buttonReload: {},
  fabProgress: {
    position: 'absolute',
    top: 4,
    left: 4,
    zIndex: 1,
  },
  CellIcon: {
    // width: '20px',
    paddingRight: theme.spacing.unit,
    background: grey[100],
  },
})
