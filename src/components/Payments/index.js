// import React from 'react';
import {
  compose,
  lifecycle,
  withState,
  withHandlers,
  withProps,
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getPayments,
  setSelectedDay,
  clearPayment,
} from '../../redux/modules/payment'
import { getCategories } from '../../redux/modules/category'
// useful tools
import moment from 'moment'
// @material-ui
import { withStyles } from '@material-ui/core/styles'
// styles
import styles from './styles'
// components
import Payments from './Payments'

export default compose(
  connect(
    ({ payments, categoryStore }) => {
      const { selectedDay, selectedDays } = payments
      const filteredDay = selectedDays[selectedDay]
      const filteredPayments = payments.payments.filter((payment) =>
        moment(payment.date).isSame(filteredDay, 'day')
      )

      return {
        paymentStore: payments,
        week: payments.week,
        filteredPayments,
        incomeAmount: filteredPayments
          .filter((p) => !p.isTransfer && p.isIncome)
          .reduce((temp, p) => {
            return temp + +p.cash + +p.card
          }, 0),
        outcomeAmount: filteredPayments
          .filter((p) => !p.isTransfer && !p.isIncome)
          .reduce((temp, p) => {
            return temp + +p.cash + +p.card
          }, 0),
        types: categoryStore.categories,
      }
    },
    { getPayments, setSelectedDay, clearPayment, getCategories }
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withState('isStatic', 'setStatic', false),
  withState('staticData', 'setStaticData', []),
  withHandlers({
    onClick: (props) => (id) => () => {
      props.history.push(`/payments/edit/${id}`)
    },
    onChange: ({ setSelectedDay, setStatic }) => (name) => (event) => {
      if (name === 'select-day') {
        const index = +event.target.value
        setSelectedDay(index)
        setStatic(false)
      }
    },
    onSelectDay: ({ setSelectedDay, setStatic }) => (i) => () => {
      setSelectedDay(i)
      setStatic(false)
    },
    countPayments: ({ paymentStore }) => (i) => {
      const { payments, selectedDays } = paymentStore
      return payments.filter((payment) =>
        moment(payment.date).isSame(selectedDays[i], 'day')
      ).length
    },
    onClickStatic: ({
      paymentStore,
      types,
      setStaticData,
      isStatic,
      setStatic,
    }) => () => {
      if (isStatic) return setStatic(false)

      const { selectedDays, payments } = paymentStore
      const getPaymentTypeTitle = (_id) =>
        types.find((t) => t._id === _id).title

      const data = selectedDays.reduce(
        (temp, selectedDay) => {
          const filteredByDayPayments = payments.filter((payment) =>
            moment(payment.date).isSame(selectedDay, 'day')
          )
          const dailyAmount = filteredByDayPayments.reduce(
            (amount, payment) => {
              return payment.isIncome
                ? {
                    ...amount,
                    income: amount.income + +payment.cash + +payment.card,
                    total: amount.total + +payment.cash + +payment.card,
                    delivery:
                      getPaymentTypeTitle(payment.type) === 'Доставка'
                        ? {
                            cash: amount.delivery.cash + payment.cash,
                            card: amount.delivery.card + payment.card,
                          }
                        : {
                            cash: amount.delivery.cash,
                            card: amount.delivery.card,
                          },
                    canteen:
                      getPaymentTypeTitle(payment.type) === 'Столовая'
                        ? {
                            cash: amount.canteen.cash + payment.cash,
                            card: amount.canteen.card + payment.card,
                          }
                        : {
                            cash: amount.canteen.cash,
                            card: amount.canteen.card,
                          },
                  }
                : {
                    ...amount,
                    outcome: amount.outcome - +payment.cash - +payment.card,
                    total: amount.total - +payment.cash - +payment.card,
                  }
            },
            {
              income: 0,
              outcome: 0,
              total: 0,
              delivery: { cash: 0, card: 0 },
              canteen: { cash: 0, card: 0 },
            }
          )

          return {
            income: [...temp.income, dailyAmount.income],
            outcome: [...temp.outcome, dailyAmount.outcome],
            total: [...temp.total, dailyAmount.total],
            delivery: {
              cash: [...temp.delivery.cash, dailyAmount.delivery.cash],
              card: [...temp.delivery.card, dailyAmount.delivery.card],
            },
            canteen: {
              cash: [...temp.canteen.cash, dailyAmount.canteen.cash],
              card: [...temp.canteen.card, dailyAmount.canteen.card],
            },
          }
        },
        {
          income: [],
          outcome: [],
          total: [],
          delivery: { cash: [], card: [] },
          canteen: { cash: [], card: [] },
        }
      )
      setStaticData(data)
      setStatic(true)
    },
    onReload: ({ getPayments, week }) => () => getPayments(week),
    closeStatic: ({ setStatic }) => () => setStatic(false),
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearPayment()
      if (this.props.paymentStore.week) {
        this.props.getPayments(this.props.paymentStore.week)
      }
      this.props.getCategories()
    },
  }),
  withStyles(styles, { withTheme: true })
)(Payments)
