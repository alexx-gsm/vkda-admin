import React from 'react'
import { Link } from 'react-router-dom'
// useful tools
import classNames from 'classnames'
import moment from 'moment'
import isEmpty from '../../helpers/is-empty'
// DayPicker
import DayPicker from '../DayPicker'
// @material-ui components
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import CircularProgress from '@material-ui/core/CircularProgress'
import {
  FormControl,
  Input,
  TextField,
  MenuItem,
  TableFooter,
} from '@material-ui/core'
import { Grid, Typography, Hidden, Divider, Paper } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core'
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add'
import Autorenew from '@material-ui/icons/Autorenew'
// react-icon-kit
import { Icon } from 'react-icons-kit'
import { barChart } from 'react-icons-kit/fa/barChart'
import { u1F426 } from 'react-icons-kit/noto_emoji_regular/u1F426'
import { u1F374 } from 'react-icons-kit/noto_emoji_regular/u1F374'
import { u1F4B8 } from 'react-icons-kit/noto_emoji_regular/u1F4B8'
// components
import Graph from '../Graph'

const Payments = ({
  paymentStore,
  filteredPayments,
  incomeAmount,
  outcomeAmount,
  isStatic,
  staticData,
  classes,
  onReload,
  onClick,
  onSelectDay,
  onChange,
  onClickStatic,
  closeStatic,
  countPayments,
  formatter,
  types,
}) => {
  const { selectedDays = [], selectedDay, loading } = paymentStore

  const deliveryCash = isStatic
    ? staticData.delivery.cash.reduce((acc, i) => acc + i)
    : 0

  const deliveryCard = isStatic
    ? staticData.delivery.card.reduce((acc, i) => acc + i)
    : 0

  const canteenCash = isStatic
    ? staticData.canteen.cash.reduce((acc, i) => acc + i)
    : 0

  const canteenCard = isStatic
    ? staticData.canteen.card.reduce((acc, i) => acc + i)
    : 0

  const deliveryTotal = isStatic
    ? staticData.delivery.cash.map((cash, index) => {
        return cash + staticData.delivery.card[index]
      })
    : 0

  const canteenTotal = isStatic
    ? staticData.canteen.cash.map((cash, index) => {
        return cash + staticData.canteen.card[index]
      })
    : 0

  const getRow = ({ item, type }) => (
    <TableRow
      key={item._id}
      onClick={onClick(item._id)}
      className={
        item.isTransfer
          ? classes.rowTransfer
          : !item.isIncome
          ? classes.rowOutcome
          : classes.rowIncome
      }>
      <TableCell className={classes.cellData} padding="none">
        <Grid container className={classes.gridData}>
          <Typography className={classes.typoTableDay} variant="title">
            {moment(item.date).format('DD')}
          </Typography>
          <Typography className={classes.typoTableMonth}>
            {moment(item.date).format('MMM')}
          </Typography>
          <Typography className={classes.typoTableYear} variant="subheading">
            {moment(item.date).format('Y')}
          </Typography>
        </Grid>
      </TableCell>
      <TableCell padding="none" className={classes.CellIcon}>
        {item.isTransfer ? (
          <div
            style={{
              color: '#3f51b5',
            }}>
            <Icon size={32} icon={u1F4B8} />
          </div>
        ) : item.variant && item.variant === 'colibri' ? (
          <div
            style={{
              color: '#f44336',
            }}>
            <Icon size={32} icon={u1F426} />
          </div>
        ) : (
          <div
            style={{
              color: '#00acc1',
            }}>
            <Icon size={32} icon={u1F374} />
          </div>
        )}
      </TableCell>
      <TableCell padding="checkbox" className={classes.cellColored}>
        <Grid container className={classes.gridInfo}>
          <Grid item container className={classes.gridType}>
            <Typography variant="title">
              {item.isTransfer
                ? 'Перемещение'
                : !isEmpty(type)
                ? type.title
                : '-'}
            </Typography>
            {!isEmpty(item.title) && (
              <Typography className={classes.typoType} variant="caption">
                {item.title}
              </Typography>
            )}
            {!isEmpty(item.comment) && (
              <Typography className={classes.typoType} variant="caption">
                {item.comment}
              </Typography>
            )}
          </Grid>
        </Grid>
      </TableCell>
      <TableCell numeric padding="checkbox" className={classes.cellColored}>
        <Grid container className={classes.gridValue}>
          {item.isTransfer ? (
            <Typography className={classes.typoValue} variant="title">
              {formatter.format(+item.transferAmount)}
            </Typography>
          ) : (
            <Typography className={classes.typoValue} variant="title">
              {item.isIncome ? '+' : '-'}
              {formatter.format(+item.cash + +item.card)}
            </Typography>
          )}
        </Grid>
      </TableCell>
    </TableRow>
  )

  return (
    <div>
      <div className={classes.wrapTools}>
        <Grid container className={classes.tools} justify="space-between">
          <Grid item>
            <Button
              mini
              to={'/payments/edit'}
              component={Link}
              variant="fab"
              aria-label="Add"
              className={classes.buttonAdd}>
              <AddIcon />
            </Button>
            <div className={classes.wrapper}>
              <IconButton
                onClick={onReload}
                className={classes.button}
                aria-label="Reload">
                {loading ? '' : <Autorenew />}
              </IconButton>
              {loading && (
                <CircularProgress
                  size={40}
                  color="secondary"
                  className={classes.fabProgress}
                />
              )}
            </div>
          </Grid>
          <Grid item>
            <Typography variant="display2">Денежка</Typography>
          </Grid>
        </Grid>
      </div>

      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid container className={classes.gridHeader}>
            <Hidden xsDown>
              <Grid item className={classes.gridDays}>
                {selectedDays.map((day, i) => {
                  const count = countPayments(i)
                  return (
                    <Grid
                      item
                      key={i}
                      className={classNames(
                        classes.gridDay,
                        selectedDay === i ? 'active' : null
                      )}
                      onClick={onSelectDay(i)}>
                      <Typography variant="title" className={classes.typoWeek}>
                        {moment(day).format('dd')}
                      </Typography>
                      <Typography variant="caption" className={classes.typoDay}>
                        {moment(day).format('D MMM')}
                      </Typography>
                      {count > 0 && (
                        <Typography
                          variant="caption"
                          className={classes.typoCountPayments}>
                          {count}
                        </Typography>
                      )}
                    </Grid>
                  )
                })}
              </Grid>
            </Hidden>
            <Hidden smUp>
              <TextField
                select
                id="date-select"
                className={classes.selectDayForm}
                value={selectedDay}
                onChange={onChange('select-day')}
                SelectProps={{
                  classes: {
                    icon: classes.colorWhite,
                  },
                }}
                InputProps={{
                  classes: {
                    underline: classes.underlineWhite,
                    input: classes.selectHeaderText,
                  },
                }}
                margin="none">
                {selectedDays.map((day, i) => (
                  <MenuItem key={i} value={i}>
                    {moment(day).format('dd: D MMMM')}
                  </MenuItem>
                ))}
              </TextField>
            </Hidden>
            <Grid item>
              <FormControl className={classes.formControl}>
                <Input
                  id="date"
                  inputComponent={() => (
                    <DayPicker
                      format="W"
                      classes={classes}
                      closeStatic={closeStatic}
                    />
                  )}
                  className={classes.headerPicker}
                  disableUnderline
                />
                <Typography className={classes.typoHeaderPicker}>
                  неделя
                </Typography>
              </FormControl>
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paperTable}>
          <Grid container className={classes.gridTable}>
            <Table>
              <TableHead className={classes.tHead}>
                <TableRow>
                  <TableCell className={classes.headCellData} padding="none">
                    <Typography align="center" variant="caption">
                      Дата
                    </Typography>
                  </TableCell>
                  <TableCell padding="none"></TableCell>
                  <TableCell
                    className={classes.headCellInfo}
                    padding="checkbox">
                    Инфо
                  </TableCell>
                  <TableCell numeric padding="checkbox">
                    Сумма
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredPayments
                  .filter((fp) => fp.isIncome && !fp.isTransfer)
                  .map((item) => {
                    const type = types.find((t) => t._id === item.type)

                    return getRow({ item, type })
                  })}
                {filteredPayments
                  .filter((fp) => !fp.isTransfer && !fp.isIncome)
                  .map((item) => {
                    const type = types.find((t) => t._id === item.type)

                    return getRow({ item, type })
                  })}
                {filteredPayments
                  .filter((fp) => fp.isTransfer)
                  .map((item) => {
                    const type = types.find((t) => t._id === item.type)

                    return getRow({ item, type })
                  })}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TableCell padding="none" className={classes.cellStatic}>
                    <IconButton
                      onClick={onClickStatic}
                      className={classes.button}
                      aria-label="Static">
                      <Icon icon={barChart} size={32} />
                    </IconButton>
                  </TableCell>
                  <TableCell
                    padding="none"
                    className={classes.cellTotalValue}></TableCell>
                  <TableCell
                    colSpan={2}
                    padding="none"
                    className={classes.cellTotalValue}>
                    <Grid container direction="row" justify="flex-end">
                      <Grid className={classes.gridTotalValue}>
                        <Grid
                          item
                          container
                          direction="column"
                          alignItems="flex-end"
                          justify="center"
                          className={classes.gridMiddleTotalValue}>
                          <Typography
                            className={classes.typoMiddleValue}
                            variant="caption">
                            +{formatter.format(incomeAmount)}
                          </Typography>
                          <Typography
                            className={classes.typoMiddleValue}
                            variant="caption">
                            -{formatter.format(outcomeAmount)}
                          </Typography>
                        </Grid>
                        <Grid
                          item
                          container
                          direction="row"
                          alignItems="center"
                          className={classes.paddingL}>
                          <Typography
                            className={classes.typoTotalValue}
                            variant="display1">
                            {formatter.format(incomeAmount - outcomeAmount)}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
              </TableFooter>
            </Table>
          </Grid>
        </Paper>
        {isStatic && (
          <Grid container>
            <Grid
              container
              className={classes.gridStatic}
              spacing={16}
              wrap="nowrap">
              <Grid item>
                <Typography className={classes.typoStaticTitle} variant="title">
                  Статистика за неделю
                </Typography>
                <Typography
                  className={classes.typoStaticTitle}
                  variant="caption">
                  {`
                ${moment(selectedDays[0]).format('D MMMM')}
               - 
                ${moment(selectedDays[6]).format('D MMMM')}
               `}
                </Typography>
                <Divider />
              </Grid>
              <Grid
                item
                container
                direction="row"
                wrap="nowrap"
                className={classes.gridStaticInfo}>
                <Grid item container alignItems="center" justify="center">
                  <Graph data={[staticData.total]} />
                </Grid>
                <Grid item container direction="column">
                  <Paper className={classNames(classes.paperStatic, 'cyan')}>
                    <Grid container direction="row" wrap="nowrap">
                      <Grid item container direction="column" sm={12}>
                        <Grid item>
                          <Typography variant="caption">Доход:</Typography>
                          <Typography variant="subheading">
                            +
                            {formatter.format(
                              staticData.income.reduce((acc, i) => acc + i)
                            )}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography variant="caption">Расход:</Typography>
                          <Typography variant="subheading">
                            {formatter.format(
                              staticData.outcome.reduce((acc, i) => acc + i)
                            )}
                          </Typography>
                        </Grid>
                      </Grid>
                      <Grid item container direction="column" sm={12}>
                        <Typography variant="caption">Сальдо:</Typography>
                        <Typography variant="display1">
                          {formatter.format(
                            staticData.total.reduce((acc, i) => acc + i)
                          )}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
            </Grid>

            <Grid
              container
              className={classes.gridStatic}
              spacing={16}
              wrap="nowrap">
              <Grid item>
                <Typography className={classes.typoStaticTitle} variant="title">
                  Доставка
                </Typography>
                <Divider />
              </Grid>
              <Grid
                item
                container
                direction="row"
                wrap="nowrap"
                className={classes.gridStaticInfo}>
                <Grid item container alignItems="center" justify="center">
                  <Graph data={[deliveryTotal]} color="orange" />
                </Grid>
                <Grid item container direction="column">
                  <Paper className={classNames(classes.paperStatic, 'orange')}>
                    <Grid container direction="row" wrap="nowrap">
                      <Grid item container direction="column" sm={12}>
                        <Grid item>
                          <Typography variant="caption">Наличные:</Typography>
                          <Typography variant="subheading">
                            +{formatter.format(deliveryCash)}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography variant="caption">Карта:</Typography>
                          <Typography variant="subheading">
                            +{formatter.format(deliveryCard)}
                          </Typography>
                        </Grid>
                      </Grid>
                      <Grid item container direction="column" sm={12}>
                        <Typography variant="caption">Доставка:</Typography>
                        <Typography variant="display1">
                          {formatter.format(deliveryCard + deliveryCash)}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
            </Grid>

            <Grid
              container
              className={classes.gridStatic}
              spacing={16}
              wrap="nowrap">
              <Grid item>
                <Typography className={classes.typoStaticTitle} variant="title">
                  Столовая
                </Typography>
                <Divider />
              </Grid>
              <Grid
                item
                container
                direction="row"
                wrap="nowrap"
                className={classes.gridStaticInfo}>
                <Grid item container alignItems="center" justify="center">
                  <Graph data={[canteenTotal]} color="indigo" />
                </Grid>
                <Grid item container direction="column">
                  <Paper className={classNames(classes.paperStatic, 'indigo')}>
                    <Grid container direction="row" wrap="nowrap">
                      <Grid item container direction="column" sm={12}>
                        <Grid item>
                          <Typography variant="caption">Наличные:</Typography>
                          <Typography variant="subheading">
                            +{formatter.format(canteenCash)}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography variant="caption">Карта:</Typography>
                          <Typography variant="subheading">
                            +{formatter.format(canteenCard)}
                          </Typography>
                        </Grid>
                      </Grid>
                      <Grid item container direction="column" sm={12}>
                        <Typography variant="caption">Столовая:</Typography>
                        <Typography variant="display1">
                          {formatter.format(canteenCard + canteenCash)}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Paper>
    </div>
  )
}

Payments.defaultProps = {
  filteredPayments: [],
  types: [],
}
export default Payments
