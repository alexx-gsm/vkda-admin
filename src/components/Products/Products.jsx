import React from 'react';
import { Link } from 'react-router-dom';
// components
import EnchancedTable from '../EnchancedTable';
// @material-ui components
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { Typography, Paper } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core';
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add';
import Autorenew from '@material-ui/icons/Autorenew';

const Products = ({
  classes,
  products,
  onClick,
  onEdit,
  onReload,
  loading,
  formatter
}) => {
  return (
    <div>
      <Grid container className={classes.tools} justify="space-between">
        <Grid item className={classes.gridTools}>
          <Button
            mini
            variant="fab"
            to={'/products/edit'}
            component={Link}
            aria-label="Add"
            className={classes.buttonAdd}
          >
            <AddIcon />
          </Button>
          <div className={classes.wrapper}>
            <IconButton
              onClick={onReload}
              className={classes.button}
              aria-label="Reload"
            >
              {loading ? '' : <Autorenew />}
            </IconButton>
            {loading && (
              <CircularProgress
                size={40}
                color="secondary"
                className={classes.fabProgress}
              />
            )}
          </div>
        </Grid>
      </Grid>
      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid container className={classes.gridHeader}>
            <Typography variant="display1" className={classes.typoHeaderTitle}>
              Продукты
            </Typography>
          </Grid>
        </Paper>

        <Paper className={classes.paperTable}>
          <Grid container className={classes.gridTable}>
            <Table>
              <TableHead className={classes.tHead}>
                <TableRow>
                  <TableCell
                    className={classes.headCellData}
                    padding="checkbox"
                  >
                    <Typography variant="caption">Название</Typography>
                  </TableCell>

                  <TableCell
                    numeric
                    padding="checkbox"
                    className={classes.headerCellInfo}
                  >
                    Цена за г.
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map(product => {
                  return (
                    <TableRow
                      key={product._id}
                      className={classes.tableRow}
                      onClick={onClick(product._id)}
                    >
                      <TableCell
                        className={classes.cellData}
                        padding="checkbox"
                      >
                        <Grid container className={classes.gridInfo}>
                          <Grid item container className={classes.gridType}>
                            <Typography variant="title">
                              {product.title}
                            </Typography>

                            <Typography
                              className={classes.typoType}
                              variant="caption"
                            >
                              {product.name}
                            </Typography>
                          </Grid>
                        </Grid>
                      </TableCell>
                      <TableCell
                        padding="checkbox"
                        className={classes.cellInfo}
                      >
                        <Typography variant="title" align="right">
                          {formatter.format(product.price)}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Grid>
        </Paper>
      </Paper>
    </div>
  );
};

Products.defaultProps = {
  products: []
};

export default Products;
