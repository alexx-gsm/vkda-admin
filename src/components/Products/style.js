import green from '@material-ui/core/colors/green';
export default theme => ({
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridTools: {
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },
  buttonAdd: {
    color: 'white',
    backgroundColor: green[800],
    '&:hover': { backgroundColor: green[900] }
  },
  wrapper: {
    display: 'inline-block',
    margin: `0 ${theme.spacing.unit}`,
    position: 'relative'
  },
  card: {
    maxWidth: '580px',
    margin: '40px 0',
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  cardHeader: {
    position: 'relative',
    margin: '0',
    top: '-20px',
    padding: '5px 10px',
    background: `linear-gradient(60deg, ${green[800]}, ${green[900]})`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoHeaderTitle: {
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  content: {
    marginTop: '20px',
    [theme.breakpoints.up('sm')]: {
      width: '580px',
      marginLeft: 'auto',
      marginRight: 'auto'
    },
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      marginTop: '10px'
    }
  },
  tableRow: {
    cursor: 'pointer',
    '& td': { background: green[100] },
    '&:nth-child(odd) td': {
      background: green[50]
    },
    '&:hover td': { background: green[200] }
  }
});
