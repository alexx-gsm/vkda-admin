import { compose, lifecycle, withHandlers, withProps } from 'recompose';
import { connect } from 'react-redux';
// AC
import { getProducts, clearProduct } from '../../redux/modules/product';
// components
import Products from './Products';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './style';
// import ProductList from './ProductList';

export default compose(
  connect(
    ({ productStore }) => {
      const { products, loading } = productStore;
      return {
        products,
        loading
      };
    },
    { getProducts, clearProduct }
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0
    })
  }),
  lifecycle({
    componentDidMount() {
      this.props.getProducts();
      this.props.clearProduct();
    }
  }),
  withHandlers({
    onClick: ({ history }) => id => () => history.push(`/products/edit/${id}`)
  }),
  withStyles(styles, { withTheme: true })
)(Products);
