import React from 'react';
import classNames from 'classnames';
// useful tools
import isEmpty from '../../../helpers/is-empty';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
import teal from '@material-ui/core/colors/teal';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
// @material-ui/icons
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import { Typography, Grid, Paper } from '@material-ui/core';
// icons
import { Icon } from 'react-icons-kit';
import { ic_add_circle } from 'react-icons-kit/md/ic_add_circle';

const ComplexProduct = ({
  classes,
  error,
  content,
  products,
  unusedProducts,
  price,
  onChange,
  onSelect,
  onAdd,
  onDelete
}) => {
  const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: teal[500],
      color: theme.palette.common.white,
      padding: '5px',
      textTransform: 'uppercase'
    },
    body: {
      fontSize: 14
    }
  }))(TableCell);

  const selectMenuItems = unusedProducts.map(option => (
    <MenuItem key={option._id} value={option._id}>
      {option.title}
    </MenuItem>
  ));

  return (
    <div className={classes.tableWrap}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <CustomTableCell />
            <CustomTableCell numeric>Название</CustomTableCell>
            <CustomTableCell numeric>Цена</CustomTableCell>
            <CustomTableCell className={classes.textCenter}>
              Вес
            </CustomTableCell>
            <CustomTableCell numeric>Итог</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {content.map((item, index) => (
            <TableRow key={item._id} className={classes.bodyRow}>
              <TableCell
                className={classNames(classes.removeCell, 'icon-remove')}
              >
                <RemoveCircleOutline onClick={onDelete(index)} />
              </TableCell>
              <TableCell numeric>
                {!isEmpty(item) && item._id !== '0' ? (
                  <div className="text-right cell--title">
                    {!isEmpty(products)
                      ? products.find(p => p._id === item._id).title
                      : ''}
                  </div>
                ) : (
                  <TextField
                    select
                    fullWidth
                    error={Boolean(error.content)}
                    className={classes.selectItem}
                    value="0"
                    onChange={onSelect(index)}
                    SelectProps={{
                      MenuProps: {
                        className: classes.menu
                      }
                    }}
                    helperText={!isEmpty(error.content) ? error.content : ''}
                    FormHelperTextProps={{
                      className: classes.selectError
                    }}
                    margin="none"
                  >
                    {selectMenuItems}
                  </TextField>
                )}
              </TableCell>
              <TableCell numeric>
                <Typography variant="body1">
                  {item.price ? item.price.toFixed(2) : ''}
                </Typography>
              </TableCell>

              <TableCell numeric>
                <TextField
                  fullWidth
                  id="amount"
                  value={item.amount}
                  onChange={onChange(index)}
                  type="number"
                  min={1}
                  step={1}
                />
              </TableCell>
              <TableCell numeric>
                <Typography variant="body2">
                  {(+item.price * +item.amount).toFixed(2)}
                </Typography>
              </TableCell>
            </TableRow>
          ))}
          {!isEmpty(unusedProducts) && (
            <TableRow className={classes.bodyRow}>
              <TableCell className={classNames(classes.addCell, 'icon-add')}>
                <Icon size={30} icon={ic_add_circle} onClick={onAdd} />
              </TableCell>
              <TableCell />
            </TableRow>
          )}
        </TableBody>
      </Table>
      {isEmpty(content) &&
        error.content && (
          <Typography color="error" variant="title" align="center">
            {error.content}
          </Typography>
        )}

      <Grid container justify="flex-end">
        <Grid item>
          <Paper className={classes.complexPricePaper}>
            <Grid row container alignItems="baseline">
              <Typography variant="display3">
                {price ? price.toFixed(2) : 0}
              </Typography>
              <Typography variant="display1">{'₽/г'}</Typography>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default ComplexProduct;
