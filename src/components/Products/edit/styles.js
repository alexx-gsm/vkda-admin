import teal from '@material-ui/core/colors/teal';
import red from '@material-ui/core/colors/red';

export default theme => ({
  padding: { padding: '20px' },
  margin: {
    margin: theme.spacing.unit
  },
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    alignItems: 'center',
    maxWidth: '580px',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridTools: {
    [theme.breakpoints.up('sm')]: {
      // padding: '0 20px'
    }
  },
  divider: {
    // padding: '0 20px',
    maxWidth: '580px'
  },
  content: {
    marginTop: '20px',
    [theme.breakpoints.up('sm')]: {
      width: '580px'
    },
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      marginTop: '10px'
    }
  },
  form: {
    padding: '0px 20px 20px',
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  title: {
    background: teal[500],
    color: 'white',
    fontSize: '26px',
    textTransform: ' uppercase',
    padding: '5px 0',
    borderRadius: '4px 4px 0 0',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0
    }
  },
  formGroup: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    flexWrap: 'nowrap',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'flex-start'
    }
  },
  name: {
    fontSize: '46px',
    color: '#333',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  select: {
    [theme.breakpoints.up('sm')]: {
      flexBasis: '50%'
    }
  },
  selectItem: {
    paddingTop: 0,
    paddingBottom: 0,
    height: '21px'
  },
  colorSwitchBase: {
    color: teal[500],
    '&$colorChecked': {
      color: teal[700],
      '& + $colorBar': {
        backgroundColor: teal[700]
      }
    }
  },
  colorBar: {},
  colorChecked: {},
  colorIconChecked: {},

  singleBlock: {
    marginTop: '20px',
    backgroundColor: teal[50],
    border: '1px solid',
    borderColor: teal[200]
  },
  priceBox: {
    display: 'flex',
    height: '100%',
    background: teal[100],
    border: `solid 1px ${teal[200]}`,
    borderRadius: '4px'
    // borderBottom: 'none'
  },
  price: {
    fontSize: '60px',
    textAlign: 'center'
  },
  footer: {
    padding: '10px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  cssRoot: {
    color: theme.palette.getContrastText(teal[500]),
    backgroundColor: teal[500],
    '&:hover': {
      backgroundColor: teal[700]
    }
  },
  tableWrap: {
    margin: '0 -8px'
  },
  table: {
    marginTop: '20px'
  },
  headRow: {
    height: '28px',
    minHeight: '28px'
  },
  bodyRow: {
    height: '28px',
    '&:first-child td': { paddingTop: '10px' },
    '&:nth-child(even) td': { backgroundColor: teal[50] },
    '&:last-child td': { backgroundColor: 'white' },
    '& td': {
      borderBottom: 'none',
      padding: '0',
      fontSize: '1rem',
      '&:not(:first-child)': { padding: '0 5px' },
      '&:last-child': { padding: '5px' },
      '&.icon-remove': {
        paddingLeft: '3px',
        paddingTop: '5px',
        cursor: 'pointer'
      },
      '&.icon-add': {
        paddingTop: '10px',
        cursor: 'pointer'
      }
    },
    '& td input': {
      width: '50px',
      textAlign: 'center',
      padding: 0
    }
  },
  textCenter: {
    textAlign: 'center'
  },
  removeCell: {
    paddingLeft: '3px',
    '&:hover': {
      color: red[900]
    }
  },
  addCell: {
    color: teal[500],
    fontSize: '30px',
    '&:hover': {
      color: teal[600]
    }
  },
  selectError: {
    position: 'absolute',
    bottom: '-15px',
    right: 0
  },
  complexPricePaper: {
    padding: '20px 30px',

    background: teal[100],
    border: '1px solid',
    borderColor: teal[300],
    [theme.breakpoints.down('xs')]: {
      margin: '0 20px'
    }
  }
});
