import React from 'react';
import classNames from 'classnames';
import {
  Grid,
  TextField,
  InputAdornment,
  Paper,
  FormControl,
  InputLabel,
  Input
} from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
// icons
import { Icon } from 'react-icons-kit';
import { rub } from 'react-icons-kit/fa/rub';
import { balanceScale } from 'react-icons-kit/fa/balanceScale';
import { trashO } from 'react-icons-kit/fa/trashO';

const SimpleProduct = ({
  value,
  amount,
  price,
  waste,
  onChange,
  error,
  classes
}) => {
  return (
    <Paper className={classes.singleBlock}>
      <Grid container>
        <Grid item xs={12} sm={6} className={classes.padding}>
          <TextField
            fullWidth
            id="value"
            label="Цена"
            value={value}
            onChange={onChange('value')}
            type="number"
            inputProps={{
              min: 0,
              step: 1
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Icon icon={rub} />
                </InputAdornment>
              )
            }}
            error={error.value}
            helperText={error.value ? error.value : null}
            margin="normal"
          />
          <TextField
            fullWidth
            id="amount"
            label="Вес, г"
            value={amount}
            onChange={onChange('amount')}
            type="number"
            min={1}
            step={1}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Icon size={18} icon={balanceScale} />
                </InputAdornment>
              )
            }}
            margin="normal"
          />
          <TextField
            fullWidth
            id="waste"
            label="Отходы, %"
            value={waste}
            onChange={onChange('waste')}
            type="number"
            min={0}
            max={50}
            step={1}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Icon size={18} icon={trashO} />
                </InputAdornment>
              )
            }}
            margin="normal"
          />
        </Grid>
        <Grid container item xs={12} sm={6} className={classes.padding}>
          <FormControl margin="normal" disabled>
            <InputLabel htmlFor="price">Цена за г.</InputLabel>
            <Input
              id="price"
              value={price ? price.toFixed(2) : 0}
              className={classes.priceBox}
              classes={{ input: classes.price }}
            />
          </FormControl>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default SimpleProduct;
