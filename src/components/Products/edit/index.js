import {
  compose,
  withState,
  withProps,
  withHandlers,
  lifecycle
} from 'recompose';
import { connect } from 'react-redux';
// useful tools
import isEmpty from '../../../helpers/is-empty';
// AC
import {
  addProduct,
  saveProduct,
  getProducts,
  getProductById,
  storeProduct,
  storeUnusedProducts,
  storePrice,
  clearProduct,
  deleteProduct
} from '../../../redux/modules/product';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';

// Components
import withDialog from '../../Dialog';
import ProductEdit from './ProductEdit';

const getSimpleProductPrice = product => {
  const { value, amount, waste } = product;

  return amount > 0 ? value / amount / (1 - waste / 100) : 0;
};

const getComplexProductPrice = content => {
  const total = content.reduce(
    (total, item) => ({
      totalAmount: total.totalAmount + +item.amount,
      totalPrice: total.totalPrice + +item.price * +item.amount
    }),
    { totalAmount: 0, totalPrice: 0 }
  );
  return total.totalAmount !== 0 ? total.totalPrice / total.totalAmount : 0;
};

export default compose(
  connect(
    ({ productStore }) => {
      const {
        products,
        product,
        isProductLoaded,
        unusedProducts,
        categories,
        error
      } = productStore;
      return {
        products,
        product,
        isProductLoaded,
        unusedProducts,
        categories,
        error
      };
    },
    {
      addProduct,
      saveProduct,
      getProducts,
      getProductById,
      storeProduct,
      storeUnusedProducts,
      storePrice,
      clearProduct,
      onConfirm: deleteProduct
    }
  ),
  withState('isVisible', 'setVisible', false),
  withState('deletingItemId', 'setDeletingProductId', ''),
  withProps({
    modalTitle: 'Удаление документа',
    modalText: 'Вы уверены, что хотите удалить этот документ?'
  }),
  withHandlers({
    onChange: ({ product, storeProduct }) => name => event => {
      if (name === 'value' || name === 'amount' || name === 'waste') {
        const updatedProduct = {
          ...product,
          [name]: +event.target.value
        };

        const { value, amount, waste } = updatedProduct;

        if (amount > 0) {
          storeProduct({
            ...updatedProduct,
            price: value / amount / (1 - waste / 100)
          });
        }
      } else {
        storeProduct({
          ...product,
          [name]: event.target.value
        });
      }
    },
    onSave: ({ product, history, addProduct, saveProduct }) => () => {
      if (isEmpty(product._id)) {
        addProduct(product, history);
      } else {
        saveProduct(product, history);
      }
    },

    onDelete: ({ setDeletingProductId, setVisible, deletingItemId }) => () => {
      setDeletingProductId(deletingItemId);
      setVisible(true);
    },

    onSelectComplexProductContent: ({
      product,
      products,
      storeProduct,
      storeUnusedProducts
    }) => index => event => {
      const selectedProductID = event.target.value;

      if (selectedProductID === '0') return;

      const { _id, title, price } = products.find(
        p => p._id === selectedProductID
      );

      const content = [
        ...product.content.slice(0, index),
        { _id, title, price, amount: 1 },
        ...product.content.slice(index + 1)
      ];

      storeProduct({
        ...product,
        price: getComplexProductPrice(content),
        content
      });

      const usedProducts = content.map(item => item._id);
      const restProducts = products.filter(
        p => usedProducts.indexOf(p._id) === -1
      );
      storeUnusedProducts(
        restProducts.filter(p => !p.isComplex && p._id !== product._id)
      );
    },

    onChangeComplexProductContent: ({
      product,
      storeProduct
    }) => index => event => {
      const newAmount = event.target.value;

      const content = [
        ...product.content.slice(0, index),
        { ...product.content[index], amount: newAmount },
        ...product.content.slice(index + 1)
      ];
      storeProduct({
        ...product,
        price: getComplexProductPrice(content),
        content
      });
    },

    onAddComplexProductContent: ({ product, storeProduct }) => () => {
      // check if empty row is already in content (with id=0)
      //  if yes, skip adding new one
      if (!isEmpty(product.content.find(c => c._id === '0'))) return;
      // add empty row into product content (with id = 0)
      storeProduct({
        ...product,
        content: [
          ...product.content,
          {
            _id: '0',
            title: '',
            price: 0,
            amount: 1
          }
        ]
      });
    },

    onDeleteComplexProductContent: ({
      product,
      products,
      storeProduct,
      storeUnusedProducts
    }) => index => () => {
      const content = product.content.filter((c, i) => index !== i);
      storeProduct({
        ...product,
        price: getComplexProductPrice(content),
        content
      });

      const usedProducts = content.map(item => item._id);
      const restProducts = products.filter(
        p => usedProducts.indexOf(p._id) === -1
      );
      storeUnusedProducts(
        restProducts.filter(p => !p.isComplex && p._id !== product._id)
      );
    }
  }),

  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params;
      if (id) {
        this.props.getProductById(id);
        this.props.setDeletingProductId(id);
      } else {
        this.props.clearProduct();
      }

      this.props.getProducts();
    },
    componentDidUpdate(prevProps) {
      const thisProducts = this.props.products;
      const thisProduct = this.props.product;
      const { id } = this.props.match.params;

      if (
        isEmpty(id) &&
        isEmpty(prevProps.product.content) &&
        isEmpty(prevProps.unusedProducts) &&
        !isEmpty(thisProducts)
      ) {
        this.props.storeUnusedProducts(thisProducts.filter(p => !p.isComplex));
      }

      if (
        this.props.isProductLoaded &&
        !isEmpty(thisProduct) &&
        !isEmpty(thisProducts)
      ) {
        const content = [
          ...thisProduct.content.map(item => ({
            ...item,
            price: thisProducts.find(p => p._id === item._id).price
          }))
        ];

        const price = thisProduct.isComplex
          ? getComplexProductPrice(content)
          : getSimpleProductPrice(thisProduct);

        this.props.storeProduct({
          ...thisProduct,
          content,
          price
        });

        const usedProducts = thisProduct.content.map(item => item._id);
        const restProducts = thisProducts.filter(
          p => usedProducts.indexOf(p._id) === -1
        );
        this.props.storeUnusedProducts(
          restProducts.filter(p => !p.isComplex && p._id !== thisProduct._id)
        );
      }

      if (
        !this.props.isProductLoaded &&
        prevProps.product.isComplex &&
        !thisProduct.isComplex
      ) {
        const price = getSimpleProductPrice(thisProduct);
        this.props.storePrice(price);
      }

      if (
        !this.props.isProductLoaded &&
        !prevProps.product.isComplex &&
        thisProduct.isComplex
      ) {
        const price = getComplexProductPrice(thisProduct.content);
        this.props.storePrice(price);
      }
    }
  }),
  withStyles(styles, { withTheme: true }),
  withDialog
)(ProductEdit);
