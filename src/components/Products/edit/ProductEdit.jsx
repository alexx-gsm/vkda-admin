import React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
// @material-ui components
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { Typography, Hidden, Paper, Divider } from '@material-ui/core';
// @material-ui/icons
import ArrowBack from '@material-ui/icons/ArrowBack';
// components
import SimpleProduct from './SimpleProduct';
import ComplexProduct from './ComplexProduct';
// useful utils
import isEmpty from '../../../helpers/is-empty';
// icons
import { Icon } from 'react-icons-kit';
import { trashO } from 'react-icons-kit/fa/trashO';

const ProductEdit = ({
  classes,
  products,
  product,
  unusedProducts,
  categories,
  error,
  onChange,
  onSave,
  onDelete,
  onSelectComplexProductContent,
  onChangeComplexProductContent,
  onAddComplexProductContent,
  onDeleteComplexProductContent
}) => {
  const {
    _id,
    title,
    category,
    isComplex,
    value,
    amount,
    price,
    waste,
    comment,
    content
  } = product;

  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridTools}>
          <Button
            mini
            variant="fab"
            to={'/products'}
            component={Link}
            aria-label="Add"
          >
            <ArrowBack />
          </Button>
        </Grid>
        <Grid item>
          <Typography variant="display2">Продукт</Typography>
        </Grid>
      </Grid>

      <Hidden xsDown>
        <Divider className={classes.divider} />
      </Hidden>

      <Paper className={classes.content}>
        <Typography
          variant={'display2'}
          align="center"
          className={classes.title}
        >
          {isEmpty(_id) ? 'Новый' : 'Редактируем'}
        </Typography>

        <form className={classes.form} noValidate autoComplete="off">
          <TextField
            fullWidth
            id="title"
            label="Название"
            value={title}
            onChange={onChange('title')}
            margin="normal"
            InputProps={{
              className: classes.name
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />

          <FormGroup row className={classes.formGroup}>
            <FormControlLabel
              control={
                <Switch
                  checked={isComplex}
                  onChange={onChange('isComplex')}
                  value={isComplex ? '' : 'true'}
                  classes={{
                    switchBase: classes.colorSwitchBase,
                    checked: classes.colorChecked,
                    bar: classes.colorBar
                  }}
                />
              }
              label="Составной"
            />

            <TextField
              select
              fullWidth
              error={Boolean(error.category)}
              id="select-category"
              label="Категория"
              className={classes.select}
              value={category}
              onChange={onChange('category')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu
                }
              }}
              helperText={error.category ? error.category : ''}
              margin="normal"
            >
              {categories.map(option => (
                <MenuItem key={option._id} value={option._id}>
                  {option.title}
                </MenuItem>
              ))}
            </TextField>
          </FormGroup>

          {isComplex ? (
            <ComplexProduct
              {...{
                classes,
                error,
                content,
                products,
                unusedProducts,
                price,
                onChange: onChangeComplexProductContent,
                onSelect: onSelectComplexProductContent,
                onAdd: onAddComplexProductContent,
                onDelete: onDeleteComplexProductContent
              }}
            />
          ) : (
            <SimpleProduct
              {...{ value, amount, price, waste, onChange, error, classes }}
            />
          )}

          <TextField
            fullWidth
            id="multiline-flexible"
            label="Комментарий"
            multiline
            rowsMax="5"
            value={comment}
            onChange={onChange('comment')}
            className={classes.textField}
            margin="normal"
          />
        </form>
        <Divider />
        <Grid container className={classes.footer}>
          <Grid item>
            <Button
              mini
              onClick={onDelete}
              variant="fab"
              aria-label="Add"
              color="secondary"
              className={classes.margin}
            >
              <Icon size={18} icon={trashO} />
            </Button>
          </Grid>
          <Grid item>
            <Button
              to={'/products'}
              component={Link}
              aria-label="Add"
              variant="contained"
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label="Add"
              variant="contained"
              className={classNames(classes.margin, classes.cssRoot)}
              color="primary"
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

ProductEdit.defaultProps = {
  products: [],
  product: {}
};

export default ProductEdit;
