import grey from "@material-ui/core/colors/grey";
import amber from "@material-ui/core/colors/amber";
import red from "@material-ui/core/colors/red";
import teal from "@material-ui/core/colors/teal";
import blue from "@material-ui/core/colors/blue";
import deepOrange from "@material-ui/core/colors/deepOrange";

export default theme => ({
  paperSelect: {
    marginBottom: "30px",
    padding: "0 20px 5px",
    [theme.breakpoints.down("xs")]: {
      // marginBottom: "0"
    }
  },
  marginBottom: {
    marginBottom: "15px"
  },
  wrap: {
    marginBottom: "15px",
    [theme.breakpoints.down("xs")]: {
      margin: "20px auto"
    }
  },
  rootCard: {
    overflow: "visible",
    position: "relative",
    minWidth: "250px"
  },
  rootCardHeader: {
    position: "relative",
    top: "-15px",
    margin: "0 15px",
    borderRadius: "5px",
    background: amber[800],
    boxShadow:
      "0px 1px 3px 0px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12)",
    "&.grey": { background: grey[500] },
    "&.teal": { background: teal[500] },
    "&.blue": { background: blue[500] },
    "&.deepOrange": { background: deepOrange[500] }
  },
  rootCardHeaderTitle: {
    color: theme.palette.common.white,
    textAlign: "center",
    textTransform: "uppercase",
    fontWeight: "bold"
  },
  typoSummaryTitle: {
    marginBottom: "5px",
    textTransform: "uppercase",
    borderBottom: `1px dotted ${grey[200]}`
  },
  amount: {
    minWidth: "120px",
    textAlign: "right",
    borderLeft: `1px solid ${teal[500]}`,
    color: teal[800],
    marginLeft: "10px",
    "&.red": {
      borderColor: red[500],
      color: red[900]
    }
  },
  cardFooter: {
    borderTop: `1px solid ${grey[300]}`,
    borderRadius: "0 0 4px 4px",
    background: grey[50]
  },
  total: { paddingLeft: "10px" }
});
