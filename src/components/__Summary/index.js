import { getYearPayments } from '../../redux/modules/payment'
import { connect } from 'react-redux'
import {
  compose,
  lifecycle,
  withProps,
  withState,
  withHandlers,
} from 'recompose'
// AC
import { getCategories } from '../../redux/modules/category'
import { setFilter } from '../../redux/modules/filter'
// useful tools
import isEmpty from '../../helpers/is-empty'
import moment from 'moment'
// @material-ui
import { withStyles } from '@material-ui/core/styles'
// core component
import Summary from './Summary'
// styles
import styles from './styles'

export default compose(
  connect(
    ({ payments, categoryStore, filterStore }) => {
      const { categories } = categoryStore
      const { yearPayments } = payments
      const { filters } = filterStore

      const getSummaryByCategoryTitle = (payments, title) => {
        const rootCategory = categories.find((c) => c.title === title)
        if (isEmpty(rootCategory)) {
          return {}
        }

        const filteredCategories = categories.filter(
          (c) => c.rootId === rootCategory._id
        )

        console.log('filteredCategories', filteredCategories)
        const { total, ...rest } = filteredCategories.reduce(
          (acc, cat) => {
            const filteredPayments = payments.filter((p) => p.type === cat._id)
            const summary = filteredPayments.reduce((acc, p) => {
              return acc + p.card + p.cash
            }, 0)
            return {
              ...acc,
              [cat.title]: title === 'Расход' ? -summary : summary,
              total: acc.total + summary,
            }
          },
          { total: 0 }
        )

        return {
          total: title === 'Расход' ? -total : total,
          sum: rest,
        }
      }

      const monthColors = [
        'blue',
        'teal',
        'amber',
        'grey',
        'deepOrange',
        'teal',
        'teal',
        'blue',
        'deepOrange',
        'amber',
        'blue',
        'teal',
      ]

      const { summuryYear } = filters

      const summary =
        !isEmpty(yearPayments) && !isEmpty(categories)
          ? [11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0].reduce((acc, month) => {
              const monthPayments = yearPayments.filter((p) =>
                moment(p.date).isSame(moment([summuryYear, month]), 'month')
              )

              return isEmpty(monthPayments)
                ? acc
                : [
                    ...acc,
                    {
                      title: moment([summuryYear, month]).format('MMMM'),
                      data: [
                        {
                          title: 'Приход',
                          data: getSummaryByCategoryTitle(
                            monthPayments,
                            'Приход'
                          ),
                        },
                        {
                          title: 'Расход',
                          data: getSummaryByCategoryTitle(
                            monthPayments,
                            'Расход'
                          ),
                        },
                      ],
                      color: monthColors[month],
                    },
                  ]
            }, [])
          : []

      console.log('summary', summary)
      return {
        payments,
        yearPayments,
        summary,
        filters,
      }
    },
    { getYearPayments, getCategories, setFilter }
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 1,
    }),
  }),
  withHandlers({
    onSelect: ({ setFilter, getYearPayments }) => (event) => {
      const year = event.target.value
      setFilter(year)
      getYearPayments(year)
    },
  }),
  lifecycle({
    componentDidMount() {
      const { summuryYear } = this.props.filters

      this.props.getYearPayments(summuryYear)
      this.props.getCategories()
    },
  }),
  withStyles(styles, { withTheme: true })
)(Summary)
