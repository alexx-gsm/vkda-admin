import React from 'react'
import classNames from 'classnames'
import {
  Paper,
  TextField,
  MenuItem,
  Grid,
  Card,
  CardHeader,
  CardContent,
  Typography,
} from '@material-ui/core'
// useful tools
import isEmpty from '../../helpers/is-empty'
// react-icon-kit
// import { Icon } from 'react-icons-kit';
// import { rub } from 'react-icons-kit/fa/rub';

const Summary = ({ summary, filters, onSelect, formatter, classes }) => {
  const { summuryYear, arrYears } = filters
  return (
    <div>
      <Paper className={classes.paperSelect}>
        <TextField
          select
          id="year-select"
          label="Год"
          value={isEmpty(summuryYear) ? '' : summuryYear}
          onChange={onSelect}
          margin="normal">
          {arrYears.map((year) => (
            <MenuItem key={year} value={year}>
              <Grid container direction="row" wrap="wrap">
                <Typography variant="title">{year}</Typography>
              </Grid>
            </MenuItem>
          ))}
        </TextField>
      </Paper>
      <Grid container spacing={16}>
        {summary.map((ms) => (
          <Grid item className={classes.wrap} key={ms.title}>
            <Card
              className={classes.summaryCard}
              classes={{ root: classes.rootCard }}>
              <CardHeader
                title={ms.title}
                classes={{
                  root: classes.rootCardHeader,
                  title: classes.rootCardHeaderTitle,
                }}
                className={ms.color}
              />
              <CardContent>
                {ms.data.map((s) => (
                  <div key={s.title} className={classes.marginBottom}>
                    <Typography
                      variant="caption"
                      align="right"
                      className={classes.typoSummaryTitle}>
                      {s.title}
                    </Typography>
                    {!isEmpty(s.data) &&
                      Object.keys(s.data.sum).map((key) => (
                        <Grid
                          key={key}
                          container
                          alignItems="center"
                          justify="flex-end">
                          <Typography variant="caption">{key}</Typography>
                          {/* {console.log('---', s.data.sum)} */}
                          <Typography
                            variant="title"
                            className={classNames(
                              classes.amount,
                              s.title === 'Расход' ? 'red' : ''
                            )}>
                            {s.data.sum[key] === 0
                              ? '-'
                              : formatter.format(s.data.sum[key])}
                          </Typography>
                          <Typography
                            variant="title"
                            className={classNames(
                              classes.amount,
                              s.title === 'Расход' ? 'red' : ''
                            )}>
                            {s.data.sum[key] === 0
                              ? '-'
                              : formatter.format(s.data.sum[key])}
                          </Typography>
                        </Grid>
                      ))}
                  </div>
                ))}
              </CardContent>
              <CardContent className={classes.cardFooter}>
                <Grid container alignItems="baseline" justify="flex-end">
                  <Typography variant="title">Итого:</Typography>
                  <Typography variant="display1" className={classes.total}>
                    {formatter.format(
                      !isEmpty(ms)
                        ? ms.data.reduce((acc, s) => {
                            return acc + s.data.total
                          }, 0)
                        : 0
                    )}
                  </Typography>
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </div>
  )
}

Summary.defaultProps = {
  monthPayments: [],
  summury: {},
}
export default Summary
