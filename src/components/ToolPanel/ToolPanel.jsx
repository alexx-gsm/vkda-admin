import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// material-ui components
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add';
import Autorenew from '@material-ui/icons/Autorenew';
// styles
import styles from './styles';

const ToolPanel = ({
  addButtonLink,
  loading,
  title,
  classes,
  color,
  width,
  onReload
}) => {
  return (
    <div className={classes.wrapTools}>
      <Grid container className={classes.tools} style={{ maxWidth: width }}>
        <Grid item className={classes.gridTools}>
          <Button
            mini
            to={addButtonLink}
            component={Link}
            variant="fab"
            aria-label="Add"
            className={classNames(classes.buttonAdd, color)}
          >
            <AddIcon />
          </Button>
          <div className={classes.wrapper}>
            <IconButton
              onClick={onReload}
              className={classes.button}
              aria-label="Reload"
            >
              {loading ? '' : <Autorenew />}
            </IconButton>
            {loading && (
              <CircularProgress
                size={40}
                color="secondary"
                className={classes.fabProgress}
              />
            )}
          </div>
        </Grid>
        <Grid item>
          <Typography variant="display2">{title}</Typography>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(ToolPanel);
