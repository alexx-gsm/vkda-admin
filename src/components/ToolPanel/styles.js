import blue from '@material-ui/core/colors/blue';
import grey from '@material-ui/core/colors/grey';
import red from '@material-ui/core/colors/red';
import indigo from '@material-ui/core/colors/indigo';
import teal from '@material-ui/core/colors/teal';
import orange from '@material-ui/core/colors/orange';
import green from '@material-ui/core/colors/green';

export default theme => ({
  wrapTools: {
    padding: '0 20px',
    [theme.breakpoints.down('xs')]: {
      padding: 0
    }
  },
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridBtnBack: {
    [theme.breakpoints.up('sm')]: {
      padding: '6px 20px'
    }
  },
  buttonAdd: {
    '&.blue': {
      color: 'white',
      backgroundColor: blue[500],
      '&:hover': { backgroundColor: blue[600] }
    }
  },
  wrapper: {
    display: 'inline-block',
    margin: `0 ${theme.spacing.unit}`,
    position: 'relative'
  }
});
