import teal from '@material-ui/core/colors/teal';
import orange from '@material-ui/core/colors/orange';
import deepOrange from '@material-ui/core/colors/deepOrange';
import indigo from '@material-ui/core/colors/indigo';

export default theme => ({
  graph: {
    '& .ct-series-a .ct-line, & .ct-series-a .ct-point': {
      stroke: teal[500]
    },
    '&.orange .ct-series-a .ct-line, &.orange .ct-series-a .ct-point': {
      stroke: deepOrange[500]
    },
    '&.orange .ct-series-b .ct-line, &.orange .ct-series-b .ct-point': {
      stroke: deepOrange[500]
    },
    '&.indigo .ct-series-a .ct-line, &.indigo .ct-series-a .ct-point': {
      stroke: indigo[500]
    },
    '&.indigo .ct-series-b .ct-line, &.indigo .ct-series-b .ct-point': {
      stroke: deepOrange[500]
    }
  }
});
