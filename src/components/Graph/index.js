// Chartist
import Chartist from 'chartist';
import { compose, withProps } from 'recompose';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';
// core component
import Graph from './Graph';

const delays = 80;
const durations = 500;

export default compose(
  withProps(props => ({
    data: {
      labels: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
      series: props.data
    },
    color: props.color,
    options: {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0
      }),
      low: Math.min(...props.data),
      high: Math.max(...props.data),
      chartPadding: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      }
    },
    animation: {
      draw: function(data) {
        if (data.type === 'line' || data.type === 'area') {
          data.element.animate({
            d: {
              begin: 600,
              dur: 700,
              from: data.path
                .clone()
                .scale(1, 0)
                .translate(0, data.chartRect.height())
                .stringify(),
              to: data.path.clone().stringify(),
              easing: Chartist.Svg.Easing.easeOutQuint
            }
          });
        } else if (data.type === 'point') {
          data.element.animate({
            opacity: {
              begin: (data.index + 1) * delays,
              dur: durations,
              from: 0,
              to: 1,
              easing: 'ease'
            }
          });
        }
      }
    }
  })),
  withStyles(styles, { withTheme: true })
)(Graph);
