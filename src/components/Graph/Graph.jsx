import React from 'react';
import classNames from 'classnames';
// react plugin for creating charts
import ChartistGraph from 'react-chartist';
import '../../../node_modules/chartist/dist/chartist.css';

const Graph = ({ data, options, animation, classes, color }) => {
  return (
    <ChartistGraph
      className={classNames(classes.graph, [color])}
      data={data}
      type="Line"
      options={options}
      listener={animation}
    />
  );
};

export default Graph;
