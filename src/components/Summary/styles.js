import { deepOrange, teal, red, green, indigo } from '@material-ui/core/colors'

export default (theme) => ({
  paperSelect: {
    padding: theme.spacing.unit * 2,
  },
  GridSelect: { maxWidth: '960px' },
  PaperSummary: {
    marginTop: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    maxWidth: '960px',
  },
  TypoColibri: {
    color: deepOrange[700],
  },
  TypoVkda: {
    color: teal[700],
  },
  RowType: {
    '& th:not(:first-child)': {
      background: theme.palette.grey[100],
    },
    '& th:nth-child(even)': {
      borderLeft: '1px solid',
      borderLeftColor: theme.palette.grey[300],
    },
    '& th:last-child': {
      borderRight: '1px solid',
      borderRightColor: theme.palette.grey[300],
    },
  },
  TBodyRowRoot: { height: theme.spacing.unit * 4 },
  TBodyRowType: {
    '& td:first-child > *': {
      textTransform: 'uppercase',
    },
    '& td:nth-child(even)': {
      borderLeft: '1px solid',
      borderLeftColor: theme.palette.grey[300],
    },
    '& td:last-child': {
      borderRight: '1px solid',
      borderRightColor: theme.palette.grey[300],
    },
    '&.income td:not(:first-child) h2': {
      color: green[900],
    },
    '&.outcome td:not(:first-child) h2': {
      color: red[900],
    },
    '&.income td:not(:first-child) h3': {
      color: green[900],
    },
    '&.outcome td:not(:first-child) h3': {
      color: red[900],
    },
  },
  Table: {
    '& th:first-child': {
      border: 'none',
    },
    '& td:first-child': {
      border: 'none',
    },
    '& tfoot .income': {
      background: teal[800],
      '& h3': {
        color: theme.palette.common.white,
        fontWeight: 'bold',
        textTransform: 'uppercase',
      },
    },
    '& tfoot .outcome': {
      background: red[800],
      '& h3': {
        color: theme.palette.common.white,
        fontWeight: 'bold',
        textTransform: 'uppercase',
      },
    },
    '& tfoot .transfer': {
      background: indigo[800],
      '& h3': {
        color: theme.palette.common.white,
        fontWeight: 'bold',
        textTransform: 'uppercase',
      },
    },
  },
})
