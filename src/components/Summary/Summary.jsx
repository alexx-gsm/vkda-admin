import React, { Fragment, Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import moment from 'moment'
// AC
import { getMonthPayments } from '../../redux/modules/payment'
import { getCategories } from '../../redux/modules/category'
import { setFilter } from '../../redux/modules/filter'
// tools
import isEmpty from '../../helpers/is-empty'
import classNames from 'classnames'

import {
  Paper,
  TextField,
  MenuItem,
  Grid,
  Typography,
  Button,
  TableFooter,
} from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core'
// @material-ui
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'

class Summary extends Component {
  state = {
    year: moment().year(),
    month: moment().month(),
    income: {},
    outcome: {},
    incomeTotal: {},
    outcomeTotal: {},
  }

  componentDidMount() {
    const { year, month } = this.state
    this.props.getMonthPayments({ year, month })
    this.props.getCategories()
  }

  componentDidUpdate(prevProps) {
    const { monthPayments, categories } = this.props

    if (!isEmpty(categories) && monthPayments !== prevProps.monthPayments) {
      const { income = {}, outcome = {}, transfer = {} } =
        !isEmpty(monthPayments) && !isEmpty(categories) ? this.getSummary() : {}

      this.setState({ income, outcome, transfer })

      this.setState({
        incomeTotal: this.calcTotals(income),
        outcomeTotal: this.calcTotals(outcome),
      })
    }
  }

  getSummary = () => {
    const { monthPayments, categories } = this.props

    const incomeRootCategory = categories.find(
      (c) => c.title.toLowerCase() === 'приход'
    )
    const outcomeRootCategory = categories.find(
      (c) => c.title.toLowerCase() === 'расход'
    )

    const incomeCategories = categories.filter(
      (c) => c.rootId === incomeRootCategory._id
    )

    const outcomeCategories = categories.filter(
      (c) => c.rootId === outcomeRootCategory._id
    )

    const transfers = monthPayments.filter((p) => p.isTransfer)

    const calc = (cats) =>
      cats.reduce((acc, c) => {
        const filteredPayments = monthPayments.filter(
          (p) => !p.isTransfer && p.type === c._id
        )

        const x = filteredPayments.reduce((acc, fc) => {
          return {
            ...acc,
            [fc.variant]: {
              cash: !isEmpty(acc[fc.variant])
                ? acc[fc.variant].cash + fc.cash
                : fc.cash,
              card: !isEmpty(acc[fc.variant])
                ? acc[fc.variant].card + fc.card
                : fc.card,
            },
          }
        }, {})

        return {
          ...acc,
          [c.title]: x,
        }
      }, {})

    const calcTransfers = (transfers) =>
      transfers.reduce(
        (acc, t) => {
          return t.from !== t.to
            ? {
                ...acc,
                [t.from]: {
                  ...acc[t.from],
                  cash: t.isFromCard
                    ? acc[t.from].cash
                    : acc[t.from].cash - t.transferAmount,
                  card: t.isFromCard
                    ? acc[t.from].card - t.transferAmount
                    : acc[t.from].card,
                },
                [t.to]: {
                  ...acc[t.to],
                  cash: t.isToCard
                    ? acc[t.to].cash
                    : acc[t.to].cash + t.transferAmount,
                  card: t.isToCard
                    ? acc[t.to].card + t.transferAmount
                    : acc[t.to].card,
                },
              }
            : {
                ...acc,
                [t.from]: {
                  ...acc[t.from],
                  cash: t.isFromCard
                    ? acc[t.from].cash + t.transferAmount
                    : acc[t.from].cash - t.transferAmount,
                  card: t.isFromCard
                    ? acc[t.from].card - t.transferAmount
                    : acc[t.from].card + t.transferAmount,
                },
              }
        },
        {
          colibri: { cash: 0, card: 0 },
          vkda: { cash: 0, card: 0 },
        }
      )

    return {
      income: calc(incomeCategories),
      outcome: calc(outcomeCategories),
      transfer: calcTransfers(transfers),
    }
  }

  calcTotals = (payments) =>
    Object.keys(payments).reduce(
      (acc, title) => {
        return {
          ...acc,
          ...Object.keys(payments[title]).reduce((acc1, variant) => {
            return {
              ...acc1,
              [variant]: {
                cash:
                  acc[variant] && acc[variant].cash
                    ? acc[variant].cash + payments[title][variant].cash
                    : payments[title][variant].cash,
                card:
                  acc[variant] && acc[variant].card
                    ? acc[variant].card + payments[title][variant].card
                    : payments[title][variant].card,
              },
            }
          }, {}),
        }
      },
      {
        colibri: { cash: 0, card: 0 },
        vkda: { cash: 0, card: 0 },
      }
    )

  handleSelect = (e) => this.setState({ [e.target.name]: e.target.value })

  handleClick = () => {
    const { year, month } = this.state
    this.props.getMonthPayments({ year, month })
    this.setState({
      income: {},
      outcome: {},
      incomeTotal: {},
      outcomeTotal: {},
    })
  }

  render() {
    const {
      year,
      month,
      income,
      outcome,
      incomeTotal,
      outcomeTotal,
      transfer,
    } = this.state
    const { classes, filters, monthPayments, categories } = this.props
    const { arrYears } = filters

    const formatter = new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    })

    return (
      <Fragment>
        <Grid
          container
          spacing={16}
          alignItems="baseline"
          className={classes.GridSelect}>
          <Grid item xs>
            <TextField
              select
              id="year-select"
              label="Год"
              value={year}
              name="year"
              onChange={this.handleSelect}
              margin="normal"
              fullWidth>
              {arrYears.map((year) => (
                <MenuItem key={year} value={year}>
                  <Grid container direction="row" wrap="wrap">
                    <Typography variant="title">{year}</Typography>
                  </Grid>
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs>
            <TextField
              select
              id="month-select"
              label="Месяц"
              value={month}
              name="month"
              onChange={this.handleSelect}
              margin="normal"
              fullWidth>
              {[...Array(12)].map((x, index) => (
                <MenuItem key={index} value={index}>
                  <Grid container direction="row" wrap="wrap">
                    <Typography variant="title">
                      {moment()
                        .month(index)
                        .format('MMMM')}
                    </Typography>
                  </Grid>
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              onClick={this.handleClick}>
              Загрузить
            </Button>
          </Grid>
        </Grid>

        {!isEmpty(monthPayments) && !isEmpty(categories) && (
          <Paper square className={classes.PaperSummary}>
            <Table className={classes.Table}>
              <TableHead>
                <TableRow>
                  <TableCell colSpan={2} padding="none"></TableCell>
                  <TableCell colSpan={2} padding="none">
                    <Typography
                      variant="display1"
                      align="center"
                      className={classes.TypoColibri}>
                      КОЛИБРИ
                    </Typography>
                  </TableCell>
                  <TableCell colSpan={2} padding="none">
                    <Typography
                      variant="display1"
                      align="center"
                      className={classes.TypoVkda}>
                      ВКУСНОДА
                    </Typography>
                  </TableCell>
                </TableRow>
                <TableRow className={classes.RowType}>
                  <TableCell colSpan={2} padding="none"></TableCell>
                  <TableCell padding="checkbox">
                    <Typography variant="caption" align="right">
                      Наличка
                    </Typography>
                  </TableCell>
                  <TableCell padding="checkbox">
                    <Typography variant="caption" align="right">
                      Карта
                    </Typography>
                  </TableCell>
                  <TableCell padding="checkbox">
                    <Typography variant="caption" align="right">
                      Наличка
                    </Typography>
                  </TableCell>
                  <TableCell padding="checkbox">
                    <Typography variant="caption" align="right">
                      Карта
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {Object.keys(income).map((key) => (
                  <TableRow
                    key={key}
                    className={classNames(classes.TBodyRowType, 'income')}
                    classes={{ root: classes.TBodyRowRoot }}>
                    <TableCell colSpan={2} padding="checkbox">
                      <Typography variant="caption" align="right">
                        {key}
                      </Typography>
                    </TableCell>
                    <TableCell padding="checkbox">
                      <Typography variant="title" align="right">
                        {income[key].colibri && income[key].colibri.cash > 0
                          ? formatter.format(
                              income[key].colibri.cash.toFixed(0)
                            )
                          : '-'}
                      </Typography>
                    </TableCell>
                    <TableCell padding="checkbox">
                      <Typography variant="subheading" align="right">
                        {income[key].colibri && income[key].colibri.card > 0
                          ? formatter.format(
                              income[key].colibri.card.toFixed(0)
                            )
                          : '-'}
                      </Typography>
                    </TableCell>
                    <TableCell padding="checkbox">
                      <Typography variant="title" align="right">
                        {income[key].vkda && income[key].vkda.cash > 0
                          ? formatter.format(income[key].vkda.cash.toFixed(0))
                          : '-'}
                      </Typography>
                    </TableCell>
                    <TableCell padding="checkbox">
                      <Typography variant="subheading" align="right">
                        {income[key].vkda && income[key].vkda.card > 0
                          ? formatter.format(income[key].vkda.card.toFixed(0))
                          : '-'}
                      </Typography>
                    </TableCell>
                  </TableRow>
                ))}

                {/* <TableRow></TableRow> */}

                {Object.keys(outcome).map((key) => (
                  <TableRow
                    key={key}
                    className={classNames(classes.TBodyRowType, 'outcome')}
                    classes={{ root: classes.TBodyRowRoot }}>
                    <TableCell colSpan={2} padding="checkbox">
                      <Typography variant="caption" align="right">
                        {key}
                      </Typography>
                    </TableCell>
                    <TableCell padding="checkbox">
                      <Typography variant="title" align="right">
                        {outcome[key].colibri && outcome[key].colibri.cash > 0
                          ? `-${formatter.format(
                              outcome[key].colibri.cash.toFixed(0)
                            )}`
                          : '-'}
                      </Typography>
                    </TableCell>
                    <TableCell padding="checkbox">
                      <Typography variant="subheading" align="right">
                        {outcome[key].colibri && outcome[key].colibri.card > 0
                          ? `-${formatter.format(
                              outcome[key].colibri.card.toFixed(0)
                            )}`
                          : '-'}
                      </Typography>
                    </TableCell>
                    <TableCell padding="checkbox">
                      <Typography variant="title" align="right">
                        {outcome[key].vkda && outcome[key].vkda.cash > 0
                          ? `-${formatter.format(
                              outcome[key].vkda.cash.toFixed(0)
                            )}`
                          : '-'}
                      </Typography>
                    </TableCell>
                    <TableCell padding="checkbox">
                      <Typography variant="subheading" align="right">
                        {outcome[key].vkda && outcome[key].vkda.card > 0
                          ? `-${formatter.format(
                              outcome[key].vkda.card.toFixed(0)
                            )}`
                          : '-'}
                      </Typography>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>

              {!isEmpty(incomeTotal) &&
                !isEmpty(outcomeTotal) &&
                !isEmpty(transfer) && (
                  <TableFooter>
                    <TableRow>
                      <TableCell colSpan={6} />
                    </TableRow>
                    <TableRow className="income">
                      <TableCell colSpan={2} padding="checkbox">
                        <Typography variant="subheading" align="right">
                          приход:
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {formatter.format(
                            incomeTotal.colibri.cash.toFixed(0)
                          )}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {formatter.format(
                            incomeTotal.colibri.card.toFixed(0)
                          )}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {formatter.format(incomeTotal.vkda.cash.toFixed(0))}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {formatter.format(incomeTotal.vkda.card.toFixed(0))}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow className="outcome">
                      <TableCell colSpan={2} padding="checkbox">
                        <Typography variant="subheading" align="right">
                          расход:
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {outcomeTotal.colibri.cash !== 0
                            ? `-${formatter.format(
                                outcomeTotal.colibri.cash.toFixed(0)
                              )}`
                            : '-'}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {outcomeTotal.colibri.card !== 0
                            ? `-${formatter.format(
                                outcomeTotal.colibri.card.toFixed(0)
                              )}`
                            : '-'}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {outcomeTotal.vkda.cash !== 0
                            ? `-${formatter.format(
                                outcomeTotal.vkda.cash.toFixed(0)
                              )}`
                            : '-'}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {outcomeTotal.vkda.card !== 0
                            ? `-${formatter.format(
                                outcomeTotal.vkda.card.toFixed(0)
                              )}`
                            : '-'}
                        </Typography>
                      </TableCell>
                    </TableRow>

                    <TableRow className="transfer">
                      <TableCell colSpan={2} padding="checkbox">
                        <Typography variant="subheading" align="right">
                          Перемещение
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {formatter.format(transfer.colibri.cash)}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {formatter.format(transfer.colibri.card)}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {formatter.format(transfer.vkda.cash)}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="subheading" align="right">
                          {formatter.format(transfer.vkda.card)}
                        </Typography>
                      </TableCell>
                    </TableRow>

                    <TableRow>
                      <TableCell colSpan={2} padding="checkbox">
                        <Typography variant="subheading" align="right">
                          итог:
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="headline" align="right">
                          {formatter.format(
                            incomeTotal.colibri.cash.toFixed(0) -
                              outcomeTotal.colibri.cash.toFixed(0) +
                              +transfer.colibri.cash.toFixed(0)
                          )}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="headline" align="right">
                          {formatter.format(
                            incomeTotal.colibri.card.toFixed(0) -
                              outcomeTotal.colibri.card.toFixed(0) +
                              +transfer.colibri.card.toFixed(0)
                          )}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="headline" align="right">
                          {formatter.format(
                            incomeTotal.vkda.cash.toFixed(0) -
                              outcomeTotal.vkda.cash.toFixed(0) +
                              +transfer.vkda.cash.toFixed(0)
                          )}
                        </Typography>
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Typography variant="headline" align="right">
                          {formatter.format(
                            incomeTotal.vkda.card.toFixed(0) -
                              outcomeTotal.vkda.card.toFixed(0) +
                              +transfer.vkda.card.toFixed(0)
                          )}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableFooter>
                )}
            </Table>
          </Paper>
        )}
      </Fragment>
    )
  }
}

export default compose(
  connect(
    ({ payments, categoryStore, filterStore }) => {
      const { categories } = categoryStore
      const { monthPayments } = payments
      const { filters } = filterStore

      return {
        monthPayments,
        categories,
        filters,
      }
    },
    { getMonthPayments, getCategories, setFilter }
  ),
  withStyles(styles, { withTheme: true })
)(Summary)
