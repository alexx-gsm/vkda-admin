import React from 'react';
// Day Picker
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate
} from 'react-day-picker/moment';
import 'moment/locale/ru';
// useful tools
import isEmpty from '../../helpers/is-empty';
// styles
import './styles.css';

class Btn extends React.Component {
  render() {
    return (
      <button className="text-select-day" {...this.props}>
        {this.props.value}
      </button>
    );
  }
}

const DayPicker = ({
  classes,
  format,
  overlay,
  hoverRange,
  selectedDays,
  onDayClick,
  onWeekClick,
  onDayEnter,
  onDayLeave
}) => {
  const daysAreSelected = selectedDays.length > 0;
  const modifiers = {
    hoverRange,
    selectedRange: daysAreSelected && {
      from: selectedDays[0],
      to: selectedDays[6]
    },
    hoverRangeStart: hoverRange && hoverRange.from,
    hoverRangeEnd: hoverRange && hoverRange.to,
    selectedRangeStart: daysAreSelected && selectedDays[0],
    selectedRangeEnd: daysAreSelected && selectedDays[6]
  };

  return (
    <div className="SelectedWeek">
      <DayPickerInput
        component={Btn}
        value={
          !isEmpty(selectedDays)
            ? selectedDays[0]
            : formatDate(Date.now(), format, 'ru')
        }
        formatDate={formatDate}
        parseDate={parseDate}
        format={format}
        dayPickerProps={{
          showWeekNumbers: true,
          showOutsideDays: true,
          firstDayOfWeek: 1,
          todayButton: 'Сегодня',
          locale: 'ru',
          localeUtils: MomentLocaleUtils,
          modifiers: modifiers,
          selectedDays: selectedDays,
          onDayClick: onDayClick,
          onWeekClick: onWeekClick,
          onTodayButtonClick: onDayClick,
          onDayMouseEnter: onDayEnter,
          onDayMouseLeave: onDayLeave
        }}
        overlayComponent={
          overlay ? props => overlay({ ...props, classes }) : null
        }
        keepFocus={false}
      />
    </div>
  );
};

export default DayPicker;
