import React from 'react';
import { connect } from 'react-redux';
// AC
import {
  getPayments,
  setSelectedDays,
  setSelectedDay
} from '../../redux/modules/payment';
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose';
// components
import DayPicker from './DayPicker';
import { Typography } from '@material-ui/core';
// useful tools
import moment from 'moment';
import isEmpty from '../../helpers/is-empty';

function DayPickerOverlay({
  classes,
  classNames,
  selectedDay,
  children,
  ...props
}) {
  return (
    <div className={classes.overlayWrapper} {...props}>
      <div className={classes.overlay}>
        <Typography variant="caption" align="center">
          Выбери день/неделю
        </Typography>
        {children}
      </div>
    </div>
  );
}
function getWeekDays(weekStart) {
  const days = [weekStart];
  for (let i = 1; i < 7; i += 1) {
    days.push(
      moment(weekStart)
        .add(i, 'days')
        .toDate()
    );
  }
  return days;
}
function getWeekRange(date) {
  return {
    from: moment(date)
      .startOf('isoWeek')
      .add(moment().hour(), 'h')
      .toDate(),
    to: moment(date)
      .endOf('isoWeek')
      .toDate()
  };
}

export default compose(
  connect(
    ({ payments }) => ({
      getPayments,
      paymentStore: payments,
      selectedDays: payments.selectedDays,
      week: payments.week
    }),
    { setSelectedDays, setSelectedDay }
  ),
  withProps({
    overlay: DayPickerOverlay
  }),
  withState('hoverRange', 'setHoverRange', undefined),
  withHandlers({
    onDayClick: ({ setSelectedDays, setSelectedDay, closeStatic }) => date => {
      setSelectedDays(
        getWeekDays(getWeekRange(date).from),
        moment(date).week()
      );
      setSelectedDay(moment(date).weekday());
      closeStatic();
    },
    onWeekClick: ({ setSelectedDays, closeStatic }) => (
      weekNumber,
      days,
      e
    ) => {
      setSelectedDays(days, weekNumber);
      closeStatic();
    },
    onDayEnter: ({ setHoverRange }) => date =>
      setHoverRange(getWeekRange(date)),
    onDayLeave: ({ setHoverRange }) => () => setHoverRange(undefined)
  }),
  lifecycle({
    componentDidMount() {
      const { selectedDays } = this.props.paymentStore;
      if (isEmpty(selectedDays)) {
        this.props.setSelectedDays(
          getWeekDays(getWeekRange(moment()).from),
          moment().week()
        );
        this.props.setSelectedDay(moment().weekday());
      }
    }
  })
)(DayPicker);
