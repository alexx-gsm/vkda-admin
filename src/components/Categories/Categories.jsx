import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import { Link } from 'react-router-dom';
// material-ui components
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add';
import Autorenew from '@material-ui/icons/Autorenew';
// react-icon-kit
import isEmpty from '../../helpers/is-empty';

const Categories = ({
  rootCategories,
  filteredCategories,
  tabIndex,
  onTabChange,
  onChangeIndex,
  onReload,
  loading,
  classes
}) => {
  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridTools}>
          <Button
            mini
            to={'/categories/edit'}
            component={Link}
            variant="fab"
            aria-label="Add"
            className={classes.buttonAdd}
          >
            <AddIcon />
          </Button>

          <div className={classes.wrapper}>
            <IconButton
              onClick={onReload}
              className={classes.button}
              aria-label="Reload"
            >
              {loading ? '' : <Autorenew />}
            </IconButton>
            {loading && (
              <CircularProgress
                size={40}
                color="secondary"
                className={classes.fabProgress}
              />
            )}
          </div>
        </Grid>
      </Grid>

      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid container className={classes.gridHeader}>
            <Typography variant="display1" className={classes.typoHeaderTitle}>
              Категории
            </Typography>
          </Grid>
        </Paper>
        {!isEmpty(rootCategories) && (
          <Paper className={classes.paperTabs}>
            <AppBar position="static" color="default">
              <Tabs
                value={tabIndex}
                onChange={onTabChange}
                classes={{
                  root: classes.tabsRoot,
                  indicator: classes.tabsIndicator
                }}
                fullWidth
              >
                {rootCategories.map(c => (
                  <Tab
                    key={c._id}
                    label={c.title}
                    classes={{
                      root: classes.tabRoot,
                      selected: classes.tabSelected
                    }}
                  />
                ))}
              </Tabs>
            </AppBar>

            <SwipeableViews
              axis={'x'}
              index={tabIndex}
              onChangeIndex={onChangeIndex}
            >
              {filteredCategories.map((fc, index) => {
                if (isEmpty(fc)) {
                  return (
                    <Typography key={index} className={classes.paddingH}>
                      {index}
                    </Typography>
                  );
                } else {
                  return (
                    <div key={index} className={classes.viewContainer}>
                      {fc.map(c => (
                        <Link to={`/categories/edit/${c._id}`} key={c._id}>
                          <Typography className={classes.viewItem}>
                            {c.title}
                          </Typography>
                        </Link>
                      ))}
                    </div>
                  );
                }
              })}
            </SwipeableViews>
          </Paper>
        )}
      </Paper>
    </div>
  );
};

Categories.defaultProps = {
  rootCategories: []
};

export default Categories;
