import brown from '@material-ui/core/colors/brown';

export default theme => ({
  padding: { padding: '20px' },
  paddingH: { padding: '0 20px' },

  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridTools: {
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },
  gridBtnBack: {
    [theme.breakpoints.up('sm')]: {
      padding: '6px 20px'
    }
  },
  buttonAdd: {
    color: 'white',
    backgroundColor: brown[500],
    '&:hover': { backgroundColor: brown[600] }
  },
  card: {
    maxWidth: '760px',
    margin: '40px 0',
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  cardHeader: {
    position: 'relative',
    margin: '0',
    top: '-20px',
    padding: '5px 10px',
    background: `linear-gradient(60deg, ${brown[500]}, ${brown[600]})`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoHeaderTitle: {
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  wrapper: {
    display: 'inline-block',
    margin: `0 ${theme.spacing.unit}`,
    position: 'relative'
  },
  buttonReload: {},
  fabProgress: {
    position: 'absolute',
    top: 4,
    left: 4,
    zIndex: 1
  },
  // tabsRoot: { borderBottom: '1px solid #e8e8e8' },
  tabsIndicator: { backgroundColor: brown[500] },
  tabRoot: {
    '&:hover': {
      color: brown[500],
      opacity: 1
    },
    '&$tabSelected': {
      color: brown[500],
      fontWeight: theme.typography.fontWeightMedium
    },
    '&:focus': {
      color: brown[500]
    }
  },
  tabSelected: {},
  viewContainer: {
    padding: '20px'
  },
  viewItem: {
    marginBottom: '5px'
  }
});
