import { connect } from 'react-redux'
import {
  compose,
  lifecycle,
  withState,
  withHandlers,
  withProps,
} from 'recompose'
// AC
import {
  getCategories,
  getCategoryById,
  storeCategory,
  addCategory,
  saveCategory,
} from '../../../redux/modules/category'
// core components
import withDialog from '../../Dialog'
// @material-ui
import { withStyles } from '@material-ui/core/styles'
// useful tools
import isEmpty from '../../../helpers/is-empty'
// styles
import styles from './styles'
import CategoryEdit from './CategoryEdit'

export default compose(
  connect(
    ({ categoryStore }) => {
      const { categories, category, error, loading } = categoryStore
      return {
        categories,
        rootCategories: categories
          .filter((c) => c.isRoot === true)
          .sort((a, b) => a.sorting - b.sorting),
        tabIndex: categoryStore.tabIndex,
        category,
        error,
        loading,
      }
    },
    { getCategories, getCategoryById, storeCategory, addCategory, saveCategory }
  ),
  withHandlers({
    onChange: ({ category, storeCategory }) => (name) => (event) => {
      if (name === 'isRoot') {
        const isRoot = event.target.value === 'true'
        storeCategory({
          ...category,
          isRoot,
        })
      } else {
        storeCategory({
          ...category,
          [name]: event.target.value,
        })
      }
    },
    onSave: ({
      category,
      history,
      addCategory,
      saveCategory,
      rootCategories,
      tabIndex,
    }) => () => {
      if (isEmpty(category._id)) {
        const rootId = category.isRoot
          ? '-'
          : !isEmpty(category.rootId)
          ? category.rootId
          : rootCategories[tabIndex]
          ? rootCategories[tabIndex]._id
          : ''
        addCategory({ ...category, rootId }, history)
      } else {
        saveCategory(category, history)
      }
    },
  }),
  withState('isVisible', 'setVisible', false),
  withState('deletingItemId', 'setDeletingItemId', ''),
  withProps({
    modalTitle: 'Удаление документа',
    modalText: 'Вы уверены, что хотите удалить этот документ?',
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getCategoryById(id)
      }
      console.log('id:', id)
      if (isEmpty(this.props.categories)) {
        this.props.getCategories()
      }
    },
  }),
  withDialog,
  withStyles(styles, { withTheme: true })
)(CategoryEdit)
