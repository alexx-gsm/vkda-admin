import React from 'react'
import { Link } from 'react-router-dom'
// useful utils
import isEmpty from '../../../helpers/is-empty'
import classNames from 'classnames'
// material-ui components
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Divider from '@material-ui/core/Divider'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
// @material-ui/icons
import ArrowBack from '@material-ui/icons/ArrowBack'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
import { sortNumericAsc } from 'react-icons-kit/fa/sortNumericAsc'

const CategoryEdit = ({
  rootCategories,
  tabIndex,
  category = {},
  classes = {},
  error = {},
  onChange,
  onSave,
  onDelete,
}) => {
  const { _id, title, isRoot, rootId, sorting } = category

  const defaultCategory = !isEmpty(rootCategories)
    ? rootCategories[tabIndex]._id
    : ''

  return (
    <div>
      <Grid container className={classes.tools}>
        <Grid item className={classes.gridBtnBack}>
          <Button
            mini
            variant="fab"
            to={'/categories'}
            component={Link}
            aria-label="Back">
            <ArrowBack />
          </Button>
        </Grid>
      </Grid>
      <Paper className={classes.card}>
        <Paper className={classes.cardHeader}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant="display1" className={classes.typoHeaderTitle}>
              Редактируем Категорию
            </Typography>
          </Grid>
        </Paper>

        <form className={classes.cardForm} noValidate autoComplete="off">
          <TextField
            fullWidth
            id="title"
            label="Название"
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin="dense"
            className={classes.wrapTitle}
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused,
              },
            }}
            InputProps={{
              className: classes.inputTitle,
              classes: { underline: classes.underlinePrimary },
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />
          <TextField
            fullWidth
            id="sort"
            label="Сортировка"
            type="number"
            value={isEmpty(sorting) ? 0 : sorting}
            onChange={onChange('sorting')}
            className={classes.select}
            margin="none"
            inputProps={{
              step: 1,
            }}
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused,
              },
            }}
            InputProps={{
              classes: { underline: classes.underlinePrimary },
              endAdornment: (
                <InputAdornment position="end">
                  <Icon icon={sortNumericAsc} />
                </InputAdornment>
              ),
            }}
          />
          <Grid container className={classes.gridCategorySelect} spacing={16}>
            <Grid item xs={12} sm={5}>
              <FormGroup row className={classes.formGroupSwitch}>
                <FormControlLabel
                  control={
                    <Switch
                      checked={isRoot}
                      onChange={onChange('isRoot')}
                      value={isRoot ? 'false' : 'true'}
                      classes={{
                        switchBase: classes.colorSwitchBase,
                        checked: classes.colorChecked,
                        bar: classes.colorBar,
                      }}
                    />
                  }
                  label={isRoot ? 'Root' : 'Not root'}
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} sm={7}>
              {!isRoot && (
                <TextField
                  select
                  fullWidth
                  error={Boolean(error.category)}
                  id="type-select"
                  label="Root категория"
                  // className={classes.select}
                  value={isEmpty(rootId) ? defaultCategory : rootId}
                  onChange={onChange('rootId')}
                  InputLabelProps={{
                    shrink: true,
                    FormLabelClasses: {
                      root: classes.labelPrimary,
                      focused: classes.cssFocused,
                    },
                  }}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
                  InputProps={{
                    classes: { underline: classes.underlinePrimary },
                  }}
                  helperText={error.category ? error.category : ''}
                  margin="none">
                  {rootCategories.map((option) => (
                    <MenuItem key={option._id} value={option._id}>
                      {option.title}
                    </MenuItem>
                  ))}
                </TextField>
              )
              // : (
              //   <TextField
              //     fullWidth
              //     id="sort"
              //     label="Сортировка"
              //     type="number"
              //     value={isEmpty(sorting) ? 0 : sorting}
              //     onChange={onChange('sorting')}
              //     className={classes.select}
              //     margin="none"
              //     inputProps={{
              //       min: 0,
              //       step: 1,
              //     }}
              //     InputLabelProps={{
              //       FormLabelClasses: {
              //         root: classes.labelPrimary,
              //         focused: classes.cssFocused,
              //       },
              //     }}
              //     InputProps={{
              //       classes: { underline: classes.underlinePrimary },
              //       endAdornment: (
              //         <InputAdornment position="end">
              //           <Icon icon={sortNumericAsc} />
              //         </InputAdornment>
              //       ),
              //     }}
              //   />
              // )
              }
            </Grid>
          </Grid>
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Button
              mini
              onClick={onDelete}
              variant="fab"
              aria-label="Delete"
              color="secondary"
              className={classes.margin}>
              <Icon size={18} icon={trashO} />
            </Button>
          </Grid>
          <Grid item>
            <Button
              to={'/categories'}
              component={Link}
              aria-label="Back"
              variant="contained">
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label="Add"
              variant="contained"
              className={classNames(classes.margin, classes.btnPrimary)}>
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}

export default CategoryEdit
