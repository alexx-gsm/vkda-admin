import brown from '@material-ui/core/colors/brown';

export default theme => ({
  padding: { padding: '20px' },
  paddingH: { padding: '10px 20px' },
  margin: {
    margin: theme.spacing.unit
  },
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridTools: {
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },
  gridBtnBack: {
    [theme.breakpoints.up('sm')]: {
      padding: '6px 20px'
    }
  },
  buttonAdd: {
    color: 'white',
    backgroundColor: brown[500],
    '&:hover': { backgroundColor: brown[700] }
  },
  card: {
    maxWidth: '600px',
    margin: '40px 0',

    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0',
      marginBottom: 0
    }
  },
  // CARD HEADER
  cardHeader: {
    position: 'relative',
    margin: '0 20px',
    top: '-20px',
    padding: '5px 10px',
    background: `linear-gradient(60deg, ${brown[600]}, ${brown[500]})`,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0',
      borderRadius: 0
    }
  },
  gridCardHeader: {
    justifyContent: 'center'
  },
  typoHeaderTitle: {
    color: theme.palette.common.white,
    textTransform: 'uppercase',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.5rem'
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.25rem'
    }
  },
  // CARD BODY
  cardForm: {
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  wrapTitle: {
    marginBottom: '30px'
  },
  inputTitle: {
    fontSize: '46px',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  labelPrimary: {
    '&$cssFocused': {
      color: brown[700]
    }
  },
  cssFocused: {},
  underlinePrimary: {
    '&:after': {
      borderBottomColor: brown[500]
    }
  },
  colorSwitchBase: {
    '&$colorChecked': {
      color: brown[500],
      '& + $colorBar': {
        backgroundColor: brown[700]
      }
    }
  },
  colorBar: {},
  colorChecked: {},
  gridCategorySelect: {
    alignItems: 'flex-end',
    minHeight: '75px'
  },
  formGroupSwitch: {
    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center'
    }
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  btnPrimary: {
    color: theme.palette.common.white,
    backgroundColor: brown[600],
    '&:hover': {
      backgroundColor: brown[700]
    }
  }
});
