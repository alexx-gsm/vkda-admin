import { compose, withHandlers, lifecycle } from 'recompose';
import { connect } from 'react-redux';
// AC
import {
  getCategories,
  clearCategory,
  setTabIndex
} from '../../redux/modules/category';
// @material-ui
import { withStyles } from '@material-ui/core/styles';
// styles
import styles from './styles';
import Categories from './Categories';

export default compose(
  connect(
    ({ categoryStore }) => {
      const { categories } = categoryStore;
      const rootCategories = categories
        .filter(c => c.isRoot === true)
        .sort((a, b) => a.sorting - b.sorting);
      const filteredCategories = rootCategories.reduce((acc, rootCategory) => {
        const items = categories.filter(c => c.rootId === rootCategory._id);
        return [...acc, items];
      }, []);

      return {
        rootCategories,
        categories,
        filteredCategories,
        tabIndex: categoryStore.tabIndex
      };
    },
    { getCategories, clearCategory, setTabIndex }
  ),
  // withState('tabIndex', 'setTabIndex', 0),
  withHandlers({
    onTabChange: ({ setTabIndex }) => (event, value) => setTabIndex(value),
    onChangeIndex: ({ setTabIndex }) => index => setTabIndex(index)
  }),
  lifecycle({
    componentDidMount() {
      this.props.getCategories();
      this.props.clearCategory();
    }
  }),
  withStyles(styles, { withTheme: true })
)(Categories);
