import React from 'react';
import Dialog from './Dialog.jsx';

const withDialog = Component => ({
  modalTitle,
  modalText,
  isVisible,
  setVisible,
  deletingItemId,
  onConfirm,
  ...props
}) => (
  <div>
    <Component {...props} />
    <Dialog
      title={modalTitle}
      text={modalText}
      isVisible={isVisible}
      onCancel={() => setVisible(false)}
      onConfirm={() => onConfirm(deletingItemId, props.history)}
    />
  </div>
);

export default withDialog;
