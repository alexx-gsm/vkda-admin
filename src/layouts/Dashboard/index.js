import { compose } from 'recompose';
// hocs
import withSidebar from '../../hocs/withSidebar';
import { withStyles } from '@material-ui/core/styles';
// component
import Dashboard from './Dashboard';
// styles
import styles from './style';

export default compose(
  withStyles(styles, { withTheme: true }),
  withSidebar
)(Dashboard);
