import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Payments from '../../components/Payments';
import PaymentEdit from '../../components/Payments/edit';

const PaymentView = () => {
  return (
    <Switch>
      <Route path="/payments/edit/:id?" component={PaymentEdit} />
      <Route path="/payments" component={Payments} />
    </Switch>
  );
};

export default PaymentView;
