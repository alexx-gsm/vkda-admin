import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Categories from '../../components/Categories';
import CategoryEdit from '../../components/Categories/edit';

const CategoryView = props => (
  <div>
    <Switch>
      <Route path="/categories/edit/:id?" component={CategoryEdit} />
      <Route path="/categories" component={Categories} />
    </Switch>
  </div>
);

export default CategoryView;
