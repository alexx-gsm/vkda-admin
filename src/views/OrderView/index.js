import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Orders from '../../components/Orders';
import OrderEdit from '../../components/Orders/edit';
import OrderProcessing from '../../components/Orders/processing';

const OrderView = () => (
  <Switch>
    <Route path="/orders/edit/:id?" component={OrderEdit} />
    <Route path="/orders/processing/:id?" component={OrderProcessing} />
    <Route path="/orders" component={Orders} />
  </Switch>
);

export default OrderView;
