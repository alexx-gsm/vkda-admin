import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Customers from '../../components/Customers';
import CustomerEdit from '../../components/Customers/edit';

const CustomerView = () => {
  return (
    <Switch>
      <Route path="/customers/edit/:id?" component={CustomerEdit} />
      <Route path="/customers" component={Customers} />
    </Switch>
  );
};

export default CustomerView;
