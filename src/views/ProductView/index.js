import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Products from '../../components/Products';
import ProductEdit from '../../components/Products/edit';

const ProductView = () => {
  return (
    <Switch>
      <Route path="/products/edit/:id?" component={ProductEdit} />
      <Route path="/products" component={Products} />
    </Switch>
  );
};

export default ProductView;
