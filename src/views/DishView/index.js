import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Dishes from '../../components/Dishes';
import DishEdit from '../../components/Dishes/edit';

const DishView = () => (
  <Switch>
    <Route path="/dishes/edit/:id?" component={DishEdit} />
    <Route path="/dishes" component={Dishes} />
  </Switch>
);

export default DishView;
